﻿Imports micrologic
Imports Paygateway

Imports System.Data
Imports System.Data.SqlClient

Imports MLAReportsDLL

Imports System.IO


Module Globals
    Public iUnlimitedHour As Integer = 2
    Public bProcessMonthlyAccounts As Boolean = True
    Public iMaximumTicketLength As Integer = 0
    Public iResetUnlimitedStartDateDaysAfterMonthlyNextPay As Integer = 0
    Public bResetUnlimitedStartDateAfterTimeOut As Boolean = False
    Public bDisableUnlimitedBackCharge As Boolean = False
    Public bResetUnlimitedStartDateAfterDecline As Boolean = False
    Public bCloseUnlimitedShift As Boolean = False
    Public bCheckHistoryForDuplicateCharge As Boolean = False

    'Configuration Variables
    Public sLogFileName As String = ""
    Public sServerName As String
    Public sDatabaseName As String
    Public sServerUser As String
    Public sServerPassword As String
    Public sCheckChartDataBase As String

    Public sMerchantNumber As String = ""
    Public sProcessorCode As String = ""
    Public sCCProgramAddress As String = ""
    Public sCCDBPath As String = ""
    Public sCCProgramPath As String = ""
    Public bAutoCloseBatch As Boolean = False
    Public iOfflineProcessing As Integer = 0
    Public iofflinehour As Integer = 22 'default to 10 PM so as not to process at same time as POS @ 11:30 PM
    Public iofflineminute As Integer = 0
    Public iPCCWTransMode As Integer = 0
    Public sCCUser As String = "User1"
    Public sCCUser_PW As String = ""
    Public bDisableForceFlag As Boolean = False
    Public iCreditMode As Integer = 0
    Public sClientId As String = ""
    Public iCCProgramPort As Integer = 0

    Public Employee As New EmployeeBOL
    Public PCCW As New PCCWTransBOL
    Public Account As New Account.AccountBOL
    Public Customer As New Customer.CustomerBOL
    Public Vehicle As New Vehicle.VehicleBOL
    Public Ticket As New Ticket.TicketBOL
    Public User As New User.UserBOL
    Public Surcharge As New SurchargeBOL
    Public TicketDetail As New TicketDetailBOL
    Public cPoints As New PointsBOL
    Public PrepaidBook As New PrepaidBookBOL
    Public Discount As New DiscountBOL

    'Public sReportsPath As String

    'Public ReportDB As New ReportBOL

    Public dbhelper As New DatabaseHelper
    Public cnnLogicWash As New System.Data.SqlClient.SqlConnection

    'Public TicketDetail As New TicketDetailBOL

    Public bDeclareOpeningDrawer As Boolean



    Public CreditCard As New CreditCardBOL
    Public Payment As New PaymentBOL
    Public GiftCard As New GiftCardBOL

    Public AccountInfo As AccountProperties

    Public bProcessingMonthly As Boolean = False

    Public dsConfig As New DataSet

    Public userMonthly As New UserProperties

    Public sEdgeWebId As String = ""
    Public sEdgeAuthKey As String = ""
    Public sEdgeTerminalId As String = ""
    Public sEdgeDirectUrl As String = ""
    Public sDeclineEmailAPIURL As String = ""

    Public bSaveSecurePaymentLog As Boolean = False

    Public iCCTimeOut As Integer = 0
    Public sComputerName As String = System.Net.Dns.GetHostName
    Public lComputerTerminalId As Long = 0

    Public sStatus As String = ""

    Public iFamilyAccountDiscountMode As Integer = 0
    Public bManualFamilyAccountVerification As Boolean = False
    Public iSendUnlimitedAutoChargeEmail As Integer = 0
    Public sEmailAPIURL As String = ""
    Public sEmailReceiptSubjectLine As String = ""
    Public sMailFromAddress As String = ""
    Public bEnableMonthlyProcessOnlyMode As Boolean = False

    Public dsCC As New DataSet

    'Public iDBCounter As Integer = 30
    Public dtStart As Date = Now

    Public Sub ProcessMonthlyAccounts(ByVal bTimer As Boolean)
        Dim sEmailResponse As String = ""

        Dim iChargedCounter As Integer = 0
        Dim iFailedCounter As Integer = 0

        Dim mCust(0) As CustomerProperties
        Dim iCounter As Integer = 0

        Dim sTicket As String = ""
        'Dim frmMonthlyCreditCardForm As New frmOfflineCreditCards(False, "", 60, True)
        Dim MonthlyAcctInfo As New CustomerAccountProperties
        Dim dblAmount As Double = 0

        ' Dim usermonthly.userid As Long = 9999

        'Dim userMonthly As New UserProperties

        Dim MonthlySurcharge As New SurchargeProperties

        Dim bSaveTicket As Boolean = False
        Dim bSaveTrans As Boolean = False

        Dim sCCAuth As String = ""
        Dim sCCRef As String = ""
        Dim sCCTicket As String = ""
        Dim sCCType As String = ""
        Dim sCCNumber As String = ""
        Dim sCCExp As String = ""
        'Dim sCCTrack As String = ""
        Dim sCCAmount As String = ""

        Dim dblTaxes As Double = 0
        Dim PCCWTrans As New PCCWProperties
        Dim dtTrans As DateTime = CDate("1/1/1900")
        Dim sTroutD As String = ""

        Dim bCloseshift As Boolean = False
        Dim lClosedShiftNumber As Long = 0
        Dim sFrom As String = ""
        Dim sTo As String = ""
        'Dim CashierReport As New ReportProperties
        Dim dsReport As New DataSet
        'Dim mlaPrint As New mlaDrawing

        Dim lMonthlyTicketId As Long = 0

        Dim lReturnId As Long = 0

        Dim sTransRoutD As String = ""

        Dim iAttempts As Integer = 0

        Dim iAction As Integer = 1

        Dim LastMonthlyTicket As New TicketProperties
        Dim iDays As Integer = 25
        Dim bDuplicate As Boolean = False

        Dim lSurchargeId As Long = 0
        Dim dblMonthlyAmount As Double = 0

        Dim cMPDInfo As New CustomerManagedPayerData

        Dim bFirstCharge As Boolean = False

        Dim sMonthlyTicketNumber As String = ""

        Dim dblTaxRate As Double = Account.GetDefaultTaxRate()

        Dim sCardHolderName As String = ""

        Dim iAutoChargeDeclineAttempts As Integer = 3

        Dim sGCNumber As String = ""
        Dim dblGCBalance As Double = 0

        Dim sPaymentType As String = "CREDIT CARD"

        Dim lProcessMonthlyAccountsTerminal As Long = 0

        Dim sCustomerMessage As String = ""
        Dim lMessageType As Long = 0

        Dim sDeclineEmailDays() As String
        Dim i As Integer = 0


        Dim lCUTId As Long = 0
        Try

            'bDisableTimeout = True

            lProcessMonthlyAccountsTerminal = Account.GetProcessMonthlyAccountsTerminal

            If lProcessMonthlyAccountsTerminal > 0 Then
                If lComputerTerminalId <> lProcessMonthlyAccountsTerminal Then
                    Customer.ExecMonthlyTransaction(0, "AUTOCHARGE ERROR SUMMARY: Invalid Terminal.", userMonthly.UserName, "", 0, sComputerName)
                    WriteToLog(sLogFileName, "ProcessMonthlyAccounts", 0, "Invalid Terminal", "EVENT")

                    'bDisableTimeout = False
                    Exit Sub
                End If
            End If

            iAutoChargeDeclineAttempts = Account.GetAutoChargeDeclineAttempts

            sDeclineEmailDays = Account.GetDeclineEmailDays

            'If sFormText = "" Then sFormText = Me.Text

            sStatus = "Processing unlimited accounts."
            EventLog.WriteEntry("LWMonthly", sStatus, EventLogEntryType.Information)

            userMonthly = User.GetAutoCharge

            If Hour(Now) = iUnlimitedHour Or bTimer = False Then
                mCust = Customer.GetMonthlyDueAccounts(AccountInfo.LocationDesc, False, 120)
            Else
                mCust = Customer.GetMonthlyDueAccounts(AccountInfo.LocationDesc, True, 120)
            End If

            If mCust.Length = 0 Then
                WriteToLog(sLogFileName, "GetMonthlyDueAccounts", 0, "Error", "ERROR")
            ElseIf mCust(0).CustomerId = -1 Then
                WriteToLog(sLogFileName, "GetMonthlyDueAccounts", 0, mCust(0).Comments, "ERROR")
            ElseIf mCust.Length > 0 Then
                WriteToLog(sLogFileName, "GetMonthlyDueAccounts", 0, mCust.Length & " accounts to process.", "EVENT")
            End If

            'iProcessCount = iProcessCount + 1
            'Me.lblProgramLabel.Text = iProcessCount.ToString



            sStatus = mCust.Length - 1 & " customers found."
            EventLog.WriteEntry("LWMonthly", sStatus, EventLogEntryType.Information)

            iCounter = 0
            Do Until mCust(iCounter) Is Nothing Or lReturnId = -1
                'Me.lblTitle.Text = "Charging first month."

                sStatus = " processing " & iCounter + 1 & " of " & mCust.Length - 1
                EventLog.WriteEntry("LWMonthly", sStatus, EventLogEntryType.Information)

                sCustomerMessage = ""
                lMessageType = 0

                sPaymentType = "CREDIT CARD"

                bFirstCharge = False

                sMonthlyTicketNumber = ""

                sTransRoutD = ""
                lMonthlyTicketId = 0

                MonthlyAcctInfo.Clear()
                MonthlyAcctInfo = Customer.GetAccountType(mCust(iCounter).MonthlyAccountTypeId)

                bDuplicate = False


                'If MonthlyAcctInfo.Taxable = True Then
                '    dblAmount = MonthlyAcctInfo.Amount + (MonthlyAcctInfo.Amount * dblTaxRate / 100)
                'Else
                '    dblAmount = MonthlyAcctInfo.Amount
                'End If
                'Application.DoEvents()

                lSurchargeId = MonthlyAcctInfo.SurchargeId
                dblMonthlyAmount = MonthlyAcctInfo.Amount
                'If MonthlyAcctInfo.FamilySurchargeId > 0 And (mCust(iCounter).sCCNumber <> "" Or (iCreditMode >= 4 And mCust(iCounter).sSPAN <> "")) Then
                If ((bManualFamilyAccountVerification = True And mCust(iCounter).FamilyAccount = True) Or bManualFamilyAccountVerification = False) And MonthlyAcctInfo.FamilySurchargeId > 0 And (mCust(iCounter).sCCNumber <> "" Or (iCreditMode >= 4 And mCust(iCounter).sSPAN <> "")) Then

                    Select Case iFamilyAccountDiscountMode
                        Case 1
                            If Customer.GetFamilyAccountIndex(mCust(iCounter).sCCNumber, mCust(iCounter).CustomerId, iCreditMode, mCust(iCounter).sSPAN, mCust(iCounter).sExpireMonth, mCust(iCounter).sExpireYear, mCust(iCounter).sBIN, mCust(iCounter).sSPAN2, mCust(iCounter).sBIN2, bManualFamilyAccountVerification, True) > 0 Then
                                lSurchargeId = MonthlyAcctInfo.FamilySurchargeId
                                dblMonthlyAmount = MonthlyAcctInfo.FamilyAmount
                            End If
                        Case Else
                            If Customer.HasFamilyAccount(mCust(iCounter).sCCNumber, mCust(iCounter).CustomerId, iCreditMode, mCust(iCounter).sSPAN, mCust(iCounter).sExpireMonth, mCust(iCounter).sExpireYear, mCust(iCounter).sBIN, mCust(iCounter).sSPAN2, mCust(iCounter).sBIN2, bManualFamilyAccountVerification, True) = True Then
                                lSurchargeId = MonthlyAcctInfo.FamilySurchargeId
                                dblMonthlyAmount = MonthlyAcctInfo.FamilyAmount
                            End If
                    End Select
                End If


                Dim bAllowSurchargeOnAutocharge As Boolean = False
                If bAllowSurchargeOnAutocharge = True And lSurchargeId > 0 And mCust(iCounter).MonthlyPaid <= CDate("1/1/2000") Then

                    bFirstCharge = True

                    MonthlySurcharge = Surcharge.GetSurchargeInfo(lSurchargeId, "")
                    If MonthlySurcharge.Taxable = True Then
                        'dblAmount = MonthlySurcharge.Amount + (MonthlySurcharge.Amount * dblTaxRate / 100)
                        dblTaxes = (MonthlySurcharge.Amount * dblTaxRate / 100)
                        dblAmount = MonthlySurcharge.Amount + dblTaxes
                    Else
                        dblTaxes = 0
                        dblAmount = MonthlySurcharge.Amount
                    End If

                    If MonthlyAcctInfo.iMonthlyChargeDay > 0 Then
                        If Today.Day <> MonthlyAcctInfo.iMonthlyChargeDay Then

                            dblAmount = dblAmount * (CDate(Today.Month & "/" & MonthlyAcctInfo.iMonthlyChargeDay & "/" & Today.Year).AddMonths(1) - Today).TotalDays / Date.DaysInMonth(Today.Year, Today.Month)
                            If dblTaxes > 0 Then dblTaxes = ((dblAmount) * dblTaxRate / 100)
                        End If
                    End If

                ElseIf MonthlyAcctInfo.Taxable = True Then
                    'dblAmount = MonthlyAcctInfo.Amount + (MonthlyAcctInfo.Amount * dblTaxRate / 100)
                    dblTaxes = (dblMonthlyAmount * dblTaxRate / 100)
                    dblAmount = dblMonthlyAmount + dblTaxes
                Else
                    dblTaxes = 0
                    dblAmount = dblMonthlyAmount
                    'dblAmount = 0
                End If

                dblAmount = Math.Round(dblAmount, 2, MidpointRounding.AwayFromZero)

                If mCust(iCounter).MonthlyExpires > CDate("1/1/2000") And CDate(mCust(iCounter).MonthlyExpires.ToShortDateString) <= CDate(Now.ToShortDateString) Then

                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)

                    mCust(iCounter).MonthlyActive = False
                    mCust(iCounter).MonthlyDeclined = False



                    Customer.SaveMonthlyInfo(mCust(iCounter), "LWREPORTS", 9999, AccountInfo.LocationDesc)
                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE EXPIRED", "LWREPORTS", "", 0, sComputerName)

                    dblAmount = 0
                End If

                LastMonthlyTicket = Ticket.GetLatestMonthlyTicket(mCust(iCounter).CustomerId)

                If MonthlyAcctInfo.ChargeFrequency > 0 Then
                    iDays = MonthlyAcctInfo.ChargeFrequency - 5
                Else
                    iDays = 25
                End If

                If (Now - LastMonthlyTicket.DateCreated).TotalDays < iDays And bCheckHistoryForDuplicateCharge = True Then
                    dblAmount = 0
                    bDuplicate = True
                End If


                If dblAmount > 0 And MonthlyAcctInfo.ChargeFrequency <> 0 Then 'And mCust(iCounter).MonthlyActive = True Then


                    bCloseshift = True
                    'usermonthly.userid = User.GetAutoChargeId
                    sMonthlyTicketNumber = GetUnlimitedTicketNumber(mCust(iCounter), False)

                    lSurchargeId = MonthlyAcctInfo.SurchargeId
                    If MonthlyAcctInfo.FamilySurchargeId > 0 And (mCust(iCounter).sCCNumber <> "" Or (iCreditMode >= 4 And mCust(iCounter).sSPAN <> "")) Then
                        Select Case iFamilyAccountDiscountMode
                            Case 1
                                If Customer.GetFamilyAccountIndex(mCust(iCounter).sCCNumber, mCust(iCounter).CustomerId, iCreditMode, mCust(iCounter).sSPAN, mCust(iCounter).sExpireMonth, mCust(iCounter).sExpireYear, mCust(iCounter).sBIN, mCust(iCounter).sSPAN2, mCust(iCounter).sBIN2, bManualFamilyAccountVerification, True) > 0 Then
                                    lSurchargeId = MonthlyAcctInfo.FamilySurchargeId
                                End If
                            Case Else
                                If Customer.HasFamilyAccount(mCust(iCounter).sCCNumber, mCust(iCounter).CustomerId, iCreditMode, mCust(iCounter).sSPAN, mCust(iCounter).sExpireMonth, mCust(iCounter).sExpireYear, mCust(iCounter).sBIN, mCust(iCounter).sSPAN2, mCust(iCounter).sBIN2, bManualFamilyAccountVerification, True) = True Then
                                    lSurchargeId = MonthlyAcctInfo.FamilySurchargeId
                                End If
                        End Select
                    End If

                    If bAllowSurchargeOnAutocharge = True And lSurchargeId > 0 And mCust(iCounter).MonthlyPaid <= CDate("1/1/2000") Then
                        lMonthlyTicketId = CreateMonthlyTicket(sMonthlyTicketNumber, userMonthly.UserName, userMonthly.UserID, mCust(iCounter).CustomerId, 0, lSurchargeId, MonthlyAcctInfo.AccountTypeId, dblAmount, dblTaxes, "", mCust(iCounter).sCCNumber, mCust(iCounter).sSPAN, mCust(iCounter).sExpireMonth, mCust(iCounter).sExpireYear, mCust(iCounter).sBIN, mCust(iCounter).sSPAN2, mCust(iCounter).sBIN2, mCust(iCounter).FamilyAccount)
                    Else
                        lMonthlyTicketId = CreateMonthlyTicket(sMonthlyTicketNumber, userMonthly.UserName, userMonthly.UserID, mCust(iCounter).CustomerId, 0, 0, MonthlyAcctInfo.AccountTypeId, dblAmount, dblTaxes, "", mCust(iCounter).sCCNumber, mCust(iCounter).sSPAN, mCust(iCounter).sExpireMonth, mCust(iCounter).sExpireYear, mCust(iCounter).sBIN, mCust(iCounter).sSPAN2, mCust(iCounter).sBIN2, mCust(iCounter).FamilyAccount)
                    End If

                    If Not lMonthlyTicketId > 0 Then
                        'Something went wrong saving the ticket, so don't bother charging and updating monthly info
                        'and sending email with zero total.  This customer will get charged the next day.
                        'Move to next customer
                        iCounter = iCounter + 1
                        Continue Do
                    End If

                    cMPDInfo = Customer.GetManagedPayerData(mCust(iCounter).CustomerId)



                    If mCust(iCounter).sCCNumber = "" And iCreditMode < 4 Then
                        iFailedCounter = iFailedCounter + 1
                        Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)

                        mCust(iCounter).MonthlyDeclined = False
                        mCust(iCounter).MonthlyActive = False
                        Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                        Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE NO CARD", "LWREPORTS", "", 0, sComputerName)
                        bSaveTicket = False
                        bSaveTrans = False
                    ElseIf mCust(iCounter).sCCExpDate = "" And iCreditMode < 4 Then
                        iFailedCounter = iFailedCounter + 1
                        Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)

                        mCust(iCounter).MonthlyDeclined = False
                        mCust(iCounter).MonthlyActive = False
                        Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                        Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE NO EXP DATE", "LWREPORTS", "", 0, sComputerName)
                        bSaveTicket = False
                        bSaveTrans = False
                    ElseIf LogicDecrypt(mCust(iCounter).sCCNumber) = "" And iCreditMode < 4 Then
                        iFailedCounter = iFailedCounter + 1
                        Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)

                        mCust(iCounter).MonthlyDeclined = False
                        mCust(iCounter).MonthlyActive = False
                        Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                        Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE NO CARD", "LWREPORTS", "", 0, sComputerName)
                        bSaveTicket = False
                        bSaveTrans = False
                    ElseIf iCreditMode >= 4 And cMPDInfo.sPayerIdentifier = "" Then
                        iFailedCounter = iFailedCounter + 1
                        Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)

                        mCust(iCounter).MonthlyDeclined = False
                        mCust(iCounter).MonthlyActive = False
                        Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                        Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE NO MPD", "LWREPORTS", "", 0, sComputerName)
                        bSaveTicket = False
                        bSaveTrans = False
                    Else
                        'sMonthlyTicketNumber = GetUnlimitedTicketNumber(mCust(iCounter), False)

                        If mCust(iCounter).MonthlyNextPay > CDate("1/1/1900") Then
                            dtTrans = CDate(mCust(iCounter).MonthlyNextPay.ToShortDateString)
                        ElseIf mCust(iCounter).MonthlyStart > CDate("1/1/1900") Then
                            dtTrans = CDate(mCust(iCounter).MonthlyStart.ToShortDateString)
                        Else
                            dtTrans = CDate(Now.ToShortDateString)
                        End If

                        sTroutD = ""
                        sTroutD = PCCW.WasCaptured(sMonthlyTicketNumber, dtTrans, dblAmount, LogicDecrypt(mCust(iCounter).sCCNumber), iAction)
                        If sTroutD <> "" Then
                            PCCWTrans = PCCW.GetTrans(sTroutD)
                            sCCAuth = PCCWTrans.sAuth
                            sCCRef = PCCWTrans.sResultRef
                            sCCTicket = PCCWTrans.sTicket
                            sCCType = PCCWTrans.sIssuer
                            sCCNumber = PCCWTrans.sCardNumber
                            sCCExp = ""
                            sCCAmount = Format(PCCWTrans.dblAmount, "0.00")
                            mCust(iCounter).sCCResult = "(" & Now.ToShortDateString & ") " & PCCWTrans.sResult
                            mCust(iCounter).DateLastAttempt = CDate(Now.ToShortDateString)
                            mCust(iCounter).MonthlyDeclined = False

                            bSaveTicket = True
                            bSaveTrans = True

                        ElseIf mCust(iCounter).sPayerIdentifier.Contains("#GIFT#") = True Then

                            sPaymentType = "GIFT CARD"

                            sGCNumber = mCust(iCounter).sPayerIdentifier.Replace("#GIFT#", "")

                            If sGCNumber <> "" Then
                                dblGCBalance = GiftCard.GetGiftCardBalance(0, sGCNumber)

                                If dblGCBalance >= dblAmount Then

                                    CompleteGiftCardSale(False, "", sMonthlyTicketNumber, lMonthlyTicketId, sMonthlyTicketNumber, sGCNumber, "", dblAmount, False, "", False, lMonthlyTicketId)

                                    bSaveTicket = True
                                    bSaveTrans = False
                                Else

                                    iFailedCounter = iFailedCounter + 1
                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)

                                    mCust(iCounter).MonthlyDeclined = False
                                    mCust(iCounter).MonthlyActive = False
                                    Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE GC BALANCE", userMonthly.UserName, "", 0, sComputerName)
                                    bSaveTicket = False
                                    bSaveTrans = False


                                End If
                            End If



                        Else

                            Dim frmOfflineCCform As New clsCardProcessing(False, "", 60, True)
                            'frmOfflineCCform.TopMost = Me.TopMost
                            frmOfflineCCform.sLabel1 = "Processing Credit Card"
                            frmOfflineCCform.sLabel2 = Format(dblAmount, "0.00")


                            frmOfflineCCform.sCCAmount = Format(dblAmount, "0.00")


                            'frmOfflineCCform.sCCNumber = "F" & LogicDecrypt(mCust(iCounter).sCCNumber)

                            If iCreditMode < 4 Then
                                frmOfflineCCform.sCCNumber = LogicDecrypt(mCust(iCounter).sCCNumber)
                                frmOfflineCCform.sCCExp = mCust(iCounter).sCCExpDate

                                If mCust(iCounter).sCCType = "" Then mCust(iCounter).sCCType = GetCreditCardType(LogicDecrypt(mCust(iCounter).sCCNumber))
                                frmOfflineCCform.sCCType = mCust(iCounter).sCCType
                                frmOfflineCCform.sCCHolder = mCust(iCounter).sCCHolderName
                            End If

                            frmOfflineCCform.sFullCCTicket = sMonthlyTicketNumber
                            frmOfflineCCform.bRecurring = True


                            If iMaximumTicketLength > 0 Then
                                If sMonthlyTicketNumber.Length > iMaximumTicketLength Then
                                    frmOfflineCCform.sCCTicket = Mid(sMonthlyTicketNumber, sMonthlyTicketNumber.Length - iMaximumTicketLength + 1)
                                Else
                                    frmOfflineCCform.sCCTicket = sMonthlyTicketNumber
                                End If
                            Else
                                frmOfflineCCform.sCCTicket = sMonthlyTicketNumber
                            End If
                            'frmOfflineCCform.sCCTicket = sMonthlyTicketNumber
                            frmOfflineCCform.sCCTrack = "" 'LogicDecrypt(mCust(iCounter).sCCScanned)



                            frmOfflineCCform.sTransType = "SALE"

                            frmOfflineCCform.sCCCustomerId = mCust(iCounter).CustomerId.ToString
                            frmOfflineCCform.sCCCustomerName = mCust(iCounter).FirstName & " " & mCust(iCounter).LastName

                            frmOfflineCCform.Load()

                            If frmOfflineCCform.sReturn.ToUpper = "CAPTURED" Then ' Or frmOfflineCCform.sReturn.ToUpper = "ACCEPTED" Then
                                ' , , , , , , , 
                                iChargedCounter = iChargedCounter + 1

                                sCCAuth = frmOfflineCCform.sCCAuth
                                sCCRef = frmOfflineCCform.sCCRef
                                sCCTicket = frmOfflineCCform.sCCTicket
                                sCCType = frmOfflineCCform.sCCType
                                sTransRoutD = frmOfflineCCform.sCCTroutD
                                If Mid(frmOfflineCCform.sCCNumber, 1, 1) = "F" Then
                                    sCCNumber = Mid(frmOfflineCCform.sCCNumber, 2)
                                Else
                                    sCCNumber = frmOfflineCCform.sCCNumber
                                End If
                                sCCExp = frmOfflineCCform.sCCExp
                                'sCCTrack = "" 'frmOfflineCCform.sCCTrack
                                sCCAmount = frmOfflineCCform.sCCAmount


                                mCust(iCounter).sCCResult = "(" & Now.ToShortDateString & ") " & frmOfflineCCform.sCCResult
                                mCust(iCounter).DateLastAttempt = CDate(Now.ToShortDateString)
                                'mCust(iCounter).MonthlyDeclined = False
                                '  Customer.SaveMonthlyInfo(mCust(iCounter), usermonthly.username, usermonthly.userid)

                                Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 0, userMonthly.UserName, userMonthly.UserID)
                                Customer.SetMonthlyWash(mCust(iCounter).CustomerId, 0)

                                bSaveTicket = True
                                bSaveTrans = True
                            ElseIf frmOfflineCCform.sReturn.ToUpper = "DECLINE" Then
                                iFailedCounter = iFailedCounter + 1
                                'Me.lblTitle.Text = "Card was not captured."
                                mCust(iCounter).sCCResult = "(" & Now.ToShortDateString & ") " & frmOfflineCCform.sCCResult
                                mCust(iCounter).DateLastAttempt = CDate(Now.ToShortDateString)
                                mCust(iCounter).MonthlyActive = False
                                mCust(iCounter).MonthlyDeclined = True

                                iAttempts = Customer.GetPaymentAttempts(mCust(iCounter).CustomerId)
                                Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)

                                If mCust(iCounter).EmailOptInUnlimitedAutoCharge = 1 And Trim(mCust(iCounter).Email) <> "" Then
                                    If iAttempts > (iAutoChargeDeclineAttempts - 1) Then
                                        iAttempts = 0
                                    End If

                                    If sDeclineEmailAPIURL = "" Then
                                        For i = 0 To sDeclineEmailDays.Length - 1
                                            If Val(sDeclineEmailDays(i)) > 0 And Val(sDeclineEmailDays(i)) = iAttempts + 1 Then
                                                sCustomerMessage = Account.GetMessage(1, iAttempts + 1, lMessageType)
                                                Exit For
                                            End If
                                        Next
                                    End If

                                    If sCustomerMessage <> "" Then
                                        lCUTId = 0
                                        lCUTId = Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE DECLINE EMAIL: ", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                        sEmailResponse = EmailCustomerMessage(sCustomerMessage, mCust(iCounter).Email, lCUTId, True, mCust(iCounter).Barcode)
                                        'If lCUTId > 0 Then
                                        '    Customer.UpdateTransaction(lCUTId, "AUTOCHARGE DECLINE EMAIL: " & sEmailResponse)
                                        'Else
                                        '    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE DECLINE EMAIL: " & sEmailResponse, userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                        'End If
                                    End If
                                End If

                                If iAttempts > (iAutoChargeDeclineAttempts - 1) Then
                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE: ENTER DECLINE MODE", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                ElseIf iAttempts = (iAutoChargeDeclineAttempts - 1) Then
                                    mCust(iCounter).MonthlyActive = False
                                    mCust(iCounter).MonthlyDeclined = False 'no more attempts

                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, iAttempts + 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)

                                    Customer.SaveManagedPayerData(mCust(iCounter).CustomerId, "", Now, "", "", "", "", "", "", "", "", False, iCreditMode, bManualFamilyAccountVerification, True, userMonthly.UserName, sComputerName) 'delete token
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE DECLINE", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                ElseIf iAttempts > 0 Then
                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, iAttempts + 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE RE-TRY", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                Else
                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, iAttempts + 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE: ENTER DECLINE MODE", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                End If

                                'Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                'Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE DECLINE", "LWREPORTS", sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                'Else
                                bSaveTicket = False
                                bSaveTrans = False
                                Ticket.UpdateMonthlyTicketCounter()
                            ElseIf frmOfflineCCform.sReturn.ToUpper = "ERROR" Then
                                iFailedCounter = iFailedCounter + 1
                                'Me.lblTitle.Text = "Card was not captured."
                                mCust(iCounter).sCCResult = "(" & Now.ToShortDateString & ") " & frmOfflineCCform.sCCResult
                                mCust(iCounter).DateLastAttempt = CDate(Now.ToShortDateString)
                                mCust(iCounter).MonthlyDeclined = False

                                iAttempts = Customer.GetPaymentAttempts(mCust(iCounter).CustomerId)

                                If iAttempts > (iAutoChargeDeclineAttempts - 1) Then
                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE ERROR", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                ElseIf iAttempts = (iAutoChargeDeclineAttempts - 1) Then
                                    mCust(iCounter).MonthlyActive = False
                                    mCust(iCounter).MonthlyDeclined = False 'no more attempts

                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, iAttempts + 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE DECLINE", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                Else
                                    Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, iAttempts + 1, userMonthly.UserName, userMonthly.UserID)
                                    Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE ERROR", userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                End If

                                'Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                'Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE ERROR", "LWREPORTS", sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                                'Else
                                bSaveTicket = False
                                bSaveTrans = False
                                Ticket.UpdateMonthlyTicketCounter()
                            Else
                                iFailedCounter = iFailedCounter + 1

                                mCust(iCounter).sCCResult = "(" & Now.ToShortDateString & ") " & frmOfflineCCform.sCCResult
                                mCust(iCounter).DateLastAttempt = CDate(Now.ToShortDateString)
                                mCust(iCounter).MonthlyDeclined = False
                                Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                                Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE OTHER", "LWREPORTS", sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)

                                iAttempts = Customer.GetPaymentAttempts(mCust(iCounter).CustomerId)
                                Customer.SetPaymentAttempts(mCust(iCounter).CustomerId, iAttempts + 1, userMonthly.UserName, userMonthly.UserID)

                                'Else
                                bSaveTicket = False
                                bSaveTrans = False
                                Ticket.UpdateMonthlyTicketCounter()
                            End If
                        End If
                    End If
                ElseIf MonthlyAcctInfo.ChargeFrequency = 0 Or mCust(iCounter).MonthlyActive = False Then
                    bSaveTicket = False
                    bSaveTrans = False
                Else

                    'sMonthlyTicketNumber = GetUnlimitedTicketNumber(mCust(iCounter), False)
                    dblAmount = 0
                    dblTaxes = 0
                    bSaveTicket = True
                    bSaveTrans = False '    Customer.SaveMonthlyInfo(mCust(iCounter), usermonthly.username, usermonthly.userid)
                End If

                If bSaveTicket = True Then
                    'Dim lMonthlyTicketId As Long = 0
                    bCloseshift = True
                    ''usermonthly.userid = User.GetAutoChargeId
                    'sMonthlyTicketNumber = GetUnlimitedTicketNumber(mCust(iCounter), False)
                    'If MonthlyAcctInfo.SurchargeId > 0 And mCust(iCounter).MonthlyPaid <= CDate("1/1/2000") Then
                    '    lMonthlyTicketId = SaveMonthlyTicket(sMonthlyTicketNumber, userMonthly.UserName, userMonthly.UserID, mCust(iCounter).CustomerId, 0, 0, "", userMonthly.UserID, dblAmount, MonthlyAcctInfo.SurchargeId, MonthlyAcctInfo.AccountTypeId, dblTaxes, "CREDIT CARD", "", "")
                    'Else
                    'lMonthlyTicketId = SaveMonthlyTicket(sMonthlyTicketNumber, userMonthly.UserName, userMonthly.UserID, mCust(iCounter).CustomerId, 0, 0, "", userMonthly.UserID, dblAmount, 0, MonthlyAcctInfo.AccountTypeId, dblTaxes, "CREDIT CARD", "", "")
                    'End If
                    If lMonthlyTicketId > 0 Then
                        CompleteMonthlyTicket(1, lMonthlyTicketId, sMonthlyTicketNumber, userMonthly.UserName, userMonthly.UserID, mCust(iCounter).CustomerId, 0, 0, "", userMonthly.UserID, dblAmount, 0, MonthlyAcctInfo.AccountTypeId, dblTaxes, sPaymentType, "", "")
                        Ticket.SetScanned(lMonthlyTicketId, True)
                    End If

                    If bSaveTrans = True Then
                        If sCCNumber = "" And (iCreditMode = 4 Or iCreditMode = 5 Or iCreditMode >= 8) And cMPDInfo.sSPAN <> "" Then
                            sCCNumber = "TOKEN (" & cMPDInfo.sSPAN & ")"
                        End If
                        If sCCType = "" And (iCreditMode = 4 Or iCreditMode = 5 Or iCreditMode >= 8) And cMPDInfo.sCardBrand <> "" Then
                            sCCType = cMPDInfo.sCardBrand
                        End If
                        sCardHolderName = mCust(iCounter).sCCHolderName
                        SaveMonthlyTransaction(lMonthlyTicketId, sCCAuth, sCCRef, sCCTicket, sCCType, sCCNumber, sCCExp, "", sCCAmount, userMonthly.UserID, sCardHolderName)
                    End If

                    If mCust(iCounter).MonthlyStart <= CDate("1/1/2000") Then
                        mCust(iCounter).MonthlyStart = CDate(Now.ToShortDateString)
                    End If

                    If mCust(iCounter).MonthlyNextPay <> CDate("1/1/1900") Then
                        mCust(iCounter).MonthlyRenewed = mCust(iCounter).MonthlyNextPay
                    Else
                        mCust(iCounter).MonthlyRenewed = mCust(iCounter).MonthlyStart
                        mCust(iCounter).MonthlyNextPay = mCust(iCounter).MonthlyStart
                    End If


                    If MonthlyAcctInfo.ChargeFrequency > 0 Then
                        'If bDisableUnlimitedBackCharge = True Then
                        If bDuplicate = True Then
                            mCust(iCounter).MonthlyPaid = CDate(LastMonthlyTicket.DateCreated.ToShortDateString)
                        Else
                            mCust(iCounter).MonthlyPaid = CDate(Now.ToShortDateString)
                        End If
                        mCust(iCounter).MonthlyRenewed = mCust(iCounter).MonthlyNextPay
                        mCust(iCounter).MonthlyNextPay = mCust(iCounter).MonthlyPaid + New TimeSpan(MonthlyAcctInfo.ChargeFrequency, 0, 0, 0)
                        ' mCust(iCounter).MonthlyRenewed = mCust(iCounter).MonthlyPaid
                        'Else
                        '    mCust(iCounter).MonthlyPaid = CDate(Now.ToShortDateString)
                        '    mCust(iCounter).MonthlyNextPay = mCust(iCounter).MonthlyNextPay + New TimeSpan(MonthlyAcctInfo.ChargeFrequency, 0, 0, 0)
                        'End If
                    Else
                        ' If bDisableUnlimitedBackCharge = True Then
                        If bDuplicate = True Then
                            mCust(iCounter).MonthlyPaid = CDate(LastMonthlyTicket.DateCreated.ToShortDateString)
                        Else
                            mCust(iCounter).MonthlyPaid = CDate(Now.ToShortDateString)
                        End If
                        mCust(iCounter).MonthlyRenewed = mCust(iCounter).MonthlyNextPay

                        'If bDisableUnlimitedBackCharge = True Or (bResetUnlimitedStartDateAfterDecline = True And mCust(iCounter).MonthlyDeclined = True) Or (mCust(iCounter).MonthlyNextPay.AddDays(iResetUnlimitedStartDateDaysAfterMonthlyNextPay) < CDate(Now.ToShortDateString)) Or (bResetUnlimitedStartDateAfterTimeOut = True And InStr(mCust(iCounter).sCCResult.ToUpper, "SEND") > 0 Or InStr(mCust(iCounter).sCCResult.ToUpper, "RESPONSE") > 0 Or InStr(mCust(iCounter).sCCResult.ToUpper, "CONNECT") > 0 Or InStr(mCust(iCounter).sCCResult.ToUpper, "TIMEOUT") > 0 Or mCust(iCounter).sCCResult.ToUpper = "TIMEOUT" Or mCust(iCounter).sCCResult.ToUpper = "IP INIT" Or mCust(iCounter).sCCResult.ToUpper = "IP CONNECTION ERROR") Then
                        If (bDisableUnlimitedBackCharge = True And mCust(iCounter).MonthlyNextPay.AddDays(15) < CDate(Now.ToShortDateString)) Or (bResetUnlimitedStartDateAfterDecline = True And mCust(iCounter).MonthlyDeclined = True) Or (mCust(iCounter).MonthlyNextPay.AddDays(iResetUnlimitedStartDateDaysAfterMonthlyNextPay) < CDate(Now.ToShortDateString)) Or (bResetUnlimitedStartDateAfterTimeOut = True And InStr(mCust(iCounter).sCCResult.ToUpper, "SEND") > 0 Or InStr(mCust(iCounter).sCCResult.ToUpper, "RESPONSE") > 0 Or InStr(mCust(iCounter).sCCResult.ToUpper, "CONNECT") > 0 Or InStr(mCust(iCounter).sCCResult.ToUpper, "TIMEOUT") > 0 Or mCust(iCounter).sCCResult.ToUpper = "TIMEOUT" Or mCust(iCounter).sCCResult.ToUpper = "IP INIT" Or mCust(iCounter).sCCResult.ToUpper = "IP CONNECTION ERROR") Then

                            If mCust(iCounter).MonthlyStart > CDate("1/1/2000") Then
                                If IsDate(mCust(iCounter).MonthlyStart.Month & "/" & Now.Day & "/" & mCust(iCounter).MonthlyStart.Year) = True Then
                                    mCust(iCounter).MonthlyStart = CDate(mCust(iCounter).MonthlyStart.Month & "/" & Now.Day & "/" & mCust(iCounter).MonthlyStart.Year)
                                Else
                                    mCust(iCounter).MonthlyStart = CDate(mCust(iCounter).MonthlyStart.AddMonths(1).Month & "/1/" & mCust(iCounter).MonthlyStart.Year).AddDays(-1)
                                    ' mCust(iCounter).MonthlyStart = CDate(Now.ToShortDateString)
                                End If
                            Else
                                mCust(iCounter).MonthlyStart = CDate(Now.ToShortDateString)
                            End If

                            mCust(iCounter).MonthlyNextPay = GetNextMonthlyPaymentDate(CDate(mCust(iCounter).MonthlyPaid.ToShortDateString), 0, 0, Not bDuplicate, MonthlyAcctInfo.iMonthlyChargeDay, MonthlyAcctInfo.ChargeFrequency, mCust(iCounter), MonthlyAcctInfo, False, False) 'disable special pricing for all autocharges even if not paid before

                        Else
                            mCust(iCounter).MonthlyNextPay = GetNextMonthlyPaymentDate(CDate(mCust(iCounter).MonthlyPaid.ToShortDateString), mCust(iCounter).MonthlyStart.Day, 0, Not bDuplicate, MonthlyAcctInfo.iMonthlyChargeDay, MonthlyAcctInfo.ChargeFrequency, mCust(iCounter), MonthlyAcctInfo, False, False) 'disable special pricing for all autocharges even if not paid before
                        End If

                        'mCust(iCounter).MonthlyRenewed = mCust(iCounter).MonthlyPaid
                        '    Else
                        '    mCust(iCounter).MonthlyPaid = CDate(Now.ToShortDateString)
                        '    mCust(iCounter).MonthlyNextPay = GetNextMonthlyPaymentDate(CDate(mCust(iCounter).MonthlyRenewed.ToShortDateString))
                        'End If
                    End If

                    'Call PrintCreditCardReceipt(lMonthlyTicketId, Val(frmOfflineCCform.sCCAmount), frmOfflineCCform.sCCRef, frmOfflineCCform.sCCAuth)

                    ' Call frmCashierForm.PrintTicket(lMonthlyTicketId, False)


                    mCust(iCounter).MonthlyActive = True
                    mCust(iCounter).MonthlyDeclined = False

                    lReturnId = Customer.SaveMonthlyInfo(mCust(iCounter), userMonthly.UserName, userMonthly.UserID, AccountInfo.LocationDesc)
                    Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE", "LWREPORTS", sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)

                    'If lReturnId = -1 And sTransRoutD <> "" Then
                    '    'Void the autocharge transaction since we couldn't update the montly account info and 
                    '    'we'll try to recharge the next day when processing monthlys again
                    '    If lMonthlyTicketId > 0 Then Ticket.DeleteTicket(lMonthlyTicketId.ToString, False, "AUTOCHARGE VOID")

                    '    frmCreditCardForm.ResetCreditCard()
                    '    frmCreditCardForm.lblReferenceNumber.Text = sMonthlyTicketNumber
                    '    frmCreditCardForm.lblReferenceID.Text = sMonthlyTicketNumber
                    '    frmCreditCardForm.lblReferenceID.Tag = lMonthlyTicketId
                    '    frmCreditCardForm.lblChargeAmount.Text = Format(dblAmount, "0.00")
                    '    'If bReversed = True Then
                    '    frmCreditCardForm.lblTitle.Text = "CREDIT CARD VOID"
                    '    frmCreditCardForm.sTransType = "VOID"
                    '    frmCreditCardForm.sTransRoutD = sTransRoutD
                    '    frmCreditCardForm.lblCreditCardType.Text = ""
                    '    frmCreditCardForm.lblCardNumber.Text = "0000000000000000"
                    '    frmCreditCardForm.lblCardNumber.Tag = "0000000000000000"
                    '    frmCreditCardForm.lblExpDate.Text = "0000"
                    '    'Else
                    '    'frmCreditCardForm.lblTitle.Text = "CREDIT CARD REFUND"
                    '    ' frmCreditCardForm.sTransType = "REFUND"
                    '    ' frmCreditCardForm.lblCreditCardType.Text = ""
                    '    'End If

                    '    frmCreditCardForm.btnDebit.Visible = False
                    '    frmCreditCardForm.btnDebit.Tag = 0

                    '    frmCreditCardForm.Enabled = True
                    '    'stroutd = CreditCard.gettransactionid(lTicketId).tostring
                    '    'frmCreditCardForm.SendVoidCommand(stroutd)
                    '    If iMaximumTicketLength > 0 Then
                    '        If frmCreditCardForm.lblReferenceID.Text.Length > iMaximumTicketLength Then
                    '            frmCreditCardForm.lblReferenceID.Text = Mid(frmCreditCardForm.lblReferenceID.Text, frmCreditCardForm.lblReferenceID.Text.Length - iMaximumTicketLength + 1)
                    '        End If
                    '    End If
                    '    'frmCreditCardForm.ResetCreditCard()

                    '    frmCreditCardForm.btnCardOnFile.Visible = False
                    '    frmCreditCardForm.ShowDialog()
                    '    'If frmCreditCardForm.bResponse = False Then Return -1
                    'Else
                    'Customer monthly info updated successfully and auto charge successful.
                    'Send an unlimited autocharge email to customer if email on file and
                    'iEmailOptInUnlimitedAutoCharge is 1 and configured to send unlimited auto charge emails
                    If mCust(iCounter).EmailOptInUnlimitedAutoCharge = 1 And Trim(mCust(iCounter).Email) <> "" Then
                        lcutid = 0
                        lcutid = Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE EMAIL: " & sEmailResponse, userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                        sEmailResponse = EmailTicketIfConfigured(lMonthlyTicketId, True, True, True, mCust(iCounter).Email, lcutid)
                        If lCUTId > 0 Then
                            Customer.UpdateTransaction(lCUTId, "AUTOCHARGE EMAIL: " & sEmailResponse)
                        Else
                            Customer.ExecMonthlyTransaction(mCust(iCounter).CustomerId, "AUTOCHARGE EMAIL: " & sEmailResponse, userMonthly.UserName, sMonthlyTicketNumber, lMonthlyTicketId, sComputerName)
                        End If
                    End If
                    'End If


                Else
                    If lMonthlyTicketId > 0 Then
                        'iPaid of 2 = Failed on autocharge
                        CompleteMonthlyTicket(2, lMonthlyTicketId, sMonthlyTicketNumber, userMonthly.UserName, userMonthly.UserID, mCust(iCounter).CustomerId, 0, 0, "", userMonthly.UserID, dblAmount, 0, MonthlyAcctInfo.AccountTypeId, dblTaxes, sPaymentType, "", "")
                    End If

                End If


                iCounter = iCounter + 1

                System.Threading.Thread.Sleep(500)
            Loop

            If bCloseshift = True And bCloseUnlimitedShift = True Then
                lClosedShiftNumber = CloseShift(userMonthly.UserID, userMonthly.UserName)

                'If bPrintUnlimitedReport = True Then

                '    sFrom = Now.ToShortDateString & " 00:00:00"
                '    sTo = Now.ToShortDateString & " 23:59:59"
                '    'Call m_RunFinancialReport(frmstatus1.lblUserID.Tag, sFrom, sTo, sFrom, sTo)

                '    CashierReport = ReportDB.GetCashierReport

                '    If CashierReport.FileName <> "" And CashierReport.PrinterName <> "" Then

                '        frmPrintStatusForm.Label1.Text = "Printing"
                '        frmPrintStatusForm.lblChangeDue.Text = "Report."
                '        frmPrintStatusForm.Show()
                '        Application.DoEvents()


                '        'Call CalculateCashierReport(userMonthly.UserID, dtOpened, True)

                '        If CashierReport.PrinterName = "Ticket Printer" Then 'Or CashierReport.PrinterName = "" Then

                '            'Call CalculateCashierReport(9999)
                '            Call PrintCashierReport(userMonthly.UserID, Now, True, lClosedShiftNumber, CashierReport.FileName)
                '        Else
                '            'Call CalculateCashierReport(9999)
                '            dsReport = frmLeftReportsForm.GetDailyFinancial(My.Application.Info.DirectoryPath & "\Reports\" & CashierReport.FileName, Now, userMonthly.UserID, lClosedShiftNumber, True, "Shift Report")

                '            If dsReport Is Nothing Then
                '                dsReport = frmLeftReportsForm.GetDailyFinancial(My.Application.Info.DirectoryPath & "\Reports\" & CashierReport.FileName, Now, userMonthly.UserID, lClosedShiftNumber, False, "Shift Report")
                '            End If
                '            If dsReport.Tables(0).Rows.Count > 0 Then mlaPrint.PrintNow(dsReport, 1)

                '        End If

                '        frmPrintStatusForm.Hide()

                '    End If



                'End If


                System.Threading.Thread.Sleep(500)
                'frmPrintStatusForm.Hide()
            End If

            sStatus = "ProcessMonthlyAccounts ended"

            Customer.ExecMonthlyTransaction(0, "AUTOCHARGE SUMMARY: " & iChargedCounter & " Successful, " & iFailedCounter & " Failed ", userMonthly.UserName, "", 0, sComputerName)


        Catch ex As Exception
            Customer.ExecMonthlyTransaction(0, "AUTOCHARGE ERROR SUMMARY: " & iChargedCounter & " Successful, " & iFailedCounter & " Failed ", userMonthly.UserName, "", 0, sComputerName)

            WriteToLog(sLogFileName, "ProcessMonthlyAccounts", 0, ex.Message, "ERROR")
            sStatus = "ProcessMonthlyAccounts ended"
        End Try
    End Sub

    Public Sub ProcessPayProsConversion()
        Dim cCust(0) As CustomerProperties
        Dim i As Integer = 0
        Dim sNum As String = ""
        Dim sCCHolderName As String = ""

        Dim sAuthCode As String = ""
        Dim sResult As String = ""
        Dim sReferenceNumber As String = ""
        Dim lLogId As Long = 0
        Dim sTicket As String = ""
        Dim sResponseText As String = ""
        Dim sAuthAmount As String = ""
        Dim sTransAmount As String = ""
        Dim sTroutd As String = ""
        Dim sVoidTroutD As String = ""

        Dim sExpireMonth As String = ""
        Dim sExpireYear As String = ""
        Dim sTicketNumber As String = ""
        Dim sFullTicketNumber As String = ""

        Dim sPayerIdentifier As String = ""

        Dim iCapturedCount As Integer = 0
        Dim iNotCapturedCount As Integer = 0

        Dim sExpDate As String = ""
        Try

            cCust = Customer.GetMonthlyAccounts(AccountInfo.LocationDesc, True, True, False)

            iCapturedCount = 0
            iNotCapturedCount = 0

            'frmSetupContentForm.Label1.Text = cCust.Length & " accounts found."
            'frmSetupContentForm.Label1.Refresh()

            For i = 0 To cCust.Length - 1
                If IsNothing(cCust(i)) Then Exit For


                'frmSetupContentForm.Label2.Text = "Processing " & i + 1 & " of " & cCust.Length
                'frmSetupContentForm.Label2.Refresh()

                If cCust(i).CustomerId > 0 And cCust(i).MonthlyActive = True And cCust(i).sCCNumber <> "" And cCust(i).sCCExpDate <> "" Then

                    sNum = LogicDecrypt(cCust(i).sCCNumber)

                    sCCHolderName = cCust(i).sCCHolderName

                    If sNum <> "" Then

                        sTicketNumber = GetUnlimitedTicketNumber(cCust(i), False)

                        sFullTicketNumber = sTicketNumber

                        If iMaximumTicketLength > 0 Then
                            If sTicketNumber.Length > iMaximumTicketLength Then
                                sTicketNumber = Mid(sTicketNumber, sTicketNumber.Length - iMaximumTicketLength + 1)
                            End If
                            'Else
                            '    frmOfflineCCform.sCCTicket = sTicketNumber
                        End If

                        sExpDate = cCust(i).sCCExpDate

                        If sExpDate.Length > 3 Then
                            sExpireMonth = Mid(sExpDate, 1, 2)
                            sExpireYear = Mid(sExpDate, sExpDate.Length - 1, 2)
                        End If

                        SendPayGateway("SALE", "0.00", sNum, sExpireMonth, sExpireYear, 0, AccountInfo.AccountToken, False, sTicketNumber, sResult, sAuthCode, sTroutd, sResponseText, sReferenceNumber, lLogId, cCust(i).CustomerId.ToString, sVoidTroutD, "", "", "", sPayerIdentifier, "", "", 0, "", "", sCCHolderName, sExpDate, True, False, False, "", sFullTicketNumber, False)

                        If sPayerIdentifier <> "" Then
                            cCust(i).sCCExpDate = ""
                            cCust(i).sCCHolderName = ""
                            cCust(i).sCCNumber = ""
                            cCust(i).sCCType = ""

                            Customer.SaveMonthlyInfo(cCust(i), "LWREPORTS", 9999, AccountInfo.LocationDesc)

                            'If sResult = "CAPTURED" Then
                            iCapturedCount = iCapturedCount + 1
                            'frmSetupContentForm.Label3.Text = "SUCCESS (" & iCapturedCount & ")"
                            'frmSetupContentForm.Label3.Refresh()

                            'If lLogId > 0 Then

                            '    sVoidTroutD = lLogId.ToString

                            '    SendPayGateWayVoidCommand(sVoidTroutD)
                            'End If

                        Else
                            iNotCapturedCount = iNotCapturedCount + 1
                            'frmSetupContentForm.Label4.Text = "FAILED (" & iNotCapturedCount & ")"
                            'frmSetupContentForm.Label4.Refresh()
                        End If


                    End If

                End If

            Next

            'frmSetupContentForm.Label2.Text = "Processing complete"
            'frmSetupContentForm.Label2.Refresh()



        Catch ex As Exception

        End Try
    End Sub

    Public Function GetUnlimitedTicketNumber(ByVal CustInfo As CustomerProperties, ByVal bToday As Boolean) As String
        Dim sTicket As String = "" 'sMonthlyTicketPrefix & Ticket.GetNextMonthlyNumber.ToString
        Dim sTemp As String = ""
        Try

            sTemp = Format(CustInfo.CustomerId, "00000")
            If sTemp.Length > 5 Then
                sTemp = Mid(CustInfo.CustomerId.ToString, CustInfo.CustomerId.ToString.Length - 4, 5)
            End If


            If bToday = True Then
                sTicket = Format(Now.Month, "00") & Format(Now.Day, "00")
                sTicket = sTicket & sTemp
                'sTicket = sTicket & Format(CustInfo.CustomerId, "00000")
            ElseIf CustInfo.MonthlyNextPay > CDate("1/1/1900") Then
                sTicket = Format(CustInfo.MonthlyNextPay.Month, "00") & Format(CustInfo.MonthlyNextPay.Day, "00")
                sTicket = sTicket & sTemp
            ElseIf CustInfo.MonthlyRenewed > CDate("1/1/1900") Then
                sTicket = Format(CustInfo.MonthlyRenewed.Month, "00") & Format(CustInfo.MonthlyRenewed.Day, "00")
                sTicket = sTicket & sTemp
            Else
                sTicket = Format(Now.Month, "00") & Format(Now.Day, "00")
                sTicket = sTicket & sTemp
                'sTicket = sTicket & Format(CustInfo.CustomerId, "00000")
            End If

            'sTemp = Format(123456789, "00000")
            'If sTemp.Length > 5 Then
            '    sTemp = Mid(123456789.ToString, 123456789.ToString.Length - 4, 5)
            'End If

            'sTicket = Format(CDate("2/5/09").Month, "00") & Format(CDate("2/5/09").Day, "00")
            'sTicket = sTicket & sTemp

            If sTicket = "" Then
                'sTicket = sMonthlyTicketPrefix & Ticket.GetNextMonthlyNumber.ToString
            End If

            Return sTicket
        Catch ex As Exception
            Return sTicket
        End Try
    End Function

    Public Function LogicEncrypt(ByVal sString As String) As String
        Try

            Dim Secret As String = sString
            Dim Key As String = "mlaboss"
            Dim p As Encryption.Symmetric.Provider = Encryption.Symmetric.Provider.TripleDES

            Dim sym As New Encryption.Symmetric(p)
            sym.Key.Text = Key


            Dim encryptedData As Encryption.Data
            encryptedData = sym.Encrypt(New Encryption.Data(Secret))


            Return encryptedData.Hex
            'Console.WriteLine("Encrypted String")
            'Console.WriteLine("  Hex     " & encryptedData.Hex)
            'Console.WriteLine("  Base64  " & encryptedData.Base64)
            'Console.WriteLine()

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function LogicDecrypt(ByVal sString As String) As String
        Try


            Dim p As Encryption.Symmetric.Provider = Encryption.Symmetric.Provider.TripleDES
            Dim sym As New Encryption.Symmetric(p)
            sym.Key.Text = "mlaboss"
            Dim encryptedData As New Encryption.Data '(sString)
            encryptedData.Hex = sString

            Dim decryptedData As Encryption.Data
            Dim sym2 As New Encryption.Symmetric(p)
            sym2.Key.Text = sym.Key.Text
            decryptedData = sym2.Decrypt(encryptedData)
            Return decryptedData.Text
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function GetCreditCardType(ByVal sCardNum As String) As String
        Try

            Select Case Mid(sCardNum, 1, 1)
                Case "3"
                    Return "AMEX"
                Case "4"
                    Return "VISA"
                Case "5"
                    Return "MC"
                Case "6"
                    Return "DISC"
            End Select

            Return ""

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function GetSecureResponseFromPayGateway(ByVal CCResponse As CreditCardResponse) As SecurePaymentResponse
        Dim spResponse As New SecurePaymentResponse
        Try

            spResponse.SetBankApprovalCode(CCResponse.getBankApprovalCode)
            spResponse.SetBin(CCResponse.getBin)
            spResponse.SetCapturedAmount(CCResponse.getCapturedAmount)
            spResponse.SetExpireMonth(CCResponse.getExpireMonth)
            spResponse.SetExpireYear(CCResponse.getExpireYear)
            spResponse.SetManageUntil(CCResponse.getManageUntil)
            spResponse.SetMpdResponseCode(CCResponse.getMpdResponseCode)
            spResponse.SetMpdResponseCodeText(CCResponse.getMpdResponseCodeText)
            spResponse.SetPayerIdentifier(CCResponse.getPayerIdentifier)
            spResponse.SetReferenceId(CCResponse.getReferenceId)
            spResponse.SetRequestedAmount(CCResponse.getRequestedAmount)
            spResponse.SetResponseCode(CCResponse.getResponseCode)
            spResponse.SetResponseCodeText(CCResponse.getResponseCodeText)
            spResponse.SetSecondaryResponseCode(CCResponse.getSecondaryResponseCode)
            spResponse.SetSpan(CCResponse.getSpan)




            Return spResponse
        Catch ex As Exception
            Return spResponse
        End Try
    End Function

    Public Function GetRequestFromSecureRequest(ByVal spRequest As SecurePaymentRequest) As CreditCardRequest
        Dim ccRequest As New CreditCardRequest
        Try

            If spRequest.GetBillPostalCode <> "" Then ccRequest.setBillPostalCode(spRequest.GetBillPostalCode())
            If spRequest.GetChargeTotal <> "" Then ccRequest.setChargeTotal(spRequest.GetChargeTotal())
            If spRequest.GetChargeType <> "" Then ccRequest.setChargeType(spRequest.GetChargeType())
            If spRequest.GetCreditCardNumber <> "" Then ccRequest.setCreditCardNumber(spRequest.GetCreditCardNumber())
            If spRequest.GetExpireMonth <> "" Then ccRequest.setExpireMonth(spRequest.GetExpireMonth())
            If spRequest.GetExpireYear <> "" Then ccRequest.setExpireYear(spRequest.GetExpireYear())
            If spRequest.GetFullVoidFlag <> "" Then ccRequest.setFullVoidFlag(spRequest.GetFullVoidFlag())
            If spRequest.GetIndustry <> "" Then ccRequest.setIndustry(spRequest.GetIndustry())
            If spRequest.GetInvoiceNumber <> "" Then ccRequest.setInvoiceNumber(spRequest.GetInvoiceNumber())
            If spRequest.GetManagePayerData <> "" Then ccRequest.setManagePayerData(spRequest.GetManagePayerData())
            If spRequest.GetOrderId <> "" Then ccRequest.setOrderId(spRequest.GetOrderId())
            If spRequest.GetPartialApprovalFlag <> "" Then ccRequest.setPartialApprovalFlag(spRequest.GetPartialApprovalFlag())
            If spRequest.GetPayerIdentifier <> "" Then ccRequest.setPayerIdentifier(spRequest.GetPayerIdentifier())
            If spRequest.GetPurchaseOrderNumber <> "" Then ccRequest.setPurchaseOrderNumber(spRequest.GetPurchaseOrderNumber())
            If spRequest.GetSecurePrimaryAccountNumber <> "" Then ccRequest.setSecurePrimaryAccountNumber(spRequest.GetSecurePrimaryAccountNumber())
            If spRequest.GetTrack1 <> "" Then ccRequest.setTrack1(spRequest.GetTrack1())
            If spRequest.GetTrack2 <> "" Then ccRequest.setTrack2(spRequest.GetTrack2())
            If spRequest.GetTransactionConditionCode <> "" Then ccRequest.setTransactionConditionCode(spRequest.GetTransactionConditionCode())



            Return ccRequest
        Catch ex As Exception
            Return ccRequest
        End Try
    End Function

    Public Function GetResponseString(ByVal CCResponse As CreditCardResponse) As String
        Dim sResponse As String = "SecurePaymentResponse"
        Try

            sResponse = sResponse & ",BankApprovalCode= " & CCResponse.getBankApprovalCode
            sResponse = sResponse & ",Bin= " & CCResponse.getBin
            sResponse = sResponse & ",CapturedAmount= " & CCResponse.getCapturedAmount
            sResponse = sResponse & ",ExpireMonth= " & CCResponse.getExpireMonth
            sResponse = sResponse & ",ExpireYear= " & CCResponse.getExpireYear
            sResponse = sResponse & ",ManageUntil= " & CCResponse.getManageUntil
            sResponse = sResponse & ",MpdResponseCode= " & CCResponse.getMpdResponseCode
            sResponse = sResponse & ",MpdResponseCodeText= " & CCResponse.getMpdResponseCodeText
            sResponse = sResponse & ",PayerIdentifier= " & CCResponse.getPayerIdentifier
            sResponse = sResponse & ",ReferenceId= " & CCResponse.getReferenceId
            sResponse = sResponse & ",RequestedAmount= " & CCResponse.getRequestedAmount
            sResponse = sResponse & ",ResponseCode= " & CCResponse.getResponseCode
            sResponse = sResponse & ",ResponseCodeText= " & CCResponse.getResponseCodeText
            sResponse = sResponse & ",SecondaryResponseCode= " & CCResponse.getSecondaryResponseCode
            sResponse = sResponse & ",Span= " & CCResponse.getSpan




            Return sResponse
        Catch ex As Exception
            Return sResponse
        End Try
    End Function

    Public Function GetRequestString(ByVal spRequest As SecurePaymentRequest) As String
        Dim sRequest As String = "SecurePaymentRequest"
        Try

            If spRequest.GetBillPostalCode <> "" Then sRequest = ",BillPostalCode= " & spRequest.GetBillPostalCode()
            If spRequest.GetChargeTotal <> "" Then sRequest = sRequest & ",ChargeTotal= " & spRequest.GetChargeTotal()
            If spRequest.GetChargeType <> "" Then sRequest = sRequest & ",ChargeType= " & spRequest.GetChargeType()
            If spRequest.GetCreditCardNumber <> "" Then
                If spRequest.GetCreditCardNumber.Length > 4 Then
                    sRequest = sRequest & ",CreditCardNumber= XXXXXXXX" & Mid(spRequest.GetCreditCardNumber(), spRequest.GetCreditCardNumber.Length - 3)
                Else
                    sRequest = sRequest & ",CreditCardNumber= " & spRequest.GetCreditCardNumber()
                End If
            End If
            If spRequest.GetExpireMonth <> "" Then sRequest = sRequest & ",ExpireMonth= " & spRequest.GetExpireMonth()
            If spRequest.GetExpireYear <> "" Then sRequest = sRequest & ",ExpireYear= " & spRequest.GetExpireYear()
            If spRequest.GetFullVoidFlag <> "" Then sRequest = sRequest & ",FullVoidFlag= " & spRequest.GetFullVoidFlag()
            If spRequest.GetIndustry <> "" Then sRequest = sRequest & ",Industry= " & spRequest.GetIndustry()
            If spRequest.GetInvoiceNumber <> "" Then sRequest = sRequest & ",InvoiceNumber= " & spRequest.GetInvoiceNumber()
            If spRequest.GetManagePayerData <> "" Then sRequest = sRequest & ",ManagePayerData= " & spRequest.GetManagePayerData()
            If spRequest.GetOrderId <> "" Then sRequest = sRequest & ",OrderId= " & spRequest.GetOrderId()
            If spRequest.GetPartialApprovalFlag <> "" Then sRequest = sRequest & ",PartialApprovalFlag= " & spRequest.GetPartialApprovalFlag()
            If spRequest.GetPayerIdentifier <> "" Then sRequest = sRequest & ",PayerIdentifier= " & spRequest.GetPayerIdentifier()
            If spRequest.GetPurchaseOrderNumber <> "" Then sRequest = sRequest & ",PurchaseOrderNumber= " & spRequest.GetPurchaseOrderNumber()
            If spRequest.GetSecurePrimaryAccountNumber <> "" Then sRequest = sRequest & ",SecurePrimaryAccountNumber= " & spRequest.GetSecurePrimaryAccountNumber()
            If spRequest.GetTrack1 <> "" Then
                sRequest = sRequest & ",Track1= Track1Data"
            Else
                sRequest = sRequest & ",Track1= "
            End If
            If spRequest.GetTrack2 <> "" Then
                sRequest = sRequest & ",Track2= Track2Data"
            Else
                sRequest = sRequest & ",Track2= "
            End If

            If spRequest.GetTransactionConditionCode <> "" Then sRequest = sRequest & ",TransactionConditionCode= " & spRequest.GetTransactionConditionCode()



            Return sRequest
        Catch ex As Exception
            Return sRequest
        End Try
    End Function

    Public Function SendPayGateway(ByVal sChargeType As String, ByVal sChargeTotal As String, ByVal sCardNumber As String, ByVal sExpireMonth As String, ByVal sExpireYear As String, ByVal lTicketId As Long, ByVal sAccountToken As String, ByVal bVoid As Boolean, ByVal sTicketNumber As String, ByRef sResult As String, ByRef sAuthCode As String, ByRef sTroutd As String, ByRef sResponseText As String, ByRef sReferenceNumber As String, ByRef lLogId As Long, ByVal sCCCustomerId As String, ByRef sVoidTroutD As String, ByVal sTrack1Data As String, ByVal sTrack2Data As String, ByVal sZipCode As String, ByRef sPayerIdentifier As String, ByRef sSpan As String, ByRef sCardBrand As String, ByRef iMpdResponseCode As Integer, ByRef sBIN As String, ByRef sResultsText As String, ByVal sCCHolderName As String, ByVal sExpDate As String, ByVal bTokenConversion As Boolean, ByVal bOffline As Boolean, ByVal bManualPrompt As Boolean, ByVal sTrackData As String, ByVal sFullTicketNumber As String, ByVal bRecurring As Boolean) As Boolean

        'Dim sSignature As String = ""
        'Dim sAuthCode As String = ""
        'Dim sResult As String = ""
        'Dim sReferenceNumber As String = ""
        'Dim lLogId As Long = 0
        Dim sTicket As String = ""
        'Dim sResponseText As String = ""
        Dim sAuthAmount As String = ""
        Dim sTransAmount As String = ""
        'Dim sTroutd As String = ""
        ''Dim sRec As String = ""

        Dim CCRequest As New CreditCardRequest()
        Dim TRClient As New TransactionClient()
        Dim CCResponse As CreditCardResponse
        'Dim I As Integer
        'Dim sResultsText As String = ""

        Dim tsTime As TimeSpan

        Dim sOrderID As String = ""
        Dim sCCDisplayNumber As String = ""

        Dim iResponseCode As Integer = 0

        Dim CreditCardLogInfo As New CreditCardLogProperties

        'Dim iMpdResponseCode As Integer = 0
        Dim sMpdResponseCodeText As String = ""
        'Dim sPayerIdentifier As String = ""
        Dim dtManageUntil As Date = CDate("1/1/1900")
        'Dim sSpan As String = ""
        Dim sSpanMonth As String = ""
        Dim sSpanYear As String = ""
        Dim lMPDId As Long = 0
        Dim cMPDInfo As New CustomerManagedPayerData

        Dim dtSent As Date = Now
        Dim dtReceived As Date = Now

        Dim sSpan2 As String = ""
        Dim sBIN2 As String = ""

        'Dim sMyIp As String = ""
        'Dim sZipCode As String = "07869"
        Dim bDisablePartialApproval As Boolean = False

        Dim CCSecureResponse As New SecurePaymentResponse
        Dim CCSecureRequest As New SecurePaymentRequest

        Dim CCSignatureRequest As New SecurePaymentRequest
        Dim CCSignatureResponse As New SecurePaymentResponse

        Dim bSilentProcessing As Boolean = False
        Dim bTokenDelete As Boolean = False
        Dim bTokenUpdate As Boolean = False

        Dim bDuplicateCheck As Boolean = True

        Dim bEnablePartialAuthorizations As Boolean = False
        Try



            If sCardNumber.Length > 4 Then
                sCCDisplayNumber = Mid(sCardNumber, 1, 4) & "...." & Mid(sCardNumber, sCardNumber.Length - 3, 4)
            Else
                sCCDisplayNumber = sCardNumber
            End If

            If sFullTicketNumber = "" Then
                sFullTicketNumber = sTicketNumber
            End If



            CreditCardLogInfo.Clear()
            CreditCardLogInfo.lTicketId = lTicketId
            CreditCardLogInfo.sTicketNumber = sTicketNumber
            CreditCardLogInfo.sCCNumber = sCCDisplayNumber
            CreditCardLogInfo.Amount = Val(sChargeTotal)
            CreditCardLogInfo.sTransactionType = sChargeType
            CreditCardLogInfo.lCreditCardLogId = CreditCard.CreateLog(CreditCardLogInfo.lTicketId, CreditCardLogInfo.sTicketNumber, CreditCardLogInfo.sCCNumber, CreditCardLogInfo.Amount, "LWREPORTS", CreditCardLogInfo.sTransactionType)
            sOrderID = CreditCardLogInfo.lCreditCardLogId.ToString
            lLogId = CreditCardLogInfo.lCreditCardLogId

            'sAccountToken = "FDC4E33FCE6C54083389EDF18D50AF64C3C0259544B6C3FFEEA64B268DDD5FEB9E52BE59D7BDC8EC72"

            'If sChargeType = "REFUND" Then sChargeType = "CREDIT"
            If sChargeType = "ALIASUPDATE" Then
                bTokenUpdate = True
            ElseIf sChargeType = "DELETE_CUSTOMER" Then
                bTokenDelete = True
            ElseIf sChargeType = "REFUND" Then
                sChargeType = "CREDIT"
            ElseIf sChargeType = "DEBITREFUND" Then
                sChargeType = "REFUND"
            End If

            CCSecureRequest.SetChargeType(sChargeType)
            CCSecureRequest.SetChargeTotal(sChargeTotal)
            'CCSecureRequest.setCreditCardNumber(sCardNumber)
            'CCSecureRequest.setExpireMonth(sExpireMonth)
            CCSecureRequest.SetInvoiceNumber(sTicketNumber)


            If sExpireYear <> "" Then
                If Now.Year > CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2) & sExpireYear) And CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2)) > 84 And CInt(sExpireYear) < 16 Then
                    sExpireYear = (CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2)) + 1).ToString & sExpireYear
                Else
                    sExpireYear = Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2) & sExpireYear
                End If
            End If
            If sCCCustomerId <> "" Then

                CCSecureRequest.SetManagePayerData("true") 'True)
                cMPDInfo = Customer.GetManagedPayerData(CLng(sCCCustomerId))


                If cMPDInfo.sBarcode <> "" Then CCSecureRequest.SetPurchaseOrderNumber(cMPDInfo.sBarcode)


                'If cMPDInfo.lCustomerId > 0 And cMPDInfo.sPayerIdentifier <> "" And cMPDInfo.sExpireMonth <> "" And cMPDInfo.sExpireYear <> "" And cMPDInfo.sSPAN <> "" Then

                '    CCSecureRequest.setPayerIdentifier(cMPDInfo.sPayerIdentifier)

                '    If sTrack2Data = "" Then
                '        CCSecureRequest.setTransactionConditionCode(6)
                '        CCSecureRequest.setIndustry(CreditCardRequest.DIRECT_MARKETING)
                '    End If

                '    CCSecureRequest.setSecurePrimaryAccountNumber(cMPDInfo.sSPAN)
                '    CCSecureRequest.setExpireMonth(cMPDInfo.sExpireMonth)
                '    CCSecureRequest.setExpireYear(cMPDInfo.sExpireYear)
                '    If sCardBrand = "" Then sCardBrand = cMPDInfo.sCardBrand

                '    sSpan = cMPDInfo.sSPAN
                '    sBIN = cMPDInfo.sBIN


                'Else
                '    'CCSecureRequest.setCreditCardNumber(sCardNumber)
                '    'CCSecureRequest.setExpireMonth(sExpireMonth)
                '    'CCSecureRequest.setExpireYear(sExpireYear)
                '    'If sTrack2Data <> "" Then CCSecureRequest.setTrack2(sTrack2Data)
                'End If
                If cMPDInfo.lCustomerId > 0 And (sCardNumber = "" Or (sCardNumber <> "" And sChargeType = "ALIASUPDATE")) And cMPDInfo.sPayerIdentifier <> "" And ((cMPDInfo.sExpireMonth <> "" And cMPDInfo.sExpireYear <> "" And cMPDInfo.sSPAN <> "") Or (iCreditMode = 10) Or (iCreditMode = 6) Or (iCreditMode = 7)) Then

                    bDuplicateCheck = False
                    bSilentProcessing = True
                    CCSecureRequest.SetPayerIdentifier(cMPDInfo.sPayerIdentifier)

                    If sTrack2Data = "" Then
                        CCSecureRequest.SetTransactionConditionCode(6)
                        CCSecureRequest.SetIndustry(CreditCardRequest.DIRECT_MARKETING)
                    End If

                    CCSecureRequest.SetSecurePrimaryAccountNumber(cMPDInfo.sSPAN)
                    CCSecureRequest.SetExpireMonth(cMPDInfo.sExpireMonth)
                    CCSecureRequest.SetExpireYear(cMPDInfo.sExpireYear)
                    If sCardBrand = "" Then sCardBrand = cMPDInfo.sCardBrand

                    sSpan = cMPDInfo.sSPAN
                    sBIN = cMPDInfo.sBIN

                    bDisablePartialApproval = True
                ElseIf sCardNumber <> "" Then
                    bDuplicateCheck = False
                    'CCSecureRequest.setCreditCardNumber(sCardNumber)
                    'CCSecureRequest.setExpireMonth(sExpireMonth)
                    'CCSecureRequest.setExpireYear(sExpireYear)
                    'If sTrack2Data <> "" Then CCSecureRequest.setTrack2(sTrack2Data)
                End If

                If sCardNumber <> "" Then CCSecureRequest.SetCreditCardNumber(sCardNumber)
                If sExpireMonth <> "" Then CCSecureRequest.SetExpireMonth(sExpireMonth)
                If sExpireYear <> "" Then CCSecureRequest.SetExpireYear(sExpireYear)

                If sExpDate <> "" Then CCSecureRequest.SetExpDate(sExpDate)

                If sTrack1Data <> "" Then CCSecureRequest.SetTrack1(sTrack1Data)
                If sTrack2Data <> "" Then CCSecureRequest.SetTrack2(sTrack2Data)
                If sTrackData <> "" Then CCSecureRequest.SetTrack2(sTrackData)

            Else

                If sCardNumber <> "" Then CCSecureRequest.SetCreditCardNumber(sCardNumber)
                If sExpireMonth <> "" Then CCSecureRequest.SetExpireMonth(sExpireMonth)
                If sExpireYear <> "" Then CCSecureRequest.SetExpireYear(sExpireYear)
                If sExpDate <> "" Then CCSecureRequest.SetExpDate(sExpDate)

                If sTrack1Data <> "" Then CCSecureRequest.SetTrack1(sTrack1Data)
                If sTrack2Data <> "" Then CCSecureRequest.SetTrack2(sTrack2Data)
                If sTrackData <> "" Then CCSecureRequest.SetTrack2(sTrackData)

            End If

            If sChargeType = "VOID" Then
                CCSecureRequest.SetFullVoidFlag("true") 'True)
                CCSecureRequest.SetOrderId(sVoidTroutD)
            Else
                CCSecureRequest.SetOrderId(sOrderID)
            End If

            If bDisablePartialApproval = True Or bEnablePartialAuthorizations = False Then
                CCSecureRequest.SetPartialApprovalFlag("false") 'False)
            Else
                CCSecureRequest.SetPartialApprovalFlag("true") ' True)
            End If


            CCSecureRequest.SetBillPostalCode(sZipCode)

            'CCSecureRequest.setExpireYear(sExpireYear)


            Try


                dtSent = Now

                If iCreditMode >= 8 Then
                    'CCSecureRequest = GetSecureRequestFromPayGateway(CCSecureRequest)

                    Select Case sChargeType
                        Case "PURCHASE"
                            CCSecureRequest.SetChargeType("DEBITSALE")
                        Case "REFUND"
                            CCSecureRequest.SetChargeType("DEBITRETURN")
                        Case "SALE"
                            CCSecureRequest.SetChargeType("CREDITSALE")
                        Case "CREDIT"
                            CCSecureRequest.SetChargeType("CREDITRETURN")
                        Case "AUTH"
                            CCSecureRequest.SetChargeType("CREDITAUTH")
                        Case "VOID"
                            CCSecureRequest.SetChargeType("CREDITVOID")

                    End Select

                    If sChargeType = "PURCHASE" Or sChargeType = "REFUND" Then
                        CCSecureRequest.SetTransactionType("DEBIT_CARD")
                    ElseIf sChargeType = "PURCHASE" Or sChargeType = "REFUND" Then
                        CCSecureRequest.SetTransactionType("DEBIT_CARD")
                    Else
                        CCSecureRequest.SetTransactionType("CREDIT_CARD")
                    End If


                    'If bTokenUpdate = True Then
                    '    CCSecureRequest.SetChargeType("ALIASUPDATE")
                    '    '    CCSecureResponse = doUpdateAliasTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId)
                    'End If
                    If bOffline = True Then
                        CCSecureResponse = doDirectToGatewayTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId, bOffline, bDuplicateCheck, bRecurring)
                    ElseIf iCreditMode = 10 And bTokenUpdate = True And CCSecureRequest.GetCreditCardNumber <> "" Then
                        CCSecureResponse = doDirectToGatewayTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId, bOffline, bDuplicateCheck, False)
                        'ElseIf bTokenUpdate = True And CCSecureRequest.GetCreditCardNumber <> "" Then
                        '    CCSecureResponse = doUpdateAliasTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId)
                    ElseIf bTokenConversion = True Then
                        CCSecureResponse = doCreateAliasTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId)
                        'ElseIf bTokenDelete = True Then
                        '    CCSecureResponse = doDeleteAliasTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId)
                        'ElseIf iCreditMode = 9 Then
                        '    CCSecureResponse = doExpressEdgePCTransaction(CCSecureRequest, sAccountToken, iCCTimeOut * 1000, bSilentProcessing, 0)
                    ElseIf iCreditMode = 10 Then
                        CCSecureResponse = doDirectToGatewayTransaction(CCSecureRequest, CreditCardLogInfo.lCreditCardLogId, bOffline, bDuplicateCheck, bRecurring)
                        'Else
                        '    CCSecureResponse = doExpressEdgeCloudTransaction(CCSecureRequest, sAccountToken, iCCTimeOut * 1000, bSilentProcessing, 0)

                        '    If CCSecureRequest.GetChargeType = "CREDITSALE" And CCSecureResponse.GetResponseCode = "1" And bCCSaveSignature = True And bSilentProcessing = False Then
                        '        CCSignatureRequest.SetChargeType("PPDPROMPTSIGNATURE")
                        '        CCSignatureRequest.SetSignatureTitle("Credit Card Signature")
                        '        CCSignatureRequest.SetSignatureText(CCSecureRequest.GetChargeType & vbLf & CCSecureRequest.GetInvoiceNumber & vbLf & CCSecureRequest.GetChargeTotal)
                        '        CCSignatureResponse = doExpressEdgeCloudTransaction(CCSignatureRequest, sAccountToken, iCCTimeOut * 1000, bSilentProcessing, 0)
                        '        CCSecureResponse.SetSignature(CCSignatureResponse.GetSignature)
                        '    End If
                    End If



                    'ElseIf iCreditMode > 5 Then ' = 6 Or iCreditMode = 7 Then

                    '    CCSecureResponse = doDataCapTransaction(CCSecureRequest, sAccountToken, iCCTimeOut * 1000, bSilentProcessing, 0, bManualPrompt)

                    'ElseIf iCreditMode = 5 Then
                    '    'CCSecureRequest = GetSecureRequestFromPayGateway(CCSecureRequest)
                    '    If sChargeType = "PURCHASE" Or sChargeType = "REFUND" Then
                    '        CCSecureRequest.SetTransactionType("DEBIT_CARD")
                    '    Else
                    '        CCSecureRequest.SetTransactionType("CREDIT_CARD")
                    '    End If

                    '    CCSecureResponse = doSecurePaymentTransaction(CCSecureRequest, sAccountToken, iCCTimeOut * 1000, bSilentProcessing, 0, False)
                Else

                    'CreditCard.UpdateLogTransSent()
                    Dim sRequest As String = ""
                    Dim sResponse As String = ""
                    CCRequest = GetRequestFromSecureRequest(CCSecureRequest)
                    sRequest = GetRequestString(CCSecureRequest)
                    CCResponse = DirectCast(TRClient.doTransaction(CCRequest, sAccountToken, iCCTimeOut * 1000), CreditCardResponse)
                    CCSecureResponse = GetSecureResponseFromPayGateway(CCResponse)
                    sResponse = GetResponseString(CCResponse)

                    If bSaveSecurePaymentLog = True Then
                        Account.SaveLog(sRequest, userMonthly.UserID, sComputerName, 204, False)
                        Account.SaveLog(sResponse, userMonthly.UserID, sComputerName, 205, False)
                    End If
                End If

                'CCSecureResponse = DirectCast(TRClient.doTransaction(CCSecureRequest, sAccountToken, iCCTimeOut * 1000), CreditCardResponse)

                dtReceived = Now

                tsTime = (dtReceived - dtSent)

                If Val(CCSecureRequest.GetChargeTotal) = 0 Then
                    Select Case CCSecureResponse.GetResponseCode
                        Case 1
                            If CCSecureResponse.GetResponseCodeText.ToUpper = "DECLINE" Or CCSecureResponse.GetResponseCodeText.ToUpper = "DECLINED" Then
                                sResult = "DECLINED"
                            Else
                                sResult = "CAPTURED"
                            End If
                        Case 5
                            If bTokenDelete = True Or bTokenConversion = True Then
                                sResult = "CAPTURED"
                            Else
                                If CCSecureResponse.GetResponseCodeText.ToUpper = "APPROVAL" Then
                                    sResult = "CAPTURED"
                                Else
                                    sResult = "ERROR"
                                End If
                            End If
                        Case 100
                            sResult = "DECLINED"
                        Case 813
                            sResult = "NOT CAPTURED"
                            sAuthCode = "DUPLICATE TRANSACTION"
                        Case Else
                            If InStr(CCSecureResponse.GetResponseCodeText.ToUpper, "APPROVAL") Then
                                sResult = "CAPTURED"
                            Else
                                sResult = "ERROR"
                            End If
                    End Select

                    If bTokenConversion = False Then Return True
                End If

                If sCCCustomerId <> "" Then

                    sSpan2 = CCSecureResponse.GetSpan()
                    sBIN2 = CCSecureResponse.GetBin

                    If (sSpan2 <> "" And sSpan <> "" And sSpan2 <> sSpan) Or (sBIN2 <> "" And sBIN <> "" And sBIN2 <> sBIN) Then
                        Customer.UpdateFamilyData(CLng(sCCCustomerId), sSpan, sBIN, sSpan2, sBIN2)
                    End If


                    iMpdResponseCode = CCSecureResponse.GetMpdResponseCode
                    sMpdResponseCodeText = CCSecureResponse.GetMpdResponseCodeText
                    sPayerIdentifier = CCSecureResponse.GetPayerIdentifier()
                    If sPayerIdentifier = "" And iCreditMode = 10 And bTokenUpdate = True Then
                        sPayerIdentifier = cMPDInfo.sPayerIdentifier
                    End If
                    dtManageUntil = CCSecureResponse.GetManageUntil()
                    sSpan = CCSecureResponse.GetSpan()
                    sSpanMonth = CCSecureResponse.GetExpireMonth
                    sSpanYear = CCSecureResponse.GetExpireYear
                    sBIN = CCSecureResponse.GetBin
                    If sBIN <> "" Then
                        sCardBrand = GetCreditCardType(sBIN)
                    End If
                    'Dim sccType As String = ""



                    If iMpdResponseCode = 1 And CLng(sCCCustomerId) > 0 Then
                        lMPDId = Customer.SaveManagedPayerData(CLng(sCCCustomerId), sPayerIdentifier, dtManageUntil, sSpan, sSpanMonth, sSpanYear, sZipCode, sCardBrand, sBIN, "", "", False, iCreditMode, bManualFamilyAccountVerification, True, userMonthly.UserName, sComputerName)

                        If lMPDId <= 0 Then
                            lMPDId = Customer.SaveManagedPayerData(CLng(sCCCustomerId), sPayerIdentifier, dtManageUntil, sSpan, sSpanMonth, sSpanYear, sZipCode, sCardBrand, sBIN, "", "", False, iCreditMode, bManualFamilyAccountVerification, True, userMonthly.UserName, sComputerName)
                        End If

                        Customer.SaveCCHolderName(CLng(sCCCustomerId), sCCHolderName.Replace("'", " "))
                    ElseIf CLng(sCCCustomerId) > 0 And cMPDInfo.sPayerIdentifier <> "" Then
                        If (sSpan <> "" And sSpan <> cMPDInfo.sSPAN) Or (sSpanMonth <> "" And sSpanMonth <> cMPDInfo.sExpireMonth) Or (sSpanYear <> "" And sSpanYear <> cMPDInfo.sExpireYear) Then

                            If (sSpan <> "" And sSpan <> cMPDInfo.sSPAN) Then
                                cMPDInfo.sSPAN = sSpan
                            End If

                            If (sSpanMonth <> "" And sSpanMonth <> cMPDInfo.sExpireMonth) Then
                                cMPDInfo.sExpireMonth = sSpanMonth
                            End If

                            If (sSpanYear <> "" And sSpanYear <> cMPDInfo.sExpireYear) Then
                                cMPDInfo.sExpireYear = sSpanYear
                            End If
                            lMPDId = Customer.SaveManagedPayerData(CLng(sCCCustomerId), cMPDInfo.sPayerIdentifier, cMPDInfo.dtManageUntil, cMPDInfo.sSPAN, cMPDInfo.sExpireMonth, cMPDInfo.sExpireYear, cMPDInfo.sBillPostalCode, cMPDInfo.sCardBrand, cMPDInfo.sBIN, "", "", False, iCreditMode, bManualFamilyAccountVerification, True, userMonthly.UserName, sComputerName)

                        End If
                    End If

                End If

                Select Case CCSecureResponse.GetResponseCode
                    Case 1
                        If CCSecureResponse.GetResponseCodeText.ToUpper = "DECLINE" Or CCSecureResponse.GetResponseCodeText.ToUpper = "DECLINED" Then
                            sResult = "DECLINED"
                        Else
                            sResult = "CAPTURED"
                        End If
                    Case 5
                        If bTokenDelete = True Or bTokenConversion = True Then
                            sResult = "CAPTURED"
                        Else
                            If CCSecureResponse.GetResponseCodeText.ToUpper = "APPROVAL" Then
                                sResult = "CAPTURED"
                            Else
                                sResult = "ERROR"
                            End If
                        End If
                    Case 100
                        sResult = "DECLINED"
                    Case 901
                        sResult = "DECLINED"
                        sAuthCode = CCSecureResponse.GetResponseCodeText
                    Case 813
                        sResult = "NOT CAPTURED"
                        sAuthCode = "DUPLICATE TRANSACTION"
                    Case Else
                        If CCSecureResponse.GetResponseCodeText.ToUpper = "APPROVAL" Then
                            sResult = "CAPTURED"
                        Else
                            sResult = "ERROR"
                        End If
                End Select

                'Select Case CCSecureResponse.getResponseCode
                '    Case 1
                '        sResult = "CAPTURED"
                '    Case 100
                '        sResult = "DECLINED"
                '    Case Else
                '        sResult = "ERROR"
                'End Select

                Select Case CCSecureResponse.GetSecondaryResponseCode
                    'Case 1
                    '    sResult = "CAPTURED"
                    'Case 100
                    '    sResult = "DECLINED"
                    'Case Else
                    '    sResult = "ERROR"
                End Select

                'sAuthCode = CCSecureResponse.getBankApprovalCode
                'sReferenceNumber = CCSecureResponse.getReferenceId
                ''sTroutd = CCSecureResponse.getBankTransactionId
                'sTransAmount = CCSecureResponse.getRequestedAmount
                'sAuthAmount = CCSecureResponse.getCapturedAmount
                'sTicket = "" 'CCSecureResponse.getOrderId
                'sResponseText = CCSecureResponse.getResponseCodeText
                'iResponseCode = CCSecureResponse.getResponseCode
                'sTroutd = sOrderID


                'sTroutd = CCSecureResponse.getBankTransactionId
                If CCSecureResponse.GetBankApprovalCode <> "" Then
                    sAuthCode = CCSecureResponse.GetBankApprovalCode
                End If
                sReferenceNumber = CCSecureResponse.GetReferenceId
                sTransAmount = CCSecureResponse.GetRequestedAmount
                sAuthAmount = CCSecureResponse.GetCapturedAmount
                sResponseText = CCSecureResponse.GetResponseCodeText
                iResponseCode = CCSecureResponse.GetResponseCode
                sTicket = "" 'CCSecureResponse.getOrderId
                sTroutd = sOrderID
                'sSignature = CCSecureResponse.GetSignature

                'If sSignature <> "" And lTicketId > 0 Then
                '    UpdateTicketSignature(Nothing, lTicketId, sSignature, True)
                'End If


                If sAuthCode = "" And sResult = "DECLINED" Then
                    sResult = "NOT CAPTURED"
                    sAuthCode = "DECLINED"
                End If

                'sCCType = CCSecureResponse.

                If iCreditMode >= 8 Then
                    sTroutd = CCSecureResponse.GetTransactionId
                Else
                    sTroutd = sOrderID
                End If

                If sCardBrand = "" Then
                    sCardBrand = CCSecureResponse.GetCardBrand

                    Select Case sCardBrand.ToUpper
                        Case "AMERICAN EXPRESS"
                            sCardBrand = "AMEX"
                        Case "MASTERCARD"
                            sCardBrand = "MC"
                    End Select
                End If

                If sAuthCode = "" And sResult = "DECLINED" Then
                    sResult = "NOT CAPTURED"
                    sAuthCode = "DECLINED"
                End If




            Catch ex As Exception
                dtReceived = Now
                WriteToLog(sLogFileName, "SendPayGatewayTrans", Err.Number, ex.Message, "ERROR")




                tsTime = (dtReceived - dtSent)
                'If bDisableOfflineCC = False Then
                '    sResult = "NOT CAPTURED"
                '    sAuthCode = "TIMEOUT"
                '    sResponseText = ex.Message
                'End If
                'Return False
            End Try





            'sResultsText = sResultsText + "Code:  " + Convert.ToString(CCSecureResponse.getResponseCode())
            'sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Code Text:  " + CCSecureResponse.getResponseCodeText()
            'sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Secondary Code:  " + Convert.ToString(CCSecureResponse.getSecondaryResponseCode())
            'sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Approval Code:  " + CCSecureResponse.getBankApprovalCode()
            'sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Trans ID:  " + CCSecureResponse.getBankTransactionId()
            'sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Batch ID:  " + CCSecureResponse.getBatchId()
            'sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Reference ID:  " + CCSecureResponse.getReferenceId()
            'sResultsText = sResultsText + vbNewLine
            ''sResultsText = sResultsText + "ISO Code:  " + CCSecureResponse.getIsoCode()
            ''sResultsText = sResultsText + vbNewLine
            ''sResultsText = sResultsText + "AVS Code:  " + CCSecureResponse.getAvsCode()
            ''sResultsText = sResultsText + vbNewLine
            ''sResultsText = sResultsText + "CVV Response:  " + CCSecureResponse.getCreditCardVerificationResponse()
            ''sResultsText = sResultsText + vbNewLine
            'sResultsText = sResultsText + "Date / Time:  " + CCSecureResponse.getTimeStamp().ToString

            'sResultsText = sResultsText + vbNewLine





            'sResult = CCSecureResponse.getResponseCodeText
            'sAuthCode = sResult


            sResultsText = "<XML_REQUEST>"
            sResultsText = sResultsText + "<RESULT>" + sResult
            sResultsText = sResultsText + "</RESULT>" + vbNewLine

            sResultsText = sResultsText + "<AUTH_CODE>" + sAuthCode
            sResultsText = sResultsText + "</AUTH_CODE>" + vbNewLine

            sResultsText = sResultsText + "<RESPONSECODE>" + Convert.ToString(iResponseCode)
            sResultsText = sResultsText + "</RESPONSECODE>" + vbNewLine

            sResultsText = sResultsText + "<RESPONSE_TEXT>" + sResponseText
            sResultsText = sResultsText + "</RESPONSE_TEXT>" + vbNewLine

            sResultsText = sResultsText + "<TICKET>" + sTicket
            sResultsText = sResultsText + "</TICKET>" + vbNewLine

            sResultsText = sResultsText + "<INTRN_SEQ_NUM>" + sTroutd
            sResultsText = sResultsText + "</INTRN_SEQ_NUM>" + vbNewLine

            sResultsText = sResultsText + "<REFERENCEID>" + sReferenceNumber
            sResultsText = sResultsText + "</REFERENCEID>" + vbNewLine

            sResultsText = sResultsText + "<TRANS_AMOUNT>" + sTransAmount
            sResultsText = sResultsText + "</TRANS_AMOUNT>" + vbNewLine

            sResultsText = sResultsText + "<AUTH_AMOUNT>" + sAuthAmount
            sResultsText = sResultsText + "</AUTH_AMOUNT>" + vbNewLine


            sResultsText = sResultsText + "</XML_REQUEST>"


            If Val(sTransAmount) > 0 And Val(sAuthAmount) > 0 Then
                If Val(sTransAmount) > Val(sAuthAmount) Then
                    sResult = "NOT CAPTURED"
                    sAuthCode = "INSUFFICIENT FUNDS " & sAuthAmount
                    sReferenceNumber = ""
                    'sTroutd = ""

                    If sOrderID <> "" Then

                        sVoidTroutD = sOrderID

                        SendPayGateWayVoidCommand(sOrderID)
                    End If

                End If

            End If

            If (CreditCardLogInfo.sTicketNumber.Trim <> "" And InStr(CreditCardLogInfo.sTicketNumber, ".") <= 0) And (sTicket.Trim <> "" And InStr(sTicket, ".") <= 0) Then 'And bCCSendToQueue = True Then
                If CreditCardLogInfo.sTicketNumber.Trim <> sTicket.Trim Then
                    sResult = "NOT CAPTURED"
                    sAuthCode = "TIMEOUT"
                    sReferenceNumber = ""
                    sTroutd = ""
                End If
            End If

            'rCounter = rCounter + 1
            'If rCounter = 10 Then rCounter = 0


            CreditCardLogInfo.sResult = sResult
            CreditCardLogInfo.sResultXML = sResultsText
            CreditCardLogInfo.iResponseTime = CInt(tsTime.TotalSeconds)
            CreditCardLogInfo.sTroutD = sTroutd
            CreditCardLogInfo.sAuthCode = sAuthCode

            lLogId = CreditCard.UpdateLogTransCompleted(CreditCardLogInfo, "LWREPORTS")

            'If bVoid = True Then
            '    Exit Function
            'End If

            'CompletePayGateway(sResult, sAuthCode, sTroutd, sResponseText, sReferenceNumber, lLogId)
            'If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "SALE NOT FOUND" Or sResult = "" Then
            '    Me.lblTitle.Text = sResult
            '    Me.ccCharge_Error(sResult, sAuthCode, sTroutd, lLogId, sResponseText)
            'Else

            '    If sTransType = "REFUND" Or sTransType = "VOID" Then
            '        sAuthCode = sTransType
            '        ccCharge_Finish(sResult, sTransType, sReferenceNumber, False, sTroutd, lLogId, False)
            '        '
            '    Else
            '        ccCharge_Finish(sResult, sAuthCode, sReferenceNumber, False, sTroutd, lLogId, False) ', dsreturn.Tables(0).Rows(0).Item("AUTH_CODE").ToString)
            '    End If

            'End If

            Return True
        Catch exc As Exception
            WriteToLog(sLogFileName, "SendPayGateway", Err.Number, exc.Message, "ERROR")

            Return False
            'sResultsText = exc.ToString

        End Try


    End Function

    Public Sub SendPayGateWayVoidCommand(ByVal sVoidTroutd As String)

        Dim sAuthCode As String = ""
        Dim sResult As String = ""
        Dim sReferenceNumber As String = ""
        Dim lLogId As Long = 0
        Dim sTicket As String = ""
        Dim sResponseText As String = ""
        Dim sAuthAmount As String = ""
        Dim sTransAmount As String = ""
        Dim sTroutd As String = ""
        Try

            SendPayGateway("VOID", "0.00", "", "", "", 0, AccountInfo.AccountToken, True, "", sResult, sAuthCode, sTroutd, sResponseText, sReferenceNumber, lLogId, "", sVoidTroutd, "", "", "", "", "", "", 0, "", "", "", "", False, False, False, "", "", False)



        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Function CreateMonthlyTicket(ByVal sTicketNumber As String, ByVal sUserName As String, ByVal lMonthlyUserId As Long, ByVal lCustomerId As Long, ByVal lVehicleID As Long, ByVal lSurchargeId As Long, ByVal lAccountType As Long, ByVal dblAmount As Double, ByVal dblTaxes As Double, ByVal sPartNumber As String, ByVal sCCNumber As String, ByVal sSpan As String, ByVal sExpireMonth As String, ByVal sExpireYear As String, ByVal sBIN As String, ByVal sSPAN2 As String, ByVal sBIN2 As String, bFamilyAccount As Boolean) As Long

        Dim lTicketDetailId As Long = 0
        Dim lTicketId As Long = 0

        Dim tempTicketInfo As New TicketProperties
        Dim MonthlySurcharge As New SurchargeProperties
        Dim MonthlyAccount As New CustomerAccountProperties

        Dim iCash As Integer = 0
        Dim iCredit As Integer = 0
        Dim iCheck As Integer = 0
        'Dim sUnlimited As String = ""

        Dim lMonthlyGreeterId As Long = 0
        Dim dblMonthlyAmount As Double = 0

        Dim iCounter As Integer = 0
        'Dim sDID As String = GetDID()

        'Dim sSpan As String = ""
        ''Dim sExpireMonth As String = ""
        ''Dim sExpireYear As String = ""
        Try


            iCash = 0
            iCredit = 0
            iCheck = 0

            tempTicketInfo.TicketNumber = sTicketNumber
            'tempTicketInfo.CurrentNumber = CLng(Mid(sTicketNumber, 3))

            'tempTicketInfo.Prefix = Mid(sTicketNumber, 1, 2)
            tempTicketInfo.UserName = sUserName
            'tempTicketInfo.TicketId = lTicketId
            tempTicketInfo.CustomerId = lCustomerId
            'tempTicketInfo.VehicleId = lVehicleID
            'tempTicketInfo.WorkOrderId = lWorkOrderID
            'tempTicketInfo.WorkOrderNumber = sWorkOrderNumber
            ' tempTicketInfo.Printed = True
            tempTicketInfo.Taxes = 0
            'If lngGreeterId > 0 Then
            '    tempTicketInfo.GreeterId = lngGreeterId
            'Else

            'End If

            tempTicketInfo.sTerminal = sComputerName


            lMonthlyGreeterId = Customer.GetMonthlyGreeterId(lCustomerId.ToString)

            If lMonthlyGreeterId = 0 Then
                lMonthlyGreeterId = Ticket.GetMonthlyGreeterId(lCustomerId, lMonthlyUserId)
            End If

            tempTicketInfo.GreeterId = lMonthlyGreeterId

            iCounter = 1
            'If bTabletMode = True Then

            '    sDID = GetDID()
            '    Do Until lTicketId > 0 Or iCounter = 10
            '        lTicketId = Ticket.SaveWashTicket(tempTicketInfo, lMonthlyUserId.ToString, AccountInfo.LocationId, True, True, sDID)

            '        If lTicketId <= 0 And iCounter Mod 3 = 0 Then
            '            'iCounter = 0
            '            frmMsgBox("Error saving ticket. Press OK to try again.", cnstOk, 0, False, True, 10)
            '        End If
            '        iCounter = iCounter + 1

            '    Loop
            'Else
            lTicketId = Ticket.SaveWashTicket(tempTicketInfo, lMonthlyUserId.ToString, AccountInfo.LocationId, True, True)
            'End If
            'Ticket.UpdateMonthlyTicketCounter()

            'Ticket.SetPrinted(lTicketId, True)



            If lSurchargeId > 0 Then
                MonthlySurcharge = Surcharge.GetSurchargeInfo(lSurchargeId, "")
                lTicketDetailId = SaveTicketDetails(lTicketId, sTicketNumber, MonthlySurcharge.Description, MonthlySurcharge.Amount, sUserName, lMonthlyUserId, lMonthlyGreeterId, MonthlySurcharge.Taxable, MonthlySurcharge.SurchargeId, 3, lCustomerId, 0, 1, sPartNumber, 0, 0, 0, 0)
            Else
                MonthlyAccount = Customer.GetAccountType(lAccountType)

                dblMonthlyAmount = MonthlyAccount.Amount
                If ((bManualFamilyAccountVerification = True And bFamilyAccount = True) Or bManualFamilyAccountVerification = False) And MonthlyAccount.FamilySurchargeId > 0 And (sCCNumber <> "" Or (iCreditMode >= 4 And sSpan <> "")) Then

                    'If sCCNumber.Length > 4 Then sSpan = Mid(sCCNumber, sCCNumber.Length - 3)
                    'If sExpDate.Length = 4 Then
                    '    sExpireMonth = Mid(sExpDate, 1, 2)
                    '    sExpireYear = Mid(sExpDate, 3, 2)
                    '    If sExpireYear <> "" Then
                    '        If Now.Year > CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2) & sExpireYear) And CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2)) > 84 And CInt(sExpireYear) < 16 Then
                    '            sExpireYear = (CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2)) + 1).ToString & sExpireYear
                    '        Else
                    '            sExpireYear = Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2) & sExpireYear
                    '        End If
                    '    End If
                    'End If

                    Select Case iFamilyAccountDiscountMode
                        Case 1
                            If Customer.GetFamilyAccountIndex(sCCNumber, lCustomerId, iCreditMode, sSpan, sExpireMonth, sExpireYear, sBIN, sSPAN2, sBIN2, bManualFamilyAccountVerification, True) > 0 Then
                                dblMonthlyAmount = MonthlyAccount.FamilyAmount
                            End If
                        Case Else
                            If Customer.HasFamilyAccount(sCCNumber, lCustomerId, iCreditMode, sSpan, sExpireMonth, sExpireYear, sBIN, sSPAN2, sBIN2, bManualFamilyAccountVerification, True) = True Then
                                dblMonthlyAmount = MonthlyAccount.FamilyAmount
                            End If
                    End Select

                End If

                lTicketDetailId = SaveTicketDetails(lTicketId, sTicketNumber, MonthlyAccount.AccountType & " Sale", dblMonthlyAmount, sUserName, lMonthlyUserId, lMonthlyGreeterId, MonthlyAccount.Taxable, lAccountType, 14, lCustomerId, 0, 1, "", 0, 0, 0, 0)
            End If

            If dblTaxes > 0 Then
                TicketDetail.UpdateDetailTotals(lTicketDetailId, dblTaxes, 0, 0, 0)
            End If
            'Call CompleteTicket(1, iCash, iCheck, iCredit, 0, 0, dblAmount, dblTaxes, 0, 0, sUserName, lMonthlyUserId, AccountInfo.LocationId, "", "", "", lTicketId, , , , , , , , "", 0, , , , lCustomerId)


            ''If bCreditCard = True Then
            'SavePayment(sPaymentType, lMonthlyUserId, sUserName, lTicketId, sTicketNumber, dblAmount)
            ''Else
            ''    SavePayment("CASH", lUserId, sUserName, lTicketId, sTicketNumber, dblAmount)
            ''End If

            '' OpenDrawer()


            Return lTicketId

        Catch ex As Exception
            Exit Function
        End Try
    End Function

    Function CompleteMonthlyTicket(ByVal iPaid As Integer, ByVal lTicketId As Long, ByVal sTicketNumber As String, ByVal sUserName As String, ByVal lMonthlyUserId As Long, ByVal lCustomerId As Long, ByVal lVehicleID As Long, ByVal lWorkOrderID As Long, ByVal sWorkOrderNumber As String, ByVal lngGreeterId As Long, ByVal dblAmount As Double, ByVal lSurchargeId As Long, ByVal lAccountType As Long, ByVal dblTaxes As Double, ByVal sPaymentType As String, ByVal sPartNumber As String, ByVal sChangeDue As String) As Long

        Dim lTicketDetailId As Long = 0
        ' Dim lTicketId As Long = 0

        Dim tempTicketInfo As New TicketProperties
        Dim MonthlySurcharge As New SurchargeProperties
        Dim iCash As Integer = 0
        Dim iCredit As Integer = 0
        Dim iCheck As Integer = 0
        Dim iGift As Integer = 0
        'Dim sUnlimited As String = ""

        Try

            If sPaymentType = "GIFT CARD" Then
                iCash = 0
                iCredit = 0
                iCheck = 0
                iGift = 1
            ElseIf sPaymentType = "CREDIT CARD" Then
                iCash = 0
                iCredit = 1
                iCheck = 0
                iGift = 0
            ElseIf sPaymentType = "CHECK" Then
                iCash = 0
                iCredit = 0
                iCheck = 1
                iGift = 0
            ElseIf sPaymentType = "CASH" Then
                iCash = 1
                iCredit = 0
                iCheck = 0
                iGift = 0
            Else
                iCash = 0
                iCredit = 0
                iCheck = 0
                iGift = 0
            End If

            tempTicketInfo.TicketNumber = sTicketNumber
            tempTicketInfo.CurrentNumber = CLng(Mid(sTicketNumber, 3))

            tempTicketInfo.Prefix = Mid(sTicketNumber, 1, 2)
            tempTicketInfo.UserName = sUserName
            'tempTicketInfo.TicketId = lTicketId
            tempTicketInfo.CustomerId = lCustomerId
            'tempTicketInfo.VehicleId = lVehicleID
            'tempTicketInfo.WorkOrderId = lWorkOrderID
            'tempTicketInfo.WorkOrderNumber = sWorkOrderNumber
            ' tempTicketInfo.Printed = True
            tempTicketInfo.Taxes = dblTaxes
            If lngGreeterId > 0 Then
                tempTicketInfo.GreeterId = lngGreeterId
            Else
                tempTicketInfo.GreeterId = lMonthlyUserId
            End If

            tempTicketInfo.sTerminal = sComputerName

            'lTicketId = Ticket.SaveWashTicket(tempTicketInfo, lMonthlyUserId.ToString, AccountInfo.LocationId, True, False)
            'Ticket.UpdateMonthlyTicketCounter()

            Ticket.SetPrinted(lTicketId, True)

            'If lSurchargeId > 0 Then
            '    MonthlySurcharge = Surcharge.GetSurchargeInfo(lSurchargeId)
            '    lTicketDetailId = SaveTicketDetails(lTicketId, sTicketNumber, MonthlySurcharge.Description, MonthlySurcharge.Amount, sUserName, lMonthlyUserId, lngGreeterId, MonthlySurcharge.Taxable, MonthlySurcharge.SurchargeId, 3, lCustomerId, 0, 1, sPartNumber, 0, 0, 0)
            'Else
            '    lTicketDetailId = SaveTicketDetails(lTicketId, sTicketNumber, Customer.GetAccountTypeName(lAccountType) & " Sale", dblAmount - dblTaxes, sUserName, lMonthlyUserId, lngGreeterId, False, lAccountType, 14, lCustomerId, 0, 1, "", 0, 0, 0)
            'End If

            If iPaid = 1 Then
                Call CompleteTicket(iPaid, iCash, iCheck, iCredit, iGift, 0, dblAmount, dblTaxes, 0, 0, sUserName, lMonthlyUserId, AccountInfo.LocationId, "", "", "", lTicketId, 0, "", "", "", "", "", "", "", 0, 0, "", lCustomerId, 0, lVehicleID) ', True)


                'If bCreditCard = True Then
                SavePayment(sPaymentType, lMonthlyUserId, sUserName, lTicketId, sTicketNumber, dblAmount)
                'Else
                '    SavePayment("CASH", lUserId, sUserName, lTicketId, sTicketNumber, dblAmount)
                'End If

                ' OpenDrawer()
            Else
                Call CompleteTicket(iPaid, iCash, iCheck, iCredit, iGift, 0, dblAmount, dblTaxes, 0, 0, sUserName, lMonthlyUserId, AccountInfo.LocationId, "", "", "", lTicketId, 0, "", "", "", "", "", "", "", 0, 0, "", lCustomerId, 0, lVehicleID) ', True)
            End If



            Return lTicketId

        Catch ex As Exception
            Exit Function
        End Try
    End Function

    Public Function SaveMonthlyTransaction(ByVal lTicketId As Long, ByVal sCCAuth As String, ByVal sReferenceNumber As String, ByVal sTicketNumber As String, ByVal sCCType As String, ByVal sCCNumber As String, ByVal sCCExp As String, ByVal sCCtrack As String, ByVal sCCAmount As String, ByVal lMonthlyUserId As Long, ByVal sCardHolderName As String) As Boolean

        Dim TempCCInfo As New CreditCardProperties
        'Dim sTemp As String
        'Dim sDID As String = GetDID()
        Dim lReturnId As Long = 0
        Dim iCounter As Integer = 0
        Try

            TempCCInfo.LocationId = AccountInfo.LocationId
            TempCCInfo.UserId = lMonthlyUserId
            TempCCInfo.ReferenceNumber = sTicketNumber
            TempCCInfo.ReferenceId = lTicketId
            TempCCInfo.InvNumber = sTicketNumber
            TempCCInfo.CCType = sCCType

            'If lblAuthNumber.Text = "OFFLINE" Or bOffline = True Then
            '    sTemp = LogicEncrypt(Trim(lblCardNumber.Text))
            '    TempCCInfo.CCNumber = sTemp
            'Else
            If sCCNumber.Length > 4 And InStr(sCCNumber, "TOKEN") = 0 Then
                TempCCInfo.CCNumber = Mid(Trim(sCCNumber), 1, 4) & "........" & Mid(Trim(sCCNumber), Trim(sCCNumber).Length - 3, 4)
            Else
                TempCCInfo.CCNumber = sCCNumber
            End If
            'End If

            TempCCInfo.CCExpDate = Trim(sCCExp)
            TempCCInfo.TotalAmount = CDbl(sCCAmount)

            If Trim(sMerchantNumber).Length > 25 Then
                TempCCInfo.TerminalId = Mid(Trim(sMerchantNumber), Trim(sMerchantNumber).Length - 24)
            Else
                TempCCInfo.TerminalId = Trim(sMerchantNumber)
            End If
            TempCCInfo.AuthNumber = Trim(sCCAuth)
            TempCCInfo.RefNumber = Trim(sReferenceNumber)

            TempCCInfo.ProgramVersion = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision
            TempCCInfo.CCType = Trim(sCCType)
            TempCCInfo.TransactionType = "SALE"
            TempCCInfo.TerminalName = sComputerName
            TempCCInfo.CardHolderName = sCardHolderName

            'If Not lblCardNumber.Tag Is Nothing Then
            '    TempCCInfo.Track2Data = LogicEncrypt(Trim(sCCtrack))
            'End If
            'If miTransactionType = NovaPLUSPOS.TranType.iSale Then
            'TempCCInfo.TransactionType = "SALE"
            'ElseIf miTransactionType = NovaPLUSPOS.TranType.iReturn Then
            'TempCCInfo.TransactionType = "REFUND"
            'Else
            'TempCCInfo.TransactionType = "UNKNOWN"
            'End If

            lReturnId = 0
            iCounter = 1
            'If bTabletMode = True Then

            '    sDID = GetDID()
            '    Do Until lReturnId > 0 Or iCounter = 10
            '        lReturnId = CreditCard.SaveCCTransaction(TempCCInfo, sDID)

            '        If lReturnId <= 0 And iCounter Mod 3 = 0 Then
            '            'iCounter = 0
            '            frmMsgBox("Error updating ticket details. Press OK to try again.", cnstOk, 0, False, True, 10)
            '        End If
            '        iCounter = iCounter + 1

            '    Loop
            'Else
            CreditCard.SaveCCTransaction(TempCCInfo)
            'End If




            Exit Function

        Catch ex As Exception
            'WriteToLog(sLogFileName, "m_SaveCreditCardTransaction", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10))
            WriteToLog(sLogFileName, "PrintCashierReport", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")


            'g_WriteToLog(gsLogFileName, "m_SaveCreditCardTransaction(SQL STRING)", Err.Number, sSQL)
            ' Me.Enabled = True
            'Me.bResponse = False

            'lblTitle.Text = "PROGRAM ERROR " & Err.Number & " PLEASE NOTIFY OFFICE."
            ' lblTitle.Refresh()
        End Try

    End Function

    Public Function GetNextMonthlyPaymentDate(ByVal dtStart As Date, ByVal iRenewDay As Integer, ByVal iStartDay As Integer, ByVal bCharged As Boolean, ByVal iMonthlyChargeDay As Integer, ByVal lFrequency As Long, ByVal cCust As CustomerProperties, ByVal MonthlyAcctInfo As CustomerAccountProperties, ByVal bEnableMessage As Boolean, ByVal bFirstCharge As Boolean) As Date

        Dim iMonth As Integer = 0
        Dim iYear As Integer = 0
        Dim iDay As Integer = 0

        Dim dtDate As Date
        Try

            If dtStart < CDate("1/1/2007") Then
                dtStart = CDate(Now.ToShortDateString)
            End If

            If iMonthlyChargeDay > 0 Then
                dtDate = CDate(Today.Month & "/" & iMonthlyChargeDay & "/" & Today.Year).AddMonths(1)
                Return dtDate
            End If

            'If dtStart.Day > Now.Day Then
            iMonth = dtStart.Month + 1
            'Else
            'iMonth = Now.Month
            'End If

            If iMonth > 12 Then
                iMonth = iMonth - 12
                iYear = dtStart.Year + 1
            Else
                iYear = dtStart.Year
            End If

            'If bNoBackCharges = False Then

            If iRenewDay > 0 Then
                iDay = iRenewDay
            Else
                iDay = dtStart.Day
            End If

            If Date.DaysInMonth(iYear, iMonth) < iDay Then
                dtDate = DateSerial(iYear, iMonth, Date.DaysInMonth(iYear, iMonth))
            Else
                dtDate = DateSerial(iYear, iMonth, iDay)
            End If

            If (dtDate - CDate(Now.ToShortDateString)).TotalDays < 15 And bCharged = True Then
                iMonth = iMonth + 1

                If iMonth > 12 Then
                    iMonth = iMonth - 12
                    iYear = dtDate.Year + 1
                Else
                    iYear = dtDate.Year
                End If

                If Date.DaysInMonth(iYear, iMonth) < iDay Then
                    dtDate = DateSerial(iYear, iMonth, Date.DaysInMonth(iYear, iMonth))
                Else
                    dtDate = DateSerial(iYear, iMonth, iDay)
                End If

            ElseIf (dtDate - CDate(Now.ToShortDateString)).TotalDays > 45 And bCharged = True Then

                iMonth = iMonth - 1

                If iMonth < 1 Then
                    iMonth = iMonth + 12
                    iYear = dtDate.Year - 1
                Else
                    iYear = dtDate.Year
                End If

                If Date.DaysInMonth(iYear, iMonth) < iDay Then
                    dtDate = DateSerial(iYear, iMonth, Date.DaysInMonth(iYear, iMonth))
                Else
                    dtDate = DateSerial(iYear, iMonth, iDay)
                End If

            End If

            If lFrequency < -1 And bFirstCharge = True Then
                dtDate = ProcessUnlimitedSpecial(dtDate, cCust, MonthlyAcctInfo, bEnableMessage)
            End If

            'dtDate = dtDate.AddMonths(-1)

            'If dtDate <= CDate(Now.AddDays(iMinimumDaysBetweenCharges).ToShortDateString) Then
            '    dtDate = dtDate.AddMonths(1)
            'End If

            'If dtDate <= CDate(Now.AddDays(iMinimumDaysBetweenCharges).ToShortDateString) Then
            '    dtDate = dtDate.AddMonths(1)
            'End If

            Return dtDate

        Catch ex As Exception
            Return dtStart
        End Try
    End Function

    Public Function ProcessUnlimitedSpecial(ByVal dtDate As Date, ByVal cCust As CustomerProperties, ByVal MonthlyAcctInfo As CustomerAccountProperties, ByVal bEnableMessage As Boolean) As Date
        Dim dtLastMonthlySale As Date = CDate("1/1/1900")
        Dim bSpecialEligible As Boolean = True
        Dim sMessage As String = ""
        Try

            'If cCust.MonthlyStart < Today Then
            '    bSpecialEligible = False
            'End If

            bSpecialEligible = GetUnlimitedSpecialEligibility(MonthlyAcctInfo.iNumberofInactiveDaysForSpecial, cCust.CustomerId, dtLastMonthlySale)
            'If MonthlyAcctInfo.iNumberofInactiveDaysForSpecial > 0 And bSpecialEligible = True Then

            '    dtLastMonthlySale = Customer.getLastMonthlySaleDate(cCust.CustomerId)

            '    If dtLastMonthlySale > CDate("1/1/1900") Then
            '        If (Now - dtLastMonthlySale).TotalDays < MonthlyAcctInfo.iNumberofInactiveDaysForSpecial Then
            '            bSpecialEligible = False
            '        End If
            '        'Else
            '        '    bSpecialEligible = True
            '    End If
            '    'Else
            '    '    bSpecialEligible = True
            'End If

            If bSpecialEligible = True Then



                Select Case MonthlyAcctInfo.ChargeFrequency
                    Case -2
                        dtDate = dtDate.AddMonths(1)
                        sMessage = "one month"
                    Case -3
                        dtDate = dtDate.AddMonths(2)
                        sMessage = "two months"
                    Case -4
                        dtDate = dtDate.AddMonths(3)
                        sMessage = "three months"
                    Case -5
                        dtDate = dtDate.AddMonths(4)
                        sMessage = "four months"

                    Case Is < -29
                        dtDate = dtDate.AddDays(MonthlyAcctInfo.ChargeFrequency * -1)
                        sMessage = MonthlyAcctInfo.ChargeFrequency * -1 & " days"
                End Select

                'If bEnableMessage = True Then
                '    If dtLastMonthlySale = CDate("1/1/1900") Then
                '        frmMsgBox("New activation. Customer is eligible for " & sMessage & " free.")
                '    Else
                '        frmMsgBox((Now - dtLastMonthlySale).Days & " days since last activation. Account is eligible for " & sMessage & " free.")
                '    End If
                'End If


                'ElseIf bEnableMessage = True Then
                '    frmMsgBox((Now - dtLastMonthlySale).Days & " days since last activation. Account is not eligible for special.")

            End If

            Return dtDate
        Catch ex As Exception
            Return dtDate
        End Try
    End Function

    'bolUnlimitedAutoCharge indicates that we don't want any prompts b/c sending email receipts during late night unlimited autocharge processing
    Public Function EmailTicketIfConfigured(ByVal lTicketID As Long, ByVal bolNewTicket As Boolean, ByVal bPrintUnlimitedContract As Boolean, Optional ByVal bolUnlimitedAutoCharge As Boolean = False, Optional ByVal sEmailAddress As String = "", Optional ByVal lCUTId As Long = 0) As String

        Dim sEmailAddressSendTo As String = ""
        Dim sEmailResponse As String = ""
        Try
            If bolUnlimitedAutoCharge And iSendUnlimitedAutoChargeEmail = 0 Then
                Return sEmailResponse
            End If

            If bolUnlimitedAutoCharge And iSendUnlimitedAutoChargeEmail = 1 Then
                sEmailAddressSendTo = Trim(sEmailAddress)
                If sEmailAddressSendTo = "" Then
                    Return sEmailResponse
                End If
            Else
                'If iSendCashierReceiptEmail = 1 Then
                '    If frmMsgBox("Would the customer like to receive a receipt via email?", 6, 2) = cnstNo Then 'cnstYesNo, cnstQuestion) = cnstYes Then
                '        Exit Function
                '    End If
                'ElseIf iSendCashierReceiptEmail = 2 Then
                '    'Automatic mode; attempt to send email without the above prompt
                'Else
                Return sEmailResponse
                'End If

                ''Check to see if Customer selected first.  If not, ask them to choose/create customer first
                ''If they cancel out of that, then no need to send email, so exit sub
                ''If customer selected but no email on file, prompt for email only and update customer
                'If CustomerInfo.CustomerId = 0 Then
                '    If frmMsgBox("Please select/create a customer in order to" & vbCrLf & "store their email address and send them a receipt.", 7) = cnstCancel Then
                '        Exit Function
                '    End If
                '    frmLeftReceiptForm.EditCustomer(False)

                '    'update ticket with customer and/or ticket info
                '    If CustomerInfo.CustomerId <> 0 Then
                '        If CLng(frmLeftReceiptForm.lblInvoiceNumber.Tag) <> 0 Then
                '            Call UpdateTicketCustomerID(CLng(frmLeftReceiptForm.lblInvoiceNumber.Tag), CustomerInfo.CustomerId)
                '        End If
                '        If VehicleInfo.VehicleID <> 0 Then
                '            Call UpdateTicketVehicleID(CLng(frmLeftReceiptForm.lblInvoiceNumber.Tag), VehicleInfo.VehicleID)
                '        End If
                '    Else
                '        'Customer not select/created; don't bother sending email
                '        Exit Function
                '    End If
                'End If

                'sEmailAddressSendTo = Trim(CustomerInfo.Email)
                'If sEmailAddressSendTo = "" Then
                '    sEmailAddressSendTo = Trim(KeyboardInput("E-Mail ", 30, , , , , , True))
                '    'Save the email address if customer selected but no email on file
                '    If CustomerInfo.CustomerId <> 0 And Trim(CustomerInfo.Email) = "" Then
                '        CustomerInfo.Email = sEmailAddressSendTo
                '        Customer.SaveCustomerInfo(CustomerInfo, UserInfo.UserName, UserInfo.UserID, sComputerName)
                '    End If
                'End If

            End If


        Catch ex As Exception

        End Try


        If sEmailAddressSendTo = "" Then Return sEmailResponse


        Dim sSQL As String
        Dim DA As New SqlDataAdapter
        Dim dtReport(10) As DataTable
        Dim strConnection As String = "server=" & sServerName & ";database=" & sDatabaseName & ";uid=" & sServerUser & ";pwd=" & sServerPassword & ";"
        Dim OPOS As New RawPrinterHelper

        Dim sServices(10) As String

        Dim dr As DataRow
        Dim dsTicket As New DataSet
        Dim dsTables As New DataSet
        Dim sTicket As String = ""
        Dim sTicketEmail As String = ""

        Dim sDetails As String = ""
        Dim sDetailsEmail As String = ""
        Dim sVip As String = ""
        Dim sTemp2 As String = ""
        Dim sTemp3 As String = ""
        Dim sPayment As String = ""
        Dim sSurvey As String = ""
        Dim sName As String = ""
        Dim sBarcode As String = ""
        Dim sAccount As String = ""

        Dim lTicketCustId As Long = 0
        Dim tempCustomer As New CustomerProperties
        Dim lEarnedPoints As Double = 0
        'Dim lRedeemPoints As Long = 0
        Dim lTotalPoints As Double = 0

        Dim iCounter As Integer = 0
        Dim sPincodeTextEmail As String = ""

        Dim sVipEarned As String = ""
        Dim sVipRedeem As String = ""
        Dim sNoVip As String = ""

        Dim sTemp As String = ""
        Dim bHouse As Boolean = False
        Dim sCompany As String = ""
        Dim bCash As Boolean = False
        Dim bCredit As Boolean = False
        Dim bGift As Boolean = False
        Dim bCheck As Boolean = False
        Dim bPaidOther As Boolean = False
        Dim sPaidType As String = ""
        Dim pCounter As Integer = 0

        Dim dtTicket As DateTime

        Dim sVipPoints As String = ""
        Dim sServiceCode As String = ""

        Dim sPlate As String = ""
        Dim lticketVehId As Long = 0

        Dim ticketPayments(0) As PaymentProperties
        Dim payCounter As Integer = 0

        Dim ReceiptCoupon As New DiscountProperties

        Dim sStatus As String = ""

        Dim sDate As String = ""

        Dim tempPointsInfo(0) As PointsProperties

        Dim ReceiptBook As PrepaidBookProperties
        Dim sReceiptCoupon As String = ""

        Dim sOrderId As String = ""

        Dim sCCType As String = ""
        Dim sCCNumber As String = ""

        'Dim detailDesc(12) As String
        'Dim detailPrice(12) As Double
        Dim i As Integer = 0

        Dim tempTicket As New TicketProperties

        Dim lPincodeBookId As Long = 0

        Dim sCashVoucher As String = ""

        Dim sUserName As String = ""
        Dim sUserFirstName As String = ""

        'Dim sHouseAccountBarcode As String = ""
        Dim bMacrosOnReceipts As Boolean = False
        Dim rptDate As Date
        Try


            rptDate = Now

            If bMacrosOnReceipts = True Then
                sSQL = "Select * from TicketsDetails where (lmacroid = 0 or lmacroid is null) and Ticket_ID = " & lTicketID
            Else
                sSQL = "Select * from TicketsDetails where Ticket_ID = " & lTicketID
            End If

            sSQL = sSQL & " order by Ticket_Detail_ID"

            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(0) = New DataTable("TicketsDetails")
            DA.Fill(dtReport(0))
            dsTables.Tables.Add(dtReport(0))

            sSQL = "Select * from Account"
            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(1) = New DataTable("Account")
            DA.Fill(dtReport(1))
            dsTables.Tables.Add(dtReport(1))

            sSQL = "Select * from Tickets where Ticket_ID = " & lTicketID '" & Trim$(sTicketNumber) & "'"
            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(2) = New DataTable("Tickets")
            DA.Fill(dtReport(2))
            dsTables.Tables.Add(dtReport(2))

            'If no ticket info no need to send an email
            If bolUnlimitedAutoCharge And Not dsTables.Tables("Tickets").Rows.Count > 0 Then
                Return sEmailResponse
            End If

            sSQL = "Select * from VIPAccounts"
            'DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(3) = New DataTable("VIPAccounts")
            'DA.Fill(dtReport(3))
            dsTables.Tables.Add(dtReport(3))

            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                If Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("lCustomerID")) Then lTicketCustId = CLng(dsTables.Tables("Tickets").Rows(0).Item("lCustomerID"))
                If Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("lVehicleid")) Then lticketVehId = CLng(dsTables.Tables("Tickets").Rows(0).Item("lVehicleid"))
                If Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("sUserName")) Then sUserName = dsTables.Tables("Tickets").Rows(0).Item("sUserName").ToString

            End If


            sSQL = "Select * from Customers where lCustomerId = " & lTicketCustId
            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(7) = New DataTable("Customers")
            DA.Fill(dtReport(7))
            dsTables.Tables.Add(dtReport(7))

            sSQL = "Select * from Vehicles where lVehicleId = " & lticketVehId
            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(8) = New DataTable("Vehicles")
            DA.Fill(dtReport(8))
            dsTables.Tables.Add(dtReport(8))

            'sSQL = "Select * from HouseAccounts"
            'DA = New SqlDataAdapter(sSQL, strConnection)
            'dtReport(4) = New DataTable("HouseAccounts")
            'DA.Fill(dtReport(4))
            'dsTables.Tables.Add(dtReport(4))


            sSQL = "Select * from PinCodes where lTicketID = " & lTicketID
            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(5) = New DataTable("PinCodes")
            DA.Fill(dtReport(5))
            dsTables.Tables.Add(dtReport(5))

            sSQL = "Select top 1 * from CreditCardTransactions where lReferenceId = " & lTicketID
            sSQL = sSQL & " and sTransactionType = 'SALE' order by dtcreated desc"
            DA = New SqlDataAdapter(sSQL, strConnection)
            dtReport(9) = New DataTable("CreditCardTransactions")
            DA.Fill(dtReport(9))
            dsTables.Tables.Add(dtReport(9))

            If dtReport(9).Rows.Count > 0 Then
                If Not IsDBNull(dtReport(9).Rows(0).Item("sCCNumber")) = True Then sCCNumber = dtReport(9).Rows(0).Item("sCCNumber").ToString
                If Not IsDBNull(dtReport(9).Rows(0).Item("sCCType")) = True Then sCCType = dtReport(9).Rows(0).Item("sCCType").ToString
            End If

            If bMacrosOnReceipts = True Then
                sSQL = "Select case when td.lmacroid > 0 then m.sdescription else td.sdescription end as sDescription2"
                sSQL = sSQL & ",sum(dblTotal) as dblAmount2" ', count(*)"
                sSQL = sSQL & " from ticketsdetails as td left join macros as m on td.lmacroid = m.lmacroid where  td.lmacroid > 0 and ticket_id =" & lTicketID
                sSQL = sSQL & " group by case when td.lmacroid > 0 then m.sdescription else td.sdescription end"
                'sSQL = sSQL & " order by count(*) desc"
                DA = New SqlDataAdapter(sSQL, strConnection)
                dtReport(6) = New DataTable("MacroDetails")
                DA.Fill(dtReport(6))
                dsTables.Tables.Add(dtReport(6))
            End If

            If File.Exists(My.Application.Info.DirectoryPath & "\Receipts\CashierReceiptEmail.xml") Then
                dsTicket.ReadXml(My.Application.Info.DirectoryPath & "\Receipts\CashierReceiptEmail.xml")
            Else
                dsTicket.ReadXml(My.Application.Info.DirectoryPath & "\Receipts\CashierReceipt.xml")
            End If


            Dim iRightMargin As Integer = 0
            sTicketEmail = OPOS.ProcessTicketEmail(dsTicket, dsTables)


            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("lVehicleid")) Then lticketVehId = CLng(dsTables.Tables("Tickets").Rows(0).Item("lVehicleid"))

            If lticketVehId > 0 Then
                sPlate = Vehicle.GetPlate(lticketVehId)
            Else
                sPlate = ""
            End If

            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("bHouseAccount")) Then bHouse = CBool(dsTables.Tables("Tickets").Rows(0).Item("bHouseAccount"))
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("bCash")) Then bCash = CBool(dsTables.Tables("Tickets").Rows(0).Item("bCash"))
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("bPaymentOther")) Then bPaidOther = CBool(dsTables.Tables("Tickets").Rows(0).Item("bPaymentOther"))
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("sPaymentDescription")) Then sPaidType = dsTables.Tables("Tickets").Rows(0).Item("sPaymentDescription").ToString
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("bCreditCard")) Then bCredit = CBool(dsTables.Tables("Tickets").Rows(0).Item("bCreditCard"))
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("bGiftCard")) Then bGift = CBool(dsTables.Tables("Tickets").Rows(0).Item("bGiftCard"))
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("bCheck")) Then bCheck = CBool(dsTables.Tables("Tickets").Rows(0).Item("bCheck"))
            If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("dtOut")) Then dtTicket = CDate(dsTables.Tables("Tickets").Rows(0).Item("dtOut"))

            If dtTicket < CDate("1/1/2000") Then
                If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("dtCreated")) Then dtTicket = CDate(dsTables.Tables("Tickets").Rows(0).Item("dtCreated"))
            End If

            If dtTicket < CDate("1/1/2000") Then
                dtTicket = Now
            End If

            If bMacrosOnReceipts = True Then
                For Each dr In dsTables.Tables("MacroDetails").Rows
                    If dr Is Nothing Then Exit For
                    sTemp = dr.Item("sDescription2").ToString.ToUpper & ConvertCurrency(String.Format("{0:c}", dr.Item("dblAmount2")))
                    If sTemp.Length > 39 Then
                        sTemp = Mid(dr.Item("sDescription2").ToString, 1, 39 - ConvertCurrency(String.Format("{0:c}", dr.Item("dblAmount2"))).Length)
                    Else
                        sTemp = dr.Item("sDescription2").ToString
                    End If
                    sDetails = sDetails & sTemp.ToUpper & Space(40 - sTemp.Length - ConvertCurrency(String.Format("{0:c}", dr.Item("dblAmount2"))).Length)
                    sDetails = sDetails & ConvertCurrency(String.Format("{0:c}", dr.Item("dblamount2")))
                    sDetails = sDetails & RawPrinterHelper.rptLF

                    sDetailsEmail = sDetailsEmail & sTemp.ToUpper & HtmlSpace(40 - sTemp.Length - ConvertCurrency(String.Format("{0:c}", dr.Item("dblAmount2"))).Length)
                    sDetailsEmail = sDetailsEmail & ConvertCurrency(String.Format("{0:c}", dr.Item("dblAmount2")))
                    sDetailsEmail = sDetailsEmail & HtmlSpace(iRightMargin) & "<br>"
                Next
            End If

            For Each dr In dsTables.Tables("TicketsDetails").Rows
                If dr Is Nothing Then Exit For

                'detailDesc(i) = dr.Item("sDescription").ToString
                'detailPrice(i) = Val(dr.Item("dblQty"))

                sTemp = Format(dr.Item("dblQty"), "0.## ") & dr.Item("sDescription").ToString.ToUpper & ConvertCurrency(String.Format("{0:c}", dr.Item("dblTotal")))
                If sTemp.Length > 39 Then
                    sTemp = Mid(Format(dr.Item("dblQty"), "0.## ") & dr.Item("sDescription").ToString, 1, 39 - ConvertCurrency(String.Format("{0:c}", dr.Item("dblTotal"))).Length)
                Else
                    sTemp = Format(dr.Item("dblQty"), "0.## ") & dr.Item("sDescription").ToString
                End If
                sDetailsEmail = sDetailsEmail & sTemp.ToUpper & HtmlSpace(40 - sTemp.Length - ConvertCurrency(String.Format("{0:c}", dr.Item("dblTotal"))).Length)
                sDetailsEmail = sDetailsEmail & ConvertCurrency(String.Format("{0:c}", dr.Item("dblTotal")))
                sDetailsEmail = sDetailsEmail & HtmlSpace(iRightMargin) & "<br>"
                i = i + 1
            Next


            Dim bCleanPoints As Boolean = False

            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                If Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("lCustomerID")) Then

                    lTicketCustId = CLng(dsTables.Tables("Tickets").Rows(0).Item("lCustomerID"))

                    tempCustomer = Customer.GetCustomer(lTicketCustId, "", "LWREPORTS", 9999, sComputerName)
                    If tempCustomer.CustomerId > 0 Then tempPointsInfo = cPoints.GetPoints(tempCustomer.CustomerId, bCleanPoints)


                    If tempCustomer.bVip = True Then
                        'sSQL = "Select * from VIPAccounts where lVipAccount = " & Val(dsTables.Tables("Tickets").Rows(0).Item("lVIPAccountID"))
                        'DA = New SqlDataAdapter(sSQL, strConnection)
                        ''dtReport(3) = New DataTable("VIPAccounts")
                        'DA.Fill(dsTables.Tables("VIPAccounts"))
                        ''dsTables.Tables.Add(dtReport(3))
                        'If dsTables.Tables("VIPAccounts").Rows.Count > 0 Then
                        'If Not IsDBNull(dsTables.Tables("VIPAccounts").Rows(0).Item("sAccountNumber")) Then

                        'sVip = ""
                        'sName = "Name: " & tempCustomer.LastName & "," & tempCustomer.FirstName & RawPrinterHelper.rptLF
                        'sBarcode = "VIP Account: " & tempCustomer.Barcode & RawPrinterHelper.rptLF
                        'If Not IsDBNull(dsTables.Tables("VIPAccounts").Rows(0).Item("lPointsAccumulated")) Then
                        sVip = ""
                        sName = "Name: " & tempCustomer.LastName & "," & tempCustomer.FirstName & RawPrinterHelper.rptLF
                        'sName = "Name: " & tempCustomer.LastName & "," & tempCustomer.FirstName & RawPrinterHelper.rptLF
                        If Trim(sBarcode) <> "" Then
                            sBarcode = "Barcode: " & tempCustomer.Barcode & RawPrinterHelper.rptLF
                        End If
                        sAccount = "VIP Account: " & tempCustomer.Barcode & RawPrinterHelper.rptLF

                        If Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("lPointsThisVisit")) Then

                            lEarnedPoints = CLng(dsTables.Tables("Tickets").Rows(0).Item("lPointsThisVisit"))
                            pCounter = 0
                            sVipPoints = "Reward Points: " & vbLf
                            Do Until tempPointsInfo(pCounter) Is Nothing

                                'lRedeemPoints = lRedeemPoints + tempPointsinfo(pCounter).PointsRedeemed
                                If tempPointsInfo(pCounter).ServiceCode = "" Then
                                    sServiceCode = "VIP"
                                Else
                                    sServiceCode = tempPointsInfo(pCounter).ServiceCode
                                End If
                                sVipPoints = sVipPoints & sServiceCode & ": " & tempPointsInfo(pCounter).Points + tempPointsInfo(pCounter).PointsEarned + tempPointsInfo(pCounter).PointsRedeemed & vbLf

                                pCounter = pCounter + 1
                            Loop


                            'lRedeemPoints = lRedeemPoints + lRedeemedPoints + lEarnedPoints
                            'lRedeemPoints = lEarnedPoints
                            'If bolNewTicket = True Then
                            '    lTotalPoints = tempCustomer.Points + lRedeemPoints
                            'Else
                            lTotalPoints = tempCustomer.Points
                            'End If

                            If lEarnedPoints < 0 Then
                                If AccountInfo.VipRedeemMessage <> "" Then
                                    sVipRedeem = AccountInfo.VipRedeemMessage
                                    sVipRedeem = sVipRedeem.Replace(" @vblf@ ", vbLf)
                                    sVipRedeem = sVipRedeem.Replace("@redeempoints@", Format(lEarnedPoints * -1, "0.##"))
                                    sVipRedeem = sVipRedeem.Replace("@totalpoints@", Format(lTotalPoints, "0.##"))
                                    sVipRedeem = sVipRedeem.Replace("@vippoints@", sVipPoints)
                                    sVipRedeem = sVipRedeem.Replace("@customerpoints@", GetPointString(tempPointsInfo, tempCustomer.Points, "You have "))
                                    sVip = sVip & sVipRedeem
                                    ' Else
                                    'sVip = sVip & "You have redeemed " & -(lRedeemPoints) & " award points!" & vbLf & "You have " & lTotalPoints & " points left."
                                End If
                            Else
                                If AccountInfo.VipEarnedMessage <> "" Then
                                    sVipEarned = AccountInfo.VipEarnedMessage
                                    sVipEarned = sVipEarned.Replace(" @vblf@ ", vbLf)
                                    sVipEarned = sVipEarned.Replace("@earnedpoints@", (lEarnedPoints).ToString)
                                    sVipEarned = sVipEarned.Replace("@totalpoints@", (lTotalPoints).ToString)
                                    sVipEarned = sVipEarned.Replace("@vippoints@", sVipPoints)
                                    sVipEarned = sVipEarned.Replace("@customerpoints@", GetPointString(tempPointsInfo, tempCustomer.Points, "You have "))
                                    sVip = sVip & sVipEarned
                                    'Else

                                    ' sVip = sVip & "You have earned " & lEarnedPoints & " award points!" & vbLf & "You have accumulated " & lTotalPoints & " points."
                                End If
                            End If

                        End If

                        'End If
                        'End If
                        'End If
                    Else

                        If tempCustomer.CustomerId > 0 Then
                            sVip = ""
                            sName = "Name: " & tempCustomer.LastName & "," & tempCustomer.FirstName & RawPrinterHelper.rptLF
                        End If

                        If AccountInfo.NoVipTicketMessage <> "" Then
                            sNoVip = AccountInfo.NoVipTicketMessage
                            sNoVip = sNoVip.Replace(" @vblf@ ", vbLf)
                            sVip = sVip & sNoVip
                        Else
                            'sVip = "" '"You could be earning valuable points " & Chr(10) & "towards free washes! Ask your cashier " & Chr(10) & "how you can join our VIP/Rewards club."
                        End If
                    End If

                Else

                    If AccountInfo.NoVipTicketMessage <> "" Then
                        sNoVip = AccountInfo.NoVipTicketMessage
                        sNoVip = sNoVip.Replace("@vblf@", vbLf)
                        sVip = sNoVip
                    Else
                        sVip = "" '"You could be earning valuable points " & Chr(10) & "towards free washes! Ask your cashier " & Chr(10) & "how you can join our VIP/Rewards club."
                    End If

                End If

            End If

            Dim bDisablePrepaidBookBarcodeListSearch As Boolean = True
            If dsTables.Tables("TicketsDetails").Rows.Count > 0 Then
                For iCounter = 0 To dsTables.Tables("TicketsDetails").Rows.Count - 1

                    If CLng(dsTables.Tables("TicketsDetails").Rows(iCounter).Item("Detail_Type_ID")) = 11 Then
                        If Val(dsTables.Tables("TicketsDetails").Rows(iCounter).Item("dblTotal")) < 0 Then
                            Dim tempPPB As New PrepaidBookProperties
                            'tempPPB = MLADB1.GetPrepaidBook("", CLng(dsTables.Tables("TicketsDetails").Rows(iCounter).Item("Reference_ID")), False, 0, UserInfo.UserID, UserInfo.UserName, AccountInfo.LocationDesc)
                            tempPPB = PrepaidBook.GetPrepaidBook(CLng(dsTables.Tables("TicketsDetails").Rows(iCounter).Item("Reference_ID")), False)
                            If PrepaidBook.HideBookType(tempPPB.PrepaidBookTypeId) = False Then
                                sVip = sVip & vbLf & vbLf & "PrepaidBook: " & tempPPB.Barcode & vbLf & "has " & tempPPB.Qty & " washes left."
                            End If
                            Exit For
                        Else
                            Dim tempPPB As New PrepaidBookProperties
                            'tempPPB = MLADB1.GetPrepaidBook(dsTables.Tables("TicketsDetails").Rows(iCounter).Item("sPartNumber").ToString, 0, Not bDisablePrepaidBookBarcodeListSearch, 0, UserInfo.UserID, UserInfo.UserName, AccountInfo.LocationDesc)
                            tempPPB = PrepaidBook.GetPrepaidBook(dsTables.Tables("TicketsDetails").Rows(iCounter).Item("sPartNumber").ToString, Not bDisablePrepaidBookBarcodeListSearch, False, 9999, "LWREPORTS", AccountInfo.LocationDesc)

                            If PrepaidBook.HideBookType(tempPPB.PrepaidBookTypeId) = False Then
                                sVip = sVip & vbLf & vbLf & "PrepaidBook: " & tempPPB.Barcode & vbLf & "has " & tempPPB.Qty & " washes left."
                            End If
                            Exit For
                        End If
                    End If
                Next
            End If


            'If {Tickets.lHouseAccount_ID}>0 then "House Acct: " + {HouseAccounts.sCompany} Else 
            'If lHouseAccountId > 0 Then
            'If tempCustomer.bHouse = True Then
            If bHouse = True Then

                Dim lHouseAccountId As Long = 0
                If dsTables.Tables("Tickets").Rows.Count > 0 AndAlso Not IsDBNull(dsTables.Tables("Tickets").Rows(0).Item("lHouseAccount_Id")) Then lHouseAccountId = CLng(dsTables.Tables("Tickets").Rows(0).Item("lHouseAccount_Id"))

                sSQL = "Select * from HouseAccounts where lAccountId = " & lHouseAccountId
                DA = New SqlDataAdapter(sSQL, strConnection)
                dtReport(4) = New DataTable("HouseAccounts")
                DA.Fill(dtReport(4))
                dsTables.Tables.Add(dtReport(4))

                If Not IsDBNull(dsTables.Tables("HouseAccounts").Rows(0).Item("sCompany")) Then sCompany = dsTables.Tables("HouseAccounts").Rows(0).Item("sCompany").ToString

                sPayment = "House Acct: " & sCompany 'dsTables.Tables("HouseAccounts").Rows(0).Item("sCompany").ToString '{HouseAccounts.sCompany}
            Else


                ticketPayments = Payment.GetTicketPayments(lTicketID)
                If ticketPayments.Length > 2 Then
                    sPayment = "Split Payment" & vbLf
                    Do Until ticketPayments(payCounter) Is Nothing
                        'sPayment = sPayment & "(" & Format(ticketPayments(payCounter).Amount, sCurrencyFormat)
                        sPayment = sPayment & "(" & Format(ticketPayments(payCounter).Amount, "0.00")
                        sPayment = sPayment & ") " & ticketPayments(payCounter).PaymentType & vbLf
                        payCounter = payCounter + 1
                    Loop
                Else
                    If bCash = True Then
                        sPayment = "Cash Sale"
                    End If
                    If bCredit = True Then

                        If sCCNumber.Length > 18 Then
                            sCCNumber = LogicDecrypt(sCCNumber)
                        End If

                        Dim sLastFour As String = ""
                        If sCCNumber.Contains("TOKEN") Then
                            sLastFour = Mid(sCCNumber, sCCNumber.Length - 4, 4)
                        Else
                            sLastFour = Mid(sCCNumber, sCCNumber.Length - 3)
                        End If

                        If sCCType <> "" And sCCNumber.Length > 3 Then
                            sPayment = sPayment & vbLf & sCCType & " Sale XXXX XXXX XXXX " & sLastFour
                        Else
                            sPayment = sPayment & vbLf & "Credit Card Sale"
                        End If

                    End If
                    If bGift = True Then
                        sPayment = sPayment & vbLf & "Gift Card Sale"
                        sTicketEmail = sTicketEmail.Replace("@ChangeDue@", "")
                        sTicketEmail = sTicketEmail.Replace("@Tendered@", "")
                    End If
                    Dim sCheckNumber As String = ""
                    If bCheck = True Then
                        sCheckNumber = Payment.GetCheckNumber(lTicketID)
                        sPayment = sPayment & vbLf & "Paid by Check: " & sCheckNumber
                    End If

                    If bPaidOther = True Then
                        sCheckNumber = Payment.GetCheckNumber(lTicketID)
                        sPayment = sPayment & vbLf & "Paid by " & sPaidType
                        If sCheckNumber <> "" Then sPayment = sPayment & ": " & sCheckNumber
                    End If

                    'sPayment = "---"
                End If
            End If

            'If iTicketPrintCounter >= iSurveyFreq And bPrintSurvey = True Then
            '    sSurvey = AccountInfo.sTicketSurvey
            '    sSurvey = sSurvey.Replace(" @vblf@ ", vbLf)
            '    iTicketPrintCounter = 0
            'Else
            sSurvey = "" '"You could be earning valuable points " & Chr(10) & "towards free washes! Ask your cashier " & Chr(10) & "how you can join our VIP/Rewards club."
            'End If

            If tempCustomer.PrepayAmount > 0 Then
                sTicketEmail = sTicketEmail.Replace("@CustomerPrepay@", "Prepay Balance: " & Format(tempCustomer.PrepayAmount, "0.00"))
            Else
                sTicketEmail = sTicketEmail.Replace("@CustomerPrepay@", "")
            End If

            sSurvey = sSurvey.Replace("@vblf@", "<br>")
            sSurvey = sSurvey.Replace(vbLf, "<br>")
            sTicketEmail = sTicketEmail.Replace("@Survey@", sSurvey)

            sTicketEmail = sTicketEmail.Replace("@DetailLines@", sDetailsEmail.Replace(RawPrinterHelper.rptLF, "<br>"))

            sTicketEmail = sTicketEmail.Replace("@Barcode@", sBarcode)

            sTicketEmail = sTicketEmail.Replace("@Name@", sName)

            sTicketEmail = sTicketEmail.Replace("@VIPFunction@", sVip)

            sTicketEmail = sTicketEmail.Replace("@HouseAccountBarcode@", "")

            If InStr(sUserName, " ") > 0 Then
                sUserFirstName = Mid(sUserName, 1, InStr(sUserName, " ") - 1)
            Else
                sUserFirstName = sUserName
            End If

            If bolUnlimitedAutoCharge = True Then
                sTicketEmail = sTicketEmail.Replace("YOUR CASHIER TODAY IS:", "")
                sTicketEmail = sTicketEmail.Replace("@UserName@", "")
                sTicketEmail = sTicketEmail.Replace("@UserFirstName@", "")
            Else
                sTicketEmail = sTicketEmail.Replace("@UserName@", sUserName)
                sTicketEmail = sTicketEmail.Replace("@UserFirstName@", sUserFirstName)
            End If



            sTicketEmail = sTicketEmail.Replace("@Now@", Now.ToShortDateString & " " & Now.ToShortTimeString)

            sTicketEmail = sTicketEmail.Replace("@TicketDate@", dtTicket.ToShortDateString & " " & dtTicket.ToShortTimeString)

            If sPlate <> "" Then
                sTicketEmail = sTicketEmail.Replace("@Plate@", "Plate: " & sPlate)
            Else
                sTicketEmail = sTicketEmail.Replace("@Plate@", "")
            End If

            Dim iSubTotalMaxWidth As Integer = 25

            sTemp2 = "SUBTOTAL: "
            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                sTemp3 = ConvertCurrency(String.Format("{0:c}", Val(dsTables.Tables("Tickets").Rows(0).Item("dblTotal")) - Val(dsTables.Tables("Tickets").Rows(0).Item("dblTaxes"))))
            End If
            ' sTemp3 = ConvertCurrency(sTemp3)
            sTicketEmail = sTicketEmail.Replace("@SubTotal@", sTemp2 & HtmlSpace(iSubTotalMaxWidth - sTemp2.Length - sTemp3.Length) & sTemp3 & HtmlSpace(iRightMargin))

            sTemp2 = "TAXES: "
            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                sTemp3 = ConvertCurrency(String.Format("{0:c}", Val(dsTables.Tables("Tickets").Rows(0).Item("dblTaxes"))))
            End If
            'sTemp3 = ConvertCurrency(sTemp3)
            sTicketEmail = sTicketEmail.Replace("@Taxes@", sTemp2 & HtmlSpace(iSubTotalMaxWidth - sTemp2.Length - sTemp3.Length) & sTemp3 & HtmlSpace(iRightMargin))

            sTemp2 = "TOTAL: "
            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                sTemp3 = ConvertCurrency(String.Format("{0:c}", Val(dsTables.Tables("Tickets").Rows(0).Item("dblTotal"))))
            End If
            ' sTemp3 = FormatNumber(Val(dsTables.Tables("Tickets").Rows(0).Item("dblTotal")), 2, TriState.True, )
            'sTemp3 = ConvertCurrency(sTemp3)
            sTicketEmail = sTicketEmail.Replace("@Total@", sTemp2 & HtmlSpace(iSubTotalMaxWidth - sTemp2.Length - sTemp3.Length) & sTemp3 & HtmlSpace(iRightMargin))

            sTemp2 = "TENDERED: "
            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                sTemp3 = ConvertCurrency(String.Format("{0:c}", Val(dsTables.Tables("Tickets").Rows(0).Item("dblTendered"))))
            End If
            ' sTemp3 = ConvertCurrency(sTemp3)
            sTicketEmail = sTicketEmail.Replace("@Tendered@", sTemp2 & HtmlSpace(iSubTotalMaxWidth - sTemp2.Length - sTemp3.Length) & sTemp3 & HtmlSpace(iRightMargin))

            sTemp2 = "CHANGE DUE: "
            If dsTables.Tables("Tickets").Rows.Count > 0 Then
                sTemp3 = ConvertCurrency(String.Format("{0:c}", Val(dsTables.Tables("Tickets").Rows(0).Item("dblChangeDue"))))
            End If
            'sTemp3 = ConvertCurrency(sTemp3)
            sTicketEmail = sTicketEmail.Replace("@ChangeDue@", sTemp2 & HtmlSpace(iSubTotalMaxWidth - sTemp2.Length - sTemp3.Length) & sTemp3 & HtmlSpace(iRightMargin))


            sTicketEmail = sTicketEmail.Replace("@PaymentFunction@", sPayment)


            If bolNewTicket = True Then

                ReceiptBook = PrepaidBook.GetReceiptBook(lTicketID)

                If ReceiptBook.PrepaidBookId > 0 Then 'And Val(dsTables.Tables("Tickets").Rows(0).Item("dblTotal")) >= ReceiptBookType.MinTicketAmount Then

                    sReceiptCoupon = "Scan this receipt before " & ReceiptBook.Expires.ToShortDateString
                    sReceiptCoupon = sReceiptCoupon & vbLf & " to receive " & Format(ReceiptBook.WashValue, "$0.00") & " off on a "
                    sReceiptCoupon = sReceiptCoupon & vbLf & ReceiptBook.Service & " wash."

                    'If iBTPrinterPort > 0 Then
                    '    sReceiptCoupon = sReceiptCoupon & vbLf & "#BARCODE#" & ReceiptBook.Barcode & Chr(0)
                    'Else
                    sReceiptCoupon = sReceiptCoupon & vbLf & RawPrinterHelper.rptBARCODEPRINT & ReceiptBook.Barcode & Chr(0)
                    'End If


                    sTicket = sTicket.Replace("@Coupon@", sReceiptCoupon)
                    sTicketEmail = sTicketEmail.Replace("@Coupon@", sReceiptCoupon)
                Else


                    ReceiptCoupon = Discount.GetReceiptCoupon(0, lTicketID)

                    If ReceiptCoupon.iDaysToExpire > 0 Then

                        sDate = (CDate(Now.ToShortDateString) + New TimeSpan(ReceiptCoupon.iDaysToExpire, 0, 0, 0)).ToShortDateString

                        ReceiptCoupon.sReceiptText = ReceiptCoupon.sReceiptText.Replace("@Date@", sDate)

                    End If

                    sTicket = sTicket.Replace("@Coupon@", ReceiptCoupon.sReceiptText)
                    sTicketEmail = sTicketEmail.Replace("@Coupon@", ReceiptCoupon.sReceiptText)
                End If
            Else
                sTicket = sTicket.Replace("@Coupon@", "")
                sTicketEmail = sTicketEmail.Replace("@Coupon@", "")
            End If

            If bolNewTicket = False Then
                sTicketEmail = sTicketEmail.Replace("@copy@", "(Copy)")
            Else
                sTicketEmail = sTicketEmail.Replace("@copy@", "")
            End If

            sStatus = Ticket.GetStatus(lTicketID)
            If sStatus = "" Then sStatus = "PAID"

            sTicketEmail = sTicketEmail.Replace("@status@", sStatus)

            sCashVoucher = Ticket.GetCashVoucher(lTicketID)

            If sCashVoucher <> "" Then
                'If iBTPrinterPort > 0 Then
                '    'If iPrinterLanguage = 2 Then
                '    sCashVoucher = "(Cash Voucher)" & vbLf & "#BARCODE#" & sCashVoucher & Chr(0)
                '    'Else
                '    '    sReceiptCoupon = sReceiptCoupon & vbLf & "#BARCODE#" & ReceiptBook.Barcode & Chr(0)
                '    'End If
                'Else
                sCashVoucher = "(Cash Voucher)" & vbLf & RawPrinterHelper.rptBARCODEPRINT & sCashVoucher & Chr(0)
                'End If

                sTicketEmail = sTicketEmail.Replace("@Voucher@", sCashVoucher)
            Else
                sTicketEmail = sTicketEmail.Replace("@Voucher@", "")
            End If


            If dsTables.Tables("Pincodes").Rows.Count > 0 Then

                For iCounter = 0 To dsTables.Tables("Pincodes").Rows.Count - 1

                    If Not IsDBNull(dsTables.Tables("Pincodes").Rows(iCounter).Item("lPrepaidBookId")) Then
                        lPincodeBookId = CLng(dsTables.Tables("Pincodes").Rows(iCounter).Item("lPrepaidBookId"))
                    End If

                    If lPincodeBookId > 0 Then

                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("WASH CODE:", "rptREGFONTA", "rptCENTER")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail(dsTables.Tables("Pincodes").Rows(iCounter).Item("sPinNumber").ToString, "rptBIGFONTA", "rptCENTER")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("")

                    Else
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("YOUR PIN #", "rptREGFONTA", "rptCENTER")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail(dsTables.Tables("Pincodes").Rows(iCounter).Item("sPinNumber").ToString, "rptBIGFONTA", "rptCENTER")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("TO OPERATE VACUUM:", "rptREGFONTA", "rptLEFT")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("- PRESS *", "rptREGFONTA", "rptLEFT")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("- ENTER PIN NUMBER", "rptREGFONTA", "rptLEFT")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("- PRESS # TO START VACUUM.", "rptREGFONTA", "rptLEFT")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail(" ", "rptREGFONTA", "rptLEFT")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("THANK YOU FOR YOUR BUSINESS", "rptREGFONTA", "rptLEFT")
                        sPincodeTextEmail = sPincodeTextEmail & OPOS.CreateRowForEmail("")

                    End If



                Next
                sTicketEmail = sTicketEmail.Replace("@Pincode@", sPincodeTextEmail)
            Else
                sTicketEmail = sTicketEmail.Replace("@Pincode@", sPincodeTextEmail)
            End If

            sOrderId = lTicketID.ToString

            If sOrderId.Length > 3 Then
                sOrderId = Mid(sOrderId, sOrderId.Length - 2)
            End If



            sTicketEmail = sTicketEmail.Replace("@b@", "")
            sTicketEmail = sTicketEmail.Replace("@A@", "")
            sTicketEmail = sTicketEmail.Replace("@orderid@", sOrderId)



            If bPrintUnlimitedContract = True Then
                Dim sName2 As String = ""
                Dim sContract As String = ""
                'sContract = AccountInfo.UnlimitedContract

                If dsTables.Tables("Account").Rows.Count > 0 Then
                    If Not IsDBNull(dsTables.Tables("Account").Rows(0).Item("sUnlimitedContract")) Then sContract = dsTables.Tables("Account").Rows(0).Item("sUnlimitedContract").ToString
                End If

                'tempCustomer = Customer.GetCustomer(lCustomerId, "")

                sName = tempCustomer.LastName & "," & tempCustomer.FirstName '& RawPrinterHelper.rptLF
                sName2 = tempCustomer.FirstName & " " & tempCustomer.LastName '& RawPrinterHelper.rptLF

                'sName = "Name: " & tempCustomer.LastName & "," & tempCustomer.FirstName & RawPrinterHelper.rptLF
                sBarcode = tempCustomer.Barcode '& RawPrinterHelper.rptLF
                'sAccount = tempCustomer.Barcode & RawPrinterHelper.rptLF

                'If lVehicleId > 0 Then
                '    sPlate = Vehicle.GetPlate(lVehicleId)
                'Else
                '    sPlate = ""
                'End If

                sContract = sContract.Replace("@Barcode@", sBarcode)
                sContract = sContract.Replace("@Name@", sName)
                sContract = sContract.Replace("@Name2@", sName2)
                sContract = sContract.Replace("@Plate@", sPlate)

                sContract = sContract.Replace("@vblf@", vbLf)

                sContract = sContract.Replace("@vblf@", "<br>")
                sContract = sContract.Replace(vbLf, "<br>")
                sTicketEmail = sTicketEmail.Replace("@Contract@", sContract)
                sTicketEmail = sTicketEmail.Replace("@UnlimitedContract@", sContract)
            Else
                sTicketEmail = sTicketEmail.Replace("@Contract@", "")
                sTicketEmail = sTicketEmail.Replace("@UnlimitedContract@", "")
            End If



            sTicketEmail = sTicketEmail & "<br>"



            'sTicketEmail = "<html> <body> <div> <table align=""""left"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:350px""> <tbody> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Low&nbsp;prices.&nbsp;Every&nbsp;item.&nbsp;<wbr>Every&nbsp;day.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Route10&nbsp;East,&nbsp;Roxbury&nbsp;<wbr>Mall&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Succasunna,&nbsp;NJ&nbsp;<wbr>07876&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""tel:%28973%29%C2%A0252-0633"" value=""+19732520633"" target=""_blank"">(973)&nbsp;252-0633</a>&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">SALE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>1666295&nbsp;4&nbsp;004&nbsp;17136</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0284&nbsp;<wbr>05/30/14&nbsp;02:36</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">QTY&nbsp;SKU&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRICE</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REWARDS&nbsp;NUMBER&nbsp;<wbr>5750387960&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">1&nbsp;&nbsp;&nbsp;BIC&nbsp;VELOCITY&nbsp;MECH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;070330411920&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.49&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">SUBTOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.49&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Standard&nbsp;Tax&nbsp;7.00%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.59&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$9.08&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Cash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20.00&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Cash&nbsp;Change&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.92&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;font-family:monospace;font:12pt;font-weight:bold;color:black""> TOTAL ITEMS 1 </td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;with&nbsp;Staples&nbsp;Brand&nbsp;<wbr>products,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">the&nbsp;most&nbsp;trusted&nbsp;brand&nbsp;in&nbsp;<wbr>office&nbsp;products.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;THANK&nbsp;YOU&nbsp;FOR&nbsp;SHOPPING&nbsp;AT&nbsp;<wbr>STAPLES&nbsp;!&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shop&nbsp;online&nbsp;at&nbsp;<a href=""http://www.staples.com"" target=""_blank"">www.<wbr>staples.com</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It&nbsp;pays&nbsp;to&nbsp;be&nbsp;a&nbsp;rewards&nbsp;<wbr>member.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Sign&nbsp;up&nbsp;and&nbsp;get&nbsp;5%&nbsp;back&nbsp;<wbr>in&nbsp;rewards&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;on&nbsp;everything,&nbsp;except&nbsp;<wbr>postage,&nbsp;phone/&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gift&nbsp;cards&nbsp;and&nbsp;savings&nbsp;<wbr>passes.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Plus&nbsp;free&nbsp;shipping&nbsp;on&nbsp;<a href=""http://staples.com"" target=""_blank"">stap<wbr>les.com</a>.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr style=""text-align:center;font-family:monospace;font:12pt;font-weight:normal;color:black;padding:10px 0px 0px 0px;letter-spacing:4px;margin:0""> <td>02840530141713604</td> </tr> <tr> <td align=""center""><img hspace=""90"" width=""180"" height=""60"" alt=""Barcode image for 02840530141713604"" src=""?ui=2&amp;ik=f97954799c&amp;view=fimg&amp;th=1497cdfd4bb3acea&amp;attid=0.3&amp;disp=emb&amp;realattid=34b92bb4f2cd6b05_0.1&amp;attbid=ANGjdJ__hYrhnxtDTZgpg-9NBMUQq1Lwluw1nEQU6fr_caJYRjDcz6e99HHUOQ8aCIEVgOGbpurD_JLR5x_neDt2nrg7tj60WFjXbUUGcV6AtwLF7EATaQJYGbkgUsE&amp;sz=w1600-h1000&amp;ats=1415214076367&amp;rm=1497cdfd4bb3acea&amp;zw&amp;atsh=1"" class=""CToWUd""></td> </tr> <tr style=""height:25px""> <td></td> </tr> <tr style=""text-align:center;font-family:monospace;font:10pt;font-weight:bold;color:black;padding:10px 0px 0px 0px;letter-spacing:4px;margin:0""> <td> Questions? </td> </tr> <tr> <td style=""margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black""> For more information about your purchase, please contact your local Staples store. <a href=""http://www.staples.com/sbd/cre/programs/customerservice/shipping_and_returns.html#returns_exchanges"" target=""_blank"">Click here</a> for our return policy. </td> </tr> <tr style=""height:25px""> <td></td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:8pt;font-weight:normal;color:black"">_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;CUT&nbsp;<wbr>HERE&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:13pt;font-weight:normal;font-weight:bold;color:black"">SIGN&nbsp;UP&nbsp;AND&nbsp;GET&nbsp;$10&nbsp;OFF</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:13pt;font-weight:normal;font-weight:bold;color:black"">your&nbsp;purchase&nbsp;of&nbsp;$50&nbsp;or&nbsp;more</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:11pt;font-weight:normal;font-weight:bold;color:black"">Never&nbsp;miss&nbsp;another</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:11pt;font-weight:normal;font-weight:bold;color:black"">great&nbsp;deal.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Sign&nbsp;up&nbsp;for&nbsp;Staples&nbsp;emails&nbsp;<wbr>today.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Visit&nbsp;<a href=""http://staples.com/emaildeal"" target=""_blank"">staples.com/emaildeal</a>&nbsp;<wbr>to&nbsp;get&nbsp;started</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Valid&nbsp;for&nbsp;new&nbsp;subscribers&nbsp;<wbr>only.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Coupon&nbsp;will&nbsp;be&nbsp;emailed&nbsp;within</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">2&nbsp;days&nbsp;of&nbsp;signing&nbsp;up.</td> </tr> <tr> <td align=""center""><a href=""http://weeklyad.staples.com"" target=""_blank""><img src=""?ui=2&amp;ik=f97954799c&amp;view=fimg&amp;th=1497cdfd4bb3acea&amp;attid=0.2&amp;disp=emb&amp;realattid=34b92bb4f2cd6b05_0.3&amp;attbid=ANGjdJ_-Ir8eIFPUfm_xyxE8ycZ-7uMaywysVjbnMqclfv7AFXqcKX_0107M7GMZVthbzbavhpRZ-HaJhGcBKcX1_c8A42JJenxt_BbfHLRHkzUEO4_Vg0p2h9VCKPg&amp;sz=w1600-h1000&amp;ats=1415214076370&amp;rm=1497cdfd4bb3acea&amp;zw&amp;atsh=0"" class=""CToWUd""></a></td> </tr> <tr style=""height:25px""> <td></td> </tr> <tr> <td style=""margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black""> Please do not reply to this email. This is an unmonitored address and replies to this email cannot be responded to or read. If you have any questions or comments, please contact your local Staples store. </td> </tr> </tbody></table> </div> </body> </html> "
            'sTicketEmail = "<html> <body> <div> <table align=""""left"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:350px""> <tbody> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Low&nbsp;prices.&nbsp;Every&nbsp;item.&nbsp;<wbr>Every&nbsp;day.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Route10&nbsp;East,&nbsp;Roxbury&nbsp;<wbr>Mall&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> </tbody></table> </div> </body> </html> "
            sTicketEmail = sTicketEmail.Replace("@vblf@", "<br>")
            sTicketEmail = sTicketEmail.Replace(vbLf, "<br>")
            sTicketEmail = sTicketEmail.Replace(RawPrinterHelper.rptBARCODEPRINT, "")
            sTicketEmail = sTicketEmail & " </tbody></table> </div> </body> </html> "

            sEmailResponse = SendEmail(sEmailAddressSendTo, sTicketEmail, False, lCUTId, False, "")


            Return sEmailResponse

        Catch ex As Exception
            WriteToLog(sLogFileName, "EmailTicketIfConfigured", Err.Number, Err.Description & " - " & sTicketEmail, "ERROR")
            Return sEmailResponse
        End Try

    End Function

    Public Function EmailCustomerMessage(ByVal sMessage As String, ByVal sEmailAddress As String, ByVal lCUTID As Long, bDeclineEmail As Boolean, sBarcode As String) As String
        Dim sEmailAddressSendTo As String = ""
        Dim sEmail As String = ""
        Dim rpHelper As New RawPrinterHelper

        Dim sEmailResponse As String = ""
        Try

            sEmailAddressSendTo = Trim(sEmailAddress)

            If sEmailAddressSendTo = "" Then
                Return False
            End If

            sEmail = "<html> <body> <div> <table align=""""left"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:275px; font-size:7.875pt;""> <tbody> "
            sEmail = sEmail & rpHelper.CreateRowForEmail(sMessage, RawPrinterHelper.rptREGFONTA, RawPrinterHelper.rptRIGHT)

            sEmail = sEmail & "<br>"

            'sEmail = "<html> <body> <div> <table align=""""left"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:350px""> <tbody> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Low&nbsp;prices.&nbsp;Every&nbsp;item.&nbsp;<wbr>Every&nbsp;day.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Route10&nbsp;East,&nbsp;Roxbury&nbsp;<wbr>Mall&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Succasunna,&nbsp;NJ&nbsp;<wbr>07876&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href=""tel:%28973%29%C2%A0252-0633"" value=""+19732520633"" target=""_blank"">(973)&nbsp;252-0633</a>&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">SALE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>1666295&nbsp;4&nbsp;004&nbsp;17136</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0284&nbsp;<wbr>05/30/14&nbsp;02:36</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">QTY&nbsp;SKU&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PRICE</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;REWARDS&nbsp;NUMBER&nbsp;<wbr>5750387960&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">1&nbsp;&nbsp;&nbsp;BIC&nbsp;VELOCITY&nbsp;MECH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;070330411920&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.49&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">SUBTOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;8.49&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Standard&nbsp;Tax&nbsp;7.00%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.59&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">TOTAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$9.08&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Cash&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;20.00&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Cash&nbsp;Change&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10.92&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;font-family:monospace;font:12pt;font-weight:bold;color:black""> TOTAL ITEMS 1 </td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Save&nbsp;with&nbsp;Staples&nbsp;Brand&nbsp;<wbr>products,&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">the&nbsp;most&nbsp;trusted&nbsp;brand&nbsp;in&nbsp;<wbr>office&nbsp;products.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;THANK&nbsp;YOU&nbsp;FOR&nbsp;SHOPPING&nbsp;AT&nbsp;<wbr>STAPLES&nbsp;!&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shop&nbsp;online&nbsp;at&nbsp;<a href=""http://www.staples.com"" target=""_blank"">www.<wbr>staples.com</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;It&nbsp;pays&nbsp;to&nbsp;be&nbsp;a&nbsp;rewards&nbsp;<wbr>member.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Sign&nbsp;up&nbsp;and&nbsp;get&nbsp;5%&nbsp;back&nbsp;<wbr>in&nbsp;rewards&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;on&nbsp;everything,&nbsp;except&nbsp;<wbr>postage,&nbsp;phone/&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gift&nbsp;cards&nbsp;and&nbsp;savings&nbsp;<wbr>passes.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Plus&nbsp;free&nbsp;shipping&nbsp;on&nbsp;<a href=""http://staples.com"" target=""_blank"">stap<wbr>les.com</a>.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<wbr>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr style=""text-align:center;font-family:monospace;font:12pt;font-weight:normal;color:black;padding:10px 0px 0px 0px;letter-spacing:4px;margin:0""> <td>02840530141713604</td> </tr> <tr> <td align=""center""><img hspace=""90"" width=""180"" height=""60"" alt=""Barcode image for 02840530141713604"" src=""?ui=2&amp;ik=f97954799c&amp;view=fimg&amp;th=1497cdfd4bb3acea&amp;attid=0.3&amp;disp=emb&amp;realattid=34b92bb4f2cd6b05_0.1&amp;attbid=ANGjdJ__hYrhnxtDTZgpg-9NBMUQq1Lwluw1nEQU6fr_caJYRjDcz6e99HHUOQ8aCIEVgOGbpurD_JLR5x_neDt2nrg7tj60WFjXbUUGcV6AtwLF7EATaQJYGbkgUsE&amp;sz=w1600-h1000&amp;ats=1415214076367&amp;rm=1497cdfd4bb3acea&amp;zw&amp;atsh=1"" class=""CToWUd""></td> </tr> <tr style=""height:25px""> <td></td> </tr> <tr style=""text-align:center;font-family:monospace;font:10pt;font-weight:bold;color:black;padding:10px 0px 0px 0px;letter-spacing:4px;margin:0""> <td> Questions? </td> </tr> <tr> <td style=""margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black""> For more information about your purchase, please contact your local Staples store. <a href=""http://www.staples.com/sbd/cre/programs/customerservice/shipping_and_returns.html#returns_exchanges"" target=""_blank"">Click here</a> for our return policy. </td> </tr> <tr style=""height:25px""> <td></td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:8pt;font-weight:normal;color:black"">_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;CUT&nbsp;<wbr>HERE&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_&nbsp;_</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:13pt;font-weight:normal;font-weight:bold;color:black"">SIGN&nbsp;UP&nbsp;AND&nbsp;GET&nbsp;$10&nbsp;OFF</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:13pt;font-weight:normal;font-weight:bold;color:black"">your&nbsp;purchase&nbsp;of&nbsp;$50&nbsp;or&nbsp;more</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:11pt;font-weight:normal;font-weight:bold;color:black"">Never&nbsp;miss&nbsp;another</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:11pt;font-weight:normal;font-weight:bold;color:black"">great&nbsp;deal.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Sign&nbsp;up&nbsp;for&nbsp;Staples&nbsp;emails&nbsp;<wbr>today.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Visit&nbsp;<a href=""http://staples.com/emaildeal"" target=""_blank"">staples.com/emaildeal</a>&nbsp;<wbr>to&nbsp;get&nbsp;started</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Valid&nbsp;for&nbsp;new&nbsp;subscribers&nbsp;<wbr>only.</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">Coupon&nbsp;will&nbsp;be&nbsp;emailed&nbsp;within</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">2&nbsp;days&nbsp;of&nbsp;signing&nbsp;up.</td> </tr> <tr> <td align=""center""><a href=""http://weeklyad.staples.com"" target=""_blank""><img src=""?ui=2&amp;ik=f97954799c&amp;view=fimg&amp;th=1497cdfd4bb3acea&amp;attid=0.2&amp;disp=emb&amp;realattid=34b92bb4f2cd6b05_0.3&amp;attbid=ANGjdJ_-Ir8eIFPUfm_xyxE8ycZ-7uMaywysVjbnMqclfv7AFXqcKX_0107M7GMZVthbzbavhpRZ-HaJhGcBKcX1_c8A42JJenxt_BbfHLRHkzUEO4_Vg0p2h9VCKPg&amp;sz=w1600-h1000&amp;ats=1415214076370&amp;rm=1497cdfd4bb3acea&amp;zw&amp;atsh=0"" class=""CToWUd""></a></td> </tr> <tr style=""height:25px""> <td></td> </tr> <tr> <td style=""margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black""> Please do not reply to this email. This is an unmonitored address and replies to this email cannot be responded to or read. If you have any questions or comments, please contact your local Staples store. </td> </tr> </tbody></table> </div> </body> </html> "
            'sEmail = "<html> <body> <div> <table align=""""left"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:350px""> <tbody> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Low&nbsp;prices.&nbsp;Every&nbsp;item.&nbsp;<wbr>Every&nbsp;day.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> <tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Route10&nbsp;East,&nbsp;Roxbury&nbsp;<wbr>Mall&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr> </tbody></table> </div> </body> </html> "
            sEmail = sEmail.Replace("@vblf@", "<br>")
            sEmail = sEmail.Replace(vbLf, "<br>")
            sEmail = sEmail.Replace(RawPrinterHelper.rptBARCODEPRINT, "")
            sEmail = sEmail & " </tbody></table> </div> </body> </html> "

            sEmailResponse = SendEmail(sEmailAddressSendTo, sEmail, False, lCUTID, bDeclineEmail, sBarcode)


            Return sEmailResponse


        Catch ex As Exception
            Return sEmailResponse
        End Try
    End Function

    Public Function SendEmail(ByVal Address As String, ByVal Message As String, ByVal bExcludeSubject As Boolean, ByVal lCUTId As Long, bDeclineEmail As Boolean, sBarcode As String) As String
        ' bExcludeSubject should most likely be true for text messages to save on space; 160 char limit
        ' Don't send anything if we have no SMTP Server defined in AppSettings.
        'If sMailServer = "" And sEmailAPIURL = "" Then
        Dim sEmailResponse As String = ""

        If sEmailAPIURL = "" And bDeclineEmail = False Then
            Return sEmailResponse
        End If

        If sEmailAPIURL = "" And bDeclineEmail = True And sDeclineEmailAPIURL = "" Then
            Return sEmailResponse
        End If

        'If (sMailUsername = "" Or sMailPwd = "" Or iMailPort = -1) And sEmailAPIURL = "" Then
        If sMailFromAddress = "" Then 'Or sEmailReceiptSubjectLine = "" Then
            ' If Port, ID or Pwd are not defined, exit.
            Return sEmailResponse
        End If

        'Dim mFrom As String = sComputerName & "@yourcarwa.sh" ' A properly-formed email address is mandatory in this field.  We fake a clever one.
        Dim mSubject As String = ""
        If bExcludeSubject = False Then
            'mSubject = "Alert from " & sComputerName & " (kiosk ID #" & iKioskId.ToString & ")"
            If sEmailReceiptSubjectLine <> "" Then
                mSubject = sEmailReceiptSubjectLine
            Else
                mSubject = "Message from " & AccountInfo.Name & " - " & AccountInfo.LocationDesc
            End If

        Else
            'not specifing a subject will actually send a subject of "no subject"
            mSubject = "-"
        End If

        Dim emails As List(Of String) = New List(Of String)
        Dim empty As List(Of String) = New List(Of String)

        emails.Add(Address)

        If bDeclineEmail = True And sDeclineEmailAPIURL <> "" Then
            sEmailResponse = ApiCall.SendUWCDeclineEmails(sDeclineEmailAPIURL, Address, AccountInfo.lGlobalLocationId, AccountInfo.GlobalLocationId, lCUTId, sBarcode)
            Return sEmailResponse.Replace("""", "")
        ElseIf sEmailAPIURL <> "" Then
            sEmailResponse = ApiCall.SendUWCEmails(sEmailAPIURL, "POS Receipt", mSubject, sMailFromAddress, "", "", emails, empty, empty, Message.Trim.Replace("""", "'"), AccountInfo.lGlobalLocationId, AccountInfo.GlobalLocationId, lCUTId)
            Return sEmailResponse.Replace("""", "")
        End If

        'Dim smtp As New SmtpClient(sMailServer, iMailPort)

        ''Dim TheMessage As New MailMessage(sMailUsername, Address, mSubject, Message.Trim)
        'Dim sUseAsFromAddress As String = "" 'Trim(sMailUsername)
        'If Trim(sMailFromAddress) <> "" Then
        '    sUseAsFromAddress = Trim(sMailFromAddress)
        'End If

        'Dim TheMessage As New MailMessage(sUseAsFromAddress, Address, mSubject, Message.Trim)


        'Try
        '    If iSMTPEnableSSL = 1 Then
        '        smtp.EnableSsl = True
        '    Else
        '        smtp.EnableSsl = False
        '    End If

        '    smtp.Credentials = New NetworkCredential(sMailUsername, sMailPwd)
        '    TheMessage.IsBodyHtml = True

        '    'smtp.Send(TheMessage)
        '    'smtp.SendAsync(TheMessage, mSubject & " : " & Now)
        '    AddHandler smtp.SendCompleted, AddressOf SendEmailCompletedCallback
        '    smtp.SendAsync(TheMessage, Now)
        '    'TheMessage.Dispose()
        'Catch ex As Exception
        '    'DisplayMessageDialog("Error sending alerts:  " & ex.Message, True, 0)
        '    Account.SaveLog("SendAlertEmail: error - " & ex.Message, 0, sComputerName, 30, False)
        '    TheMessage.Dispose()
        '    'smtp.Dispose()
        'Finally
        '    'smtp.Dispose()
        'End Try
    End Function

    '=====================================================================
    '= Function Name   :   g_WriteToLog
    '= Version         :   1.0.58
    '= Last Updated    :
    '= Updated By      :   MAG
    '= Comments        :   Writes sBuffer to a file(log) passed to the function.
    '=====================================================================
    Public Sub WriteToLog(ByVal sFileName As String, ByVal sFunction As String, ByVal lErrNumber As Integer, ByVal sBuffer As String, ByVal sEvent As String) 'As Object

        EventLog.WriteEntry("LWMonthly", sBuffer, EventLogEntryType.Error)
        'Dim iFileNumber As Integer
        ''Dim sTemp As String

        ''On Error Resume Next
        'Try
        '    iFileNumber = FreeFile()
        '    FileOpen(iFileNumber, My.Application.Info.DirectoryPath & "\Logs\" & Format(Now, "MMddyyyy") & ".log", OpenMode.Append)
        '    sBuffer = Format(Now, "MM/dd/yyyy hh:mm:ss tt") & " v" & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision & " FUNCTION:" & sFunction & " " & sEvent & ": " & Str(lErrNumber) & " " & sBuffer
        '    PrintLine(iFileNumber, sBuffer)
        '    FileClose(iFileNumber)

        '    'Call frmTopMenu.m_SendData(sBuffer)
        '    'On Error GoTo 0

        'Catch ex As Exception
        '    iFileNumber = iFileNumber
        'End Try
    End Sub

    Public Function CloseShift(ByVal lUserID As Long, ByVal sUsername As String, Optional ByVal sDate As String = "") As Long
        Dim lShiftNumber As Long = 0
        'Dim sSQL As String
        Dim iCounter As Integer = 0
        Try

            'For iCounter = 23 To 28
            lShiftNumber = User.GetNextShiftNumber()

            'Call UpdateShiftNumber()


            If lShiftNumber > 0 Then
                User.CloseShift(lShiftNumber, lUserID, sDate, sUsername, sComputerName)
            End If


            'ReportDB.SaveShiftJournal(lShiftNumber, lUserID)
            ' Next
            Return lShiftNumber

            Exit Function

        Catch ex As Exception
            WriteToLog(sLogFileName, "g_CloseShift", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            Return 0
        End Try


    End Function

    Public Function SaveTicketDetails(ByVal lTicketID As Long, ByVal sTicketNumber As String, ByVal sDescription As String, ByVal dblAmount As Double, ByVal sUserName As String, ByVal lUserID As Long, ByVal lGreeter As Long, ByVal bTaxable As Boolean, ByVal lReferenceID As Long, ByVal lDetailTypeID As Long, ByVal lCustomerID As Long, ByVal lVehicleID As Long, ByVal dblQty As Double, ByVal sPartNumber As String, ByVal lTicketGreeterId As Long, ByVal lMacroId As Long, ByVal lTaxId As Long, ByVal lTaxId2 As Long) As Long

        Dim lTicketDetailID As Long

        Dim iCounter As Integer = 0
        Dim TicketDetailInfo As New TicketDetailProperties
        'Dim sDID As String = GetDID()
        Try
            TicketDetailInfo.Clear()
            TicketDetailInfo.MacroId = lMacroId
            TicketDetailInfo.TaxId = lTaxId
            TicketDetailInfo.TaxId2 = lTaxId2
            TicketDetailInfo.TicketID = lTicketID
            TicketDetailInfo.TicketNumber = sTicketNumber
            TicketDetailInfo.Description = sDescription
            TicketDetailInfo.Amount = dblAmount
            TicketDetailInfo.Total = dblAmount * dblQty
            TicketDetailInfo.UserName = sUserName
            TicketDetailInfo.UserID = lUserID
            TicketDetailInfo.Taxable = bTaxable
            TicketDetailInfo.ReferenceID = lReferenceID
            TicketDetailInfo.DetailTypeID = lDetailTypeID
            TicketDetailInfo.CustomerID = lCustomerID
            TicketDetailInfo.VehicleID = lVehicleID
            TicketDetailInfo.Quantity = dblQty
            TicketDetailInfo.PartNumber = sPartNumber
            TicketDetailInfo.LocationID = AccountInfo.LocationId
            'TicketDetailInfo.ServiceCode = sservicecode

            'If lGreeterId = lUserID Then
            lTicketGreeterId = Ticket.GetGreeterId(lTicketID)
            'Else
            '    lTicketGreeterId = 0
            'End If
            'Else
            If lTicketGreeterId > 0 Then
                TicketDetailInfo.GreeterID = lTicketGreeterId
            ElseIf lGreeter > 0 Then
                TicketDetailInfo.GreeterID = lGreeter
            Else
                TicketDetailInfo.GreeterID = lUserID
            End If
            'End If

            iCounter = 1
            'If bTabletMode = True Then

            '    sDID = GetDID()
            '    Do Until lTicketDetailID > 0 Or iCounter = 10
            '        lTicketDetailID = TicketDetail.SaveWashDetails(TicketDetailInfo, sDID)

            '        If lTicketDetailID <= 0 And iCounter Mod 3 = 0 Then
            '            'iCounter = 0
            '            frmMsgBox("Error saving ticket detail. Press OK to try again.", cnstOk, 0, False, True, 10)
            '        End If
            '        iCounter = iCounter + 1

            '    Loop
            'Else
            lTicketDetailID = TicketDetail.SaveWashDetails(TicketDetailInfo)
            'End If



            'frmMsgBox("Error Saving Ticket Details. Error:" & gcnnDatabase.Errors.Item("0").Number & " Desc: " & gcnnDatabase.Errors.Item("0").Description)
            'gcnnDatabase.Execute sSQL
            Return lTicketDetailID
            Exit Function

        Catch ex As Exception
            WriteToLog(sLogFileName, "g_SaveTicketDetails", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            Return -1
        End Try

    End Function

    'Public Function CompleteTicket(ByVal iPaid As Short, ByVal iCash As Short, ByVal iCheck As Short, ByVal iCreditCard As Short, ByVal iGiftCard As Short, ByVal iHouseAccount As Short, ByVal dblTotal As Double, ByVal dblTaxes As Double, ByVal dblTendered As Double, ByVal dblChangeDue As Double, ByVal sUserName As String, ByVal lUserID As Long, ByVal lLocationID As Long, ByVal sZipcode As String, ByVal sAge As String, ByVal sGender As String, ByVal lTicketID As Long, ByVal lHouseAccountID As Long, ByVal sPlate As String, ByVal sMake As String, ByVal sCarNumber As String, ByVal sDriverName As String, ByVal sFleetCardNumber As String, ByVal sFleetCardExpDate As String, ByVal sVIPAccount As String, ByVal lPoints As Double, ByVal iPaidOther As Integer, ByVal sPaymentType As String, ByVal lCustomerId As Long, ByVal dblPrepayAmount As Double, ByVal lVehicleId As Long, ByVal bMonthly As Boolean) As Boolean
    Public Function CompleteTicket(ByVal iPaid As Integer, ByVal iCash As Integer, ByVal iCheck As Integer, ByVal iCreditCard As Integer, ByVal iGiftCard As Integer, ByVal iHouseAccount As Short, ByVal dblTotal As Double, ByVal dblTaxes As Double, ByVal dblTendered As Double, ByVal dblChangeDue As Double, ByVal sUserName As String, ByVal lUserID As Long, ByVal lLocationID As Long, ByVal sZipcode As String, ByVal sAge As String, ByVal sGender As String, ByVal lTicketID As Long, ByVal lHouseAccountID As Long, ByVal sPlate As String, ByVal sMake As String, ByVal sCarNumber As String, ByVal sDriverName As String, ByVal sFleetCardNumber As String, ByVal sFleetCardExpDate As String, ByVal sVIPAccount As String, ByVal lPoints As Double, ByVal iPaidOther As Integer, ByVal sPaymentType As String, ByVal lcustomerid As Long, ByVal dblPrepayAmount As Double, ByVal lVehicleId As Long) As Boolean

        ' Dim sSQL As String
        ' Dim rsTemp As New ADODB.Recordset
        Dim iPinQty As Long
        Dim iCounter As Long
        Dim lVIPAccountId As Long = 0
        Dim tempTicketInfo As New TicketProperties
        Dim sGiftCardNumber As String = ""
        Dim dblGiftCardAmount As Double = 0
        Dim iGCCounter As Integer = 0

        Dim lBookTypeId As Long = 0
        'Dim pbBarcode As String = ""
        Dim iPBCounter As Integer = 0
        'Dim ppbInfo As New PrepaidBookProperties

        ' Dim GCInfo As New GiftCardProperties
        Try



            If iPaid = 1 Then tempTicketInfo.Paid = True
            If iCash = 1 Then tempTicketInfo.Cash = True
            If iCheck = 1 Then tempTicketInfo.Check = True
            If iCreditCard = 1 Then tempTicketInfo.CreditCard = True
            If iGiftCard = 1 Then tempTicketInfo.GiftCard = True
            If iHouseAccount = 1 Then tempTicketInfo.HouseAccount = True
            If iPaidOther = 1 Then tempTicketInfo.PaidOther = True
            ' If lGreeterId > 0 Then
            tempTicketInfo.GreeterId = lUserID '1
            'Else
            'tempTicketInfo.GreeterId = lUserID
            'End If

            tempTicketInfo.UserId = lUserID
            'tempTicketInfo.HouseAccountId = lHouseAccountID
            tempTicketInfo.Total = dblTotal
            tempTicketInfo.Taxes = dblTaxes
            tempTicketInfo.Tendered = dblTendered
            tempTicketInfo.ChangeDue = dblChangeDue
            tempTicketInfo.UserName = sUserName

            tempTicketInfo.VehPlate = sPlate
            tempTicketInfo.vehMake = sMake
            tempTicketInfo.CarNumber = sCarNumber
            tempTicketInfo.DriverName = sDriverName
            tempTicketInfo.ZipCode = sZipcode
            tempTicketInfo.Age = sAge
            tempTicketInfo.Gender = sGender
            tempTicketInfo.FleetCardNumber = sFleetCardNumber
            tempTicketInfo.FleetCardExpDate = sFleetCardExpDate
            tempTicketInfo.VIPAccount = sVIPAccount
            tempTicketInfo.VIPAccountID = lVIPAccountId
            tempTicketInfo.PointsThisVisit = lPoints
            tempTicketInfo.TicketId = lTicketID
            tempTicketInfo.PaymentType = sPaymentType

            tempTicketInfo.CustomerId = lcustomerid
            tempTicketInfo.VehicleId = lVehicleId
            tempTicketInfo.sTerminal = sComputerName




            If lHouseAccountID > 0 Then
                'tempTicketInfo.CustomerId = lHouseAccountID
                tempTicketInfo.HouseAccountId = lHouseAccountID
            End If

            'If sVIPAccount <> "" Then
            '    lVIPAccountId = VIPAccount.getVipAccountId(sVIPAccount)
            'End If

            Ticket.UpdateWashTicket(tempTicketInfo, lUserID.ToString, 1, iPaid, False)
            TicketDetail.UpdateWashDetails(tempTicketInfo, lUserID.ToString, False)


            'Dim GCTD() As TicketDetailProperties

            'GCTD = TicketDetail.GetGiftCardDetails(tempTicketInfo.TicketId)

            'If GCTD.Length > 1 Then
            '    For iGCCounter = 0 To GCTD.Length - 2
            '        'GCInfo = GiftCard.GetGiftCardInfo(GCTD(iGCCounter).PartNumber)
            '        dblGiftCardAmount = GCTD(iGCCounter).Amount '+ GCInfo.Balance
            '        GiftCard.SaveGiftCardInfo(GCTD(iGCCounter).PartNumber, dblGiftCardAmount, UserInfo.UserName)
            '    Next
            'End If

            'Dim PBTD() As TicketDetailProperties

            'PBTD = TicketDetail.GetPrepaidBookDetails(tempTicketInfo.TicketId)

            'If PBTD.Length > 1 Then
            '    For iPBCounter = 0 To PBTD.Length - 2
            '        If PBTD(iPBCounter).Description = "Prepaid Book Sale" Then
            '            lBookTypeId = PBTD(iPBCounter).ReferenceID

            '            ppbInfo = PrepaidBook.GetPrepaidBookType(lBookTypeId)
            '            ppbInfo.Barcode = PBTD(iPBCounter).PartNumber

            '            If ppbInfo.DaysToUse > 0 Then
            '                ppbInfo.Expires = Format(Now + New TimeSpan(ppbInfo.DaysToUse, 0, 0, 0), "MM/dd/yyyy")
            '            End If

            '            'ppbInfo.LastUsed = Now

            '            PrepaidBook.SaveBook(ppbInfo, UserInfo.UserID, UserInfo.UserName, CustomerInfo.CustomerId)
            '        Else
            '            PrepaidBook.UpdateQty(PBTD(iPBCounter).ReferenceID)
            '        End If
            '    Next
            'End If


            'Now check if/how many Pin Numbers we need to Generate
            'If iOperationMode = 3 Then

            'iPinQty = TicketDetail.GetPinQty(lTicketID)

            'If iPinQty > 0 Then
            '    For iCounter = 1 To iPinQty
            '        Call CreatePinCode(CInt(frmLeftReceiptForm.lblInvoiceNumber.Tag), UserInfo.UserName, UserInfo.UserID)
            '    Next iCounter
            'End If
            ' End If

            Return True
            Exit Function


        Catch ex As Exception
            WriteToLog(sLogFileName, "g_CompleteTicket", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            'WriteToLog(gsLogFileName, "g_CompleteTicket", 0, sSQL)
            Return False
        End Try

    End Function

    Public Function SavePayment(ByVal sPaymentTypeDesc As String, ByVal lUserID As Long, ByVal sUserName As String, ByVal lTicketID As Long, ByVal sTicketNumber As String, ByVal dblAmount As Double, Optional ByVal sCheckNumber As String = "") As Boolean

        'Dim rsPayment As New ADODB.Recordset
        Dim lPaymentTypeID As Long
        'Dim sSQL As String
        Dim tempPaymentInfo As New PaymentProperties

        Try

            If Trim(sPaymentTypeDesc) <> "" Then


                lPaymentTypeID = GetPaymentID(sPaymentTypeDesc)


                If lPaymentTypeID <> 0 Then

                    tempPaymentInfo.TicketId = lTicketID
                    tempPaymentInfo.UserId = lUserID
                    tempPaymentInfo.UserName = sUserName
                    tempPaymentInfo.LocationId = 1
                    tempPaymentInfo.PaymentTypeId = lPaymentTypeID
                    tempPaymentInfo.PaymentType = sPaymentTypeDesc
                    tempPaymentInfo.TicketNumber = sTicketNumber
                    tempPaymentInfo.Amount = dblAmount
                    tempPaymentInfo.CheckNumber = sCheckNumber

                    Payment.SavePayment(tempPaymentInfo)

                End If
            End If

            Return True
            Exit Function


        Catch ex As Exception
            WriteToLog(sLogFileName, "SavePayment", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            'g_WriteToLog(gsLogFileName, "g_SavePayment", 0, sSQL)
            'On Error GoTo 0
            Return False
        End Try

    End Function

    Public Function GetPaymentID(ByVal sPaymentDesc As String) As Long
        'Dim rsPaymentType As New ADODB.Recordset
        ' Dim sSQL As String
        Dim lPaymentTypeId As Long = 0
        Try



            If Trim(sPaymentDesc) <> "" Then
                lPaymentTypeId = Payment.GetPaymentTypeId(sPaymentDesc)

            End If

            Return lPaymentTypeId

        Catch ex As Exception
            WriteToLog(sLogFileName, "GetPaymentID", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            'g_WriteToLog(gsLogFileName, "g_GetPaymentID", 0, sSQL)
            'On Error GoTo 0
            Return lPaymentTypeId
        End Try

    End Function

    Public Function GetUnlimitedSpecialEligibility(ByVal iNumberofInactiveDaysForSpecial As Integer, ByVal lCustomerId As Long, ByRef dtLastMonthlySale As Date) As Boolean
        Dim bSpecialEligible As Boolean = True
        'Dim dtLastMonthlySale As Date = CDate("1/1/1900")
        Try

            If iNumberofInactiveDaysForSpecial > 0 And bSpecialEligible = True Then

                dtLastMonthlySale = Customer.GetLastMonthlySaleDate(lCustomerId)

                If dtLastMonthlySale > CDate("1/1/1900") Then
                    If (Now - dtLastMonthlySale).TotalDays < iNumberofInactiveDaysForSpecial Then
                        bSpecialEligible = False
                    End If
                    'Else
                    '    bSpecialEligible = True
                End If
                'Else
                '    bSpecialEligible = True
            End If

            Return bSpecialEligible
        Catch ex As Exception
            Return bSpecialEligible
        End Try
    End Function

    Public Function ConvertCurrency(ByVal sTemp As String) As String
        Dim sTemp1 As String = ""
        Dim sTemp2 As String = ""
        Dim iPeriod As Integer = 0
        Try

            iPeriod = InStr(sTemp, ".")

            If iPeriod > 0 Then
                sTemp1 = Mid(sTemp, 1, iPeriod - 1)
                sTemp2 = Mid(sTemp, sTemp.Length - 2)
            Else
                sTemp1 = sTemp
                sTemp2 = ""
            End If

            'Select Case sCurrencyFormat
            '    Case "C"
            '        sTemp = sTemp1.Replace("$", "C")
            '        sTemp = sTemp.Replace(",", ".")
            '        sTemp = sTemp & sTemp2.Replace(".", ",")
            'End Select

            Return sTemp

        Catch ex As Exception
            Return sTemp
        End Try
    End Function

    Public Function HtmlSpace(ByVal Number As Integer) As String
        Dim sReturn As String = ""
        Try
            Dim iCounter As Integer = 0
            For iCounter = 1 To Number
                sReturn = sReturn & "&nbsp;"
            Next
        Catch ex As Exception

        End Try

        Return sReturn

    End Function

    Public Function GetPointString(ByVal pPoints As PointsProperties(), ByVal iPoints As Double, ByVal sStart As String) As String
        Dim sPoints As String = ""
        Dim pCounter As Integer = 0
        Try

            sPoints = sStart

            Do Until pPoints(pCounter) Is Nothing


                If sPoints <> sStart Then
                    sPoints = sPoints & ", "
                End If

                sPoints = sPoints & pPoints(pCounter).Points & " " & pPoints(pCounter).ServiceCode '& " points"

                pCounter = pCounter + 1
            Loop

            If sPoints <> sStart Then
                If iPoints > 0 Then
                    sPoints = sPoints & ", "
                    sPoints = sPoints & iPoints & " VIP points."
                Else
                    'sPoints = sPoints & " VIP points."
                End If
            Else
                sPoints = sPoints & iPoints & " VIP points."
            End If

            Return sPoints
        Catch ex As Exception
            Return sPoints
        End Try
    End Function

    Public Function GetDatasetFromByteArray(ByVal bBytes As Byte()) As DataSet
        Dim dsResponse As New DataSet
        Try

            Dim myStream As New System.IO.MemoryStream(bBytes)
            dsResponse.ReadXml(myStream)


            Return dsResponse
        Catch ex As Exception
            Return dsResponse
        End Try
    End Function

    Public Function GetPCCWDB() As Boolean



        Dim bReturn As Boolean = False

        Dim sConn As String = ""
        Dim dcConn As New OleDb.OleDbConnection()
        Dim DA As New OleDb.OleDbDataAdapter
        Dim sSql As String = ""
        Dim dcCom As New OleDb.OleDbCommand
        Dim drReader As OleDb.OleDbDataReader
        Dim dbPath As String = ""

        Dim pccwTrans(0) As PCCWProperties
        Dim iCounter As Integer = 0

        Dim sDate As String = ""



        'Dim dbAuth As String = ""
        'Dim dbRef As String = ""
        'Dim dbIssuer As String = ""
        'Dim dbResult As String = ""

        Try

            If sCCProgramPath = "" And sCCDBPath = "" Then
                Return False
            ElseIf sCCProgramPath <> "" Then
                dbPath = sCCProgramPath
            Else
                dbPath = sCCDBPath
            End If
            dcConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=" & dbPath & "\pccw.mdb;Mode=Share Deny None"


            dcConn.Open()

            'If sTransType = "SALE" Then
            '    sSql = "Select * from Trans where Ticket like '" & Me.lblReferenceID.Text & Space(10 - Me.lblReferenceID.Text.Length) & "' and Result like 'CAPTURED%' and Date = #" & Now.Date & "# and Action = 1"
            'ElseIf sTransType = "REFUND" Then
            '    sSql = "Select * from Trans where Ticket like '" & Me.lblReferenceID.Text & Space(10 - Me.lblReferenceID.Text.Length) & "' and Result like 'CAPTURED' and Date = #" & Now.Date & "# and Action = 2"
            'Else
            '    sSql = "Select * from Trans where Ticket like '" & Me.lblReferenceID.Text & Space(10 - Me.lblReferenceID.Text.Length) & "' and Result like 'CAPTURED' and Date = #" & Now.Date & "#"
            'End If
            sSql = "Select * from Trans where Date = #" & Now.Date & "#"
            'sSql = "Select * from Trans" ' where Date = #" & Now.Date & "#"


            dcCom.CommandText = sSql
            dcCom.Connection = dcConn


            drReader = dcCom.ExecuteReader
            'dcConn.Close()

            Do While drReader.Read = True
                ReDim Preserve pccwTrans(iCounter + 1)
                pccwTrans(iCounter) = New PCCWProperties
                ' If iCounter = drReader. Then Exit Do

                If Not IsDBNull(drReader("Manual")) Then pccwTrans(iCounter).bManual = CBool(drReader("Manual"))
                If Not IsDBNull(drReader("Amount")) Then pccwTrans(iCounter).dblAmount = CDbl(drReader("Amount"))
                If Not IsDBNull(drReader("Action")) Then pccwTrans(iCounter).iAction = CInt(drReader("Action"))
                If Not IsDBNull(drReader("BatchNumber")) Then pccwTrans(iCounter).iBatch = CInt(drReader("BatchNumber"))
                If Not IsDBNull(drReader("Date")) Then sDate = CDate(drReader("Date")).ToShortDateString 'CDate(CDate(drReader("Date")).Date.ToString & " " & drReader("Time").ToString)
                If Not IsDBNull(drReader("Time")) Then sDate = sDate & " " & drReader("Time").ToString 'pccwTrans(iCounter).TransactionDate = CDate(CDate(drReader("Date")).Date.ToString & " " & drReader("Time").ToString)
                pccwTrans(iCounter).TransactionDate = CDate(sDate)
                If Not IsDBNull(drReader("Auth")) Then pccwTrans(iCounter).sAuth = drReader("Auth").ToString.Trim
                If Not IsDBNull(drReader("Card")) Then pccwTrans(iCounter).sCardNumber = drReader("Card").ToString.Trim
                If Not IsDBNull(drReader("CardPresent")) Then pccwTrans(iCounter).sCardPresent = drReader("CardPresent").ToString.Trim
                If Not IsDBNull(drReader("Issuer")) Then pccwTrans(iCounter).sIssuer = drReader("Issuer").ToString.Trim
                If Not IsDBNull(drReader("Member")) Then pccwTrans(iCounter).sMember = drReader("Member").ToString.Trim
                If Not IsDBNull(drReader("Processor")) Then pccwTrans(iCounter).sProcessor = drReader("Processor").ToString.Trim
                If Not IsDBNull(drReader("Ref")) Then pccwTrans(iCounter).sRef = drReader("Ref").ToString.Trim
                If Not IsDBNull(drReader("Result")) Then pccwTrans(iCounter).sResult = drReader("Result").ToString.Trim
                If Not IsDBNull(drReader("Result_Ref")) Then pccwTrans(iCounter).sResultRef = drReader("Result_Ref").ToString.Trim
                If Not IsDBNull(drReader("Status")) Then pccwTrans(iCounter).sStatus = drReader("Status").ToString.Trim
                If Not IsDBNull(drReader("Ticket")) Then pccwTrans(iCounter).sTicket = drReader("Ticket").ToString.Trim
                If Not IsDBNull(drReader("TID")) Then pccwTrans(iCounter).sTID = drReader("TID").ToString.Trim
                If Not IsDBNull(drReader("TroutD")) Then pccwTrans(iCounter).sTroutD = drReader("TroutD").ToString.Trim


                iCounter = iCounter + 1
            Loop

            PCCW.SaveTrans(pccwTrans)

            drReader.Close()
            dcConn.Close()



            Return bReturn

        Catch ex As Exception
            WriteToLog(sLogFileName, "GetPCCWDB", Err.Number, ex.Message, "ERROR")
            'dcConn.Close()
            Return False
        End Try
    End Function

    Public Function CompleteGiftCardSale(ByVal bBalanceTicket As Boolean, ByVal sBalance As String, ByVal sReferenceNumber As String, ByVal lReferenceId As Long, ByVal sInvNumber As String, ByVal sGCNumber As String, ByVal sExpDate As String, ByVal dblChargeAmount As Double, ByVal bManualEntry As Boolean, ByVal sComments As String, ByVal bPrintReceipt As Boolean, lTicketId As Long) As Boolean
        'Dim sSQL As String

        'check to see if its captured
        'If (ccCharge.GetCaptured = True And miTransactionType = iSale) Or _
        ''    (UCase$(ccCharge.GetResult) = "PROCESSED" And miTransactionType = iReturn) Then
        Dim tempGCTransInfo As New GiftCardTransPropreties

        'Dim sDID As String = GetDID()
        Dim lReturnId As Long = 0
        Dim iCounter As Integer = 0

        Try

            tempGCTransInfo.UserId = userMonthly.UserID
            tempGCTransInfo.ReferenceNumber = sReferenceNumber 'lblReferenceID.Text

            If bBalanceTicket = True Then
                tempGCTransInfo.ReferenceId = lReferenceId * -1 'CLng(lblReferenceID.Tag) * -1
            Else
                tempGCTransInfo.ReferenceId = lReferenceId 'CLng(lblReferenceID.Tag)
            End If

            tempGCTransInfo.InvNumber = sInvNumber 'lblReferenceNumber.Text
            tempGCTransInfo.CCNumber = sGCNumber 'Trim(lblCardNumber.Text)
            tempGCTransInfo.CCExpDate = sExpDate 'Trim(lblExpDate.Text)
            tempGCTransInfo.TotalAmount = dblChargeAmount 'CDbl(lblChargeAmount.Text)
            tempGCTransInfo.TerminalId = "00000000"
            tempGCTransInfo.AuthNumber = "0001"
            tempGCTransInfo.RefNumber = sInvNumber 'Trim(lblReferenceNumber.Text)

            tempGCTransInfo.ProgramVersion = My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision '& "',"
            tempGCTransInfo.CCType = "GIFT CARD"
            tempGCTransInfo.TransactionType = "SALE"
            tempGCTransInfo.sGiftCard_Balance = sBalance

            'If Val(lblExpDate.Tag) = 1 Then
            '    tempGCTransInfo.bManualEntry = True
            'End If
            tempGCTransInfo.bManualEntry = bManualEntry

            'If miTransactionType = NovaPLUSPOS.TranType.iSale Then
            '    tempGCTransInfo.TransactionType = "SALE"
            'ElseIf miTransactionType = NovaPLUSPOS.TranType.iReturn Then
            '    tempGCTransInfo.TransactionType = "REFUND"
            'Else
            '    tempGCTransInfo.TransactionType = "UNKNOWN"
            'End If
            lReturnId = 0
            iCounter = 1
            'If bTabletMode = True Then

            '    sDID = GetDID()
            '    Do Until lReturnId > 0 Or iCounter = 10
            '        lReturnId = GiftCard.SaveTransaction(tempGCTransInfo, sDID)

            '        If lReturnId <= 0 And iCounter Mod 3 = 0 Then
            '            'iCounter = 0
            '            frmMsgBox("Error updating ticket details. Press OK to try again.", cnstOk, 0, False, True, 10)
            '        End If
            '        iCounter = iCounter + 1

            '    Loop
            'Else
            GiftCard.SaveTransaction(tempGCTransInfo)
            'End If
            'Call SaveGiftCardInfo(Trim(lblCardNumber.Text), CDbl(lblChargeAmount.Text) * -1, (frmStatus1.lblUserID.Text), CustomerInfo.CustomerId)
            'If sGiftCardTerminal = "" Or (sGiftCardTerminal <> "" And sGiftCardProcessor.ToUpper = "P2P" And GiftCard.CardExists(sGCNumber, False) = True) Then
            GiftCard.SaveGiftCardInfo(sGCNumber, dblChargeAmount * -1, userMonthly.UserID.ToString, 0, CDate("1/1/1900"), AccountInfo.LocationDesc, sComments, 0, New Guid)
            'End If
            Ticket.UpdateTicketAfterTrans(lTicketId, sComputerName)


            'If bPrintReceipt = True Then
            '    'MsgBox "REF_ID.TAG: " & lblReferenceID.Tag & " REF_ID: " & lblReferenceID.Caption
            '    If bBalanceTicket = True Then
            '        Call frmGiftCardForm.PrintGiftCardReceipt(lReferenceId * -1, dblChargeAmount, sReferenceNumber, "0001")
            '    Else
            '        Call frmGiftCardForm.PrintGiftCardReceipt(lReferenceId, dblChargeAmount, sReferenceNumber, "0001")
            '    End If
            '    'If bCutReceipts = True Then
            '    '    Call CutReceipt()
            '    'End If
            'End If


            ''Customer  Copy
            'Call PrintGiftCardReceipt(CInt(lblReferenceID.Tag), CDbl(lblChargeAmount.Text), (lblReferenceID.Text), "0001", "GCRECEIPTPAID.RPT")
            'If bCutReceipts = True Then
            '    Call CutReceipt()
            'End If


            Return True
            ''Me.Enabled = True
            ''Unload Me
            '    Else
            '
            '        'Transaction was not captured
            '        'this means it was a declined transaction or
            '        'a non monentary transaction.
            '
            '        'get result
            '        m_CompleteGiftCardSale = False
            '        Me.Enabled = True
            '        Me.Response = False
            '        'Get reason
            '    End If
            ' Exit Function

        Catch ex As Exception
            WriteToLog(sLogFileName, "m_CompleteGiftCardSale", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            'g_WriteToLog(gsLogFileName, "m_CompleteGiftCardSale(SQL STRING)", Err.Number, sSQL)
            'Me.Enabled = True
            'Me.Response = False
            Return False

            'lblTitle.Text = "PROGRAM ERROR " & Err.Number & " PLEASE NOTIFY OFFICE."
            'lblTitle.Refresh()
        End Try

    End Function

End Module
