'Option Strict On
Imports System.IO
Imports System.Drawing.Printing
Imports System.Runtime.InteropServices



Public Class RawPrinterHelper

    Public Const rptLF As String = Chr(10)
    Public Const rptLF2 As String = Chr(27) & Chr(100) & Chr(2)
    Public Const rptLFREVERSE As String = Chr(27) & Chr(101) & Chr(2)
    Public Const rptINIT As String = Chr(27) & Chr(64)
    Public Const rptREGFONTA As String = Chr(27) & Chr(33) & Chr(0)
    Public Const rptREGFONTB As String = Chr(27) & Chr(33) & Chr(1)
    Public Const rptREGBOLDFONTA As String = Chr(27) & Chr(33) & Chr(8)
    Public Const rptREGBOLDFONTB As String = Chr(27) & Chr(33) & Chr(9)
    Public Const rptBIGFONTA As String = Chr(27) & Chr(33) & Chr(48)
    Public Const rptBIGFONTB As String = Chr(27) & Chr(33) & Chr(49)
    Public Const rptBIGBOLDFONTA As String = Chr(27) & Chr(33) & Chr(56)
    Public Const rptBIGGERBOLDFONTA As String = Chr(27) & Chr(33) & Chr(56) & Chr(29) & Chr(33) & Chr(34)
    Public Const rptBIGGESTBOLDFONTA As String = Chr(27) & Chr(33) & Chr(56) & Chr(29) & Chr(33) & Chr(51)
    Public Const rptBIGBOLDFONTB As String = Chr(27) & Chr(33) & Chr(57)
    Public Const rptCENTER As String = Chr(27) & Chr(97) & Chr(1)
    Public Const rptLEFT As String = Chr(27) & Chr(97) & Chr(0)
    Public Const rptRIGHT As String = Chr(27) & Chr(97) & Chr(2)
    Public Const rptPARTIALCUT As String = Chr(29) & Chr(86) & Chr(66) & Chr(0)
    Public Const rptBARCODESMALL As String = Chr(29) & Chr(104) & Chr(50)
    Public Const rptBARCODEMED As String = Chr(29) & Chr(104) & Chr(127)
    Public Const rptBARCODETHIN As String = Chr(29) & Chr(119) & Chr(2)
    Public Const rptBARCODETHICK As String = Chr(29) & Chr(119) & Chr(4)
    Public Const rptBARCODEPRINT As String = rptBARCODESMALL & rptBARCODETHIN & Chr(29) & Chr(107) & Chr(4)
    Public Const rptBARCODEPRINT2 As String = rptBARCODEMED & rptBARCODETHICK & Chr(29) & Chr(107) & Chr(4)
    Public Const rptPRINTLOGO As String = Chr(28) & Chr(112) & Chr(1) & Chr(0)
    Public Const rptPRINTLOGO1 As String = Chr(28) & Chr(112) & Chr(2) & Chr(0)
    Public Const rptPRINTLOGO2 As String = Chr(28) & Chr(112) & Chr(3) & Chr(0)
    Public Const rptPRINTLOGO3 As String = Chr(28) & Chr(112) & Chr(4) & Chr(0)
    Public Const rptPRINTLOGO4 As String = Chr(28) & Chr(112) & Chr(5) & Chr(0)
    Public Const rptPRINTLOGO5 As String = Chr(28) & Chr(112) & Chr(6) & Chr(0)


    ' Structure and API declarions:
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Unicode)> _
    Structure DOCINFOW
        <MarshalAs(UnmanagedType.LPWStr)> Public pDocName As String
        <MarshalAs(UnmanagedType.LPWStr)> Public pOutputFile As String
        <MarshalAs(UnmanagedType.LPWStr)> Public pDataType As String
    End Structure

    <DllImport("winspool.Drv", EntryPoint:="OpenPrinterW", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function OpenPrinter(ByVal src As String, ByRef hPrinter As IntPtr, ByVal pd As IntPtr) As Boolean
    End Function
    <DllImport("winspool.Drv", EntryPoint:="ClosePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function ClosePrinter(ByVal hPrinter As IntPtr) As Boolean
    End Function
    <DllImport("winspool.Drv", EntryPoint:="StartDocPrinterW", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function StartDocPrinter(ByVal hPrinter As IntPtr, ByVal level As Int32, ByRef pDI As DOCINFOW) As Boolean
    End Function
    <DllImport("winspool.Drv", EntryPoint:="EndDocPrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function EndDocPrinter(ByVal hPrinter As IntPtr) As Boolean
    End Function
    <DllImport("winspool.Drv", EntryPoint:="StartPagePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function StartPagePrinter(ByVal hPrinter As IntPtr) As Boolean
    End Function
    <DllImport("winspool.Drv", EntryPoint:="EndPagePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function EndPagePrinter(ByVal hPrinter As IntPtr) As Boolean
    End Function
    <DllImport("winspool.Drv", EntryPoint:="WritePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function WritePrinter(ByVal hPrinter As IntPtr, ByVal pBytes As IntPtr, ByVal dwCount As Int32, ByRef dwWritten As Int32) As Boolean
    End Function

    ' SendBytesToPrinter()
    ' When the function is given a printer name and an unmanaged array of
    ' bytes, the function sends those bytes to the print queue.
    ' Returns True on success or False on failure.
    Public Shared Function SendBytesToPrinter(ByVal szPrinterName As String, ByVal pBytes As IntPtr, ByVal dwCount As Int32) As Boolean
        Dim hPrinter As IntPtr      ' The printer handle.
        Dim dwError As Int32        ' Last error - in case there was trouble.
        Dim di As DOCINFOW          ' Describes your document (name, port, data type).
        Dim dwWritten As Int32      ' The number of bytes written by WritePrinter().
        Dim bSuccess As Boolean     ' Your success code.

        ' Set up the DOCINFO structure.
        With di
            .pDocName = "My Visual Basic .NET RAW Document"
            .pDataType = "RAW"
        End With
        ' Assume failure unless you specifically succeed.
        bSuccess = False
        If OpenPrinter(szPrinterName, hPrinter, 0) Then
            If StartDocPrinter(hPrinter, 1, di) Then
                If StartPagePrinter(hPrinter) Then
                    ' Write your printer-specific bytes to the printer.
                    bSuccess = WritePrinter(hPrinter, pBytes, dwCount, dwWritten)
                    EndPagePrinter(hPrinter)
                End If
                EndDocPrinter(hPrinter)
            End If
            ClosePrinter(hPrinter)
        End If
        ' If you did not succeed, GetLastError may give more information
        ' about why not.
        If bSuccess = False Then
            dwError = Marshal.GetLastWin32Error()
        End If
        Return bSuccess
    End Function ' SendBytesToPrinter()

    ' SendFileToPrinter()
    ' When the function is given a file name and a printer name,
    ' the function reads the contents of the file and sends the
    ' contents to the printer.
    ' Presumes that the file contains printer-ready data.
    ' Shows how to use the SendBytesToPrinter function.
    ' Returns True on success or False on failure.
    Public Shared Function SendFileToPrinter(ByVal szPrinterName As String, ByVal szFileName As String) As Boolean
        ' Open the file.
        Dim fs As New FileStream(szFileName, FileMode.Open)
        ' Create a BinaryReader on the file.
        Dim br As New BinaryReader(fs)
        ' Dim an array of bytes large enough to hold the file's contents.
        Dim bytes(CInt(fs.Length)) As Byte
        Dim bSuccess As Boolean
        ' Your unmanaged pointer.
        Dim pUnmanagedBytes As IntPtr

        ' Read the contents of the file into the array.
        bytes = br.ReadBytes(CInt(fs.Length))
        ' Allocate some unmanaged memory for those bytes.
        pUnmanagedBytes = Marshal.AllocCoTaskMem(CInt(fs.Length))
        ' Copy the managed byte array into the unmanaged array.
        Marshal.Copy(bytes, 0, pUnmanagedBytes, CInt(fs.Length))
        ' Send the unmanaged bytes to the printer.
        bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, CInt(fs.Length))
        ' Free the unmanaged memory that you allocated earlier.
        Marshal.FreeCoTaskMem(pUnmanagedBytes)
        Return bSuccess
    End Function ' SendFileToPrinter()

    ' When the function is given a string and a printer name,
    ' the function sends the string to the printer as raw bytes.
    Public Shared Sub SendStringToPrinter(ByVal szPrinterName As String, ByVal szString As String)
        Dim pBytes As IntPtr
        Dim dwCount As Int32
        ' How many characters are in the string?
        dwCount = szString.Length()
        ' Assume that the printer is expecting ANSI text, and then convert
        ' the string to ANSI text.
        pBytes = Marshal.StringToCoTaskMemAnsi(szString)
        ' Send the converted ANSI string to the printer.
        SendBytesToPrinter(szPrinterName, pBytes, dwCount)
        Marshal.FreeCoTaskMem(pBytes)
    End Sub

    Public Function PrintPOSTicket(ByVal sPrinterName As String, ByVal sAccountName As String, ByVal sAccountAddress As String, ByVal sAccountCity As String, ByVal sAccountState As String, ByVal sAccountZipCode As String, ByVal sUserName As String, ByVal sTicketNumber As String, ByVal sServices As String()) As Boolean
        Dim sTemp As String

        sTemp = rptINIT & rptCENTER & rptREGFONTA
        sTemp = sTemp & sAccountName.ToUpper & rptLF
        sTemp = sTemp & sAccountAddress.ToUpper & rptLF
        sTemp = sTemp & sAccountCity.ToUpper & "," & sAccountState.ToUpper & " " & sAccountZipCode & rptLF2
        'sTemp = sTemp & rptREGBOLDFONTA & Format(Now, "MM/dd/yyyy hh:mm:ss tt") & rptLF2
        sTemp = sTemp & rptREGBOLDFONTA & Now.ToShortDateString & " " & Now.ToShortTimeString & rptLF2
        sTemp = sTemp & rptREGFONTA & "YOUR CASHIER TODAY IS: " & sUserName.ToUpper & rptLF2
        sTemp = sTemp & rptBIGBOLDFONTA & sTicketNumber & rptLF2
        sTemp = sTemp & rptREGFONTA & rptLEFT & sServices(0).ToUpper & rptLF2

        

        SendStringToPrinter(sPrinterName, sTemp)
    End Function

    Public Function PrintString(ByVal sPrinterName As String, ByVal sTemp As String) As Boolean
        SendStringToPrinter(sPrinterName, sTemp)
    End Function

    Public Function CreateRowForEmail(ByVal sText As String, Optional ByVal sFontType As String = "", Optional ByVal sAlignType As String = "") As String
        Dim sReturn As String = ""

        Try
            Dim sLineStyle As String = ""
            sLineStyle = "<tr> <td style=""text-align:;margin:1px;font-family:monospace;font:pt;font-weight:;color:black"">"

            Select Case sFontType
                Case "rptREGFONTA", "rptREGFONTB"
                    sLineStyle = sLineStyle.Replace("font:pt;", "font-size:100%;")
                    sLineStyle = sLineStyle.Replace("font-weight:;", "font-weight:normal;")
                Case "rptREGBOLDFONTA", "rptREGBOLDFONTB"
                    sLineStyle = sLineStyle.Replace("font:pt;", "font-size:100%;")
                    sLineStyle = sLineStyle.Replace("font-weight:;", "font-weight:bold;")
                Case "rptBIGFONTA"
                    sLineStyle = sLineStyle.Replace("font:pt;", "font-size:200%;")
                    sLineStyle = sLineStyle.Replace("font-weight:;", "font-weight:normal;")
                Case "rptBIGBOLDFONTA"
                    sLineStyle = sLineStyle.Replace("font:pt;", "font-size:200%;")
                    sLineStyle = sLineStyle.Replace("font-weight:;", "font-weight:bold;")
                Case "rptBIGFONTB"
                    sLineStyle = sLineStyle.Replace("font:pt;", "font-size:125%;")
                    sLineStyle = sLineStyle.Replace("font-weight:;", "font-weight:normal;")
                Case Else
                    sLineStyle = sLineStyle.Replace("font:pt;", "font-size:100%;")
                    sLineStyle = sLineStyle.Replace("font-weight:;", "font-weight:normal;")
            End Select

            'sTicketStrings(i) = sTicketStrings(i).Replace("rptREGFONTA", "T Ari08Bpt.cpf 0") ' 0 " & iY)
            'sTicketStrings(i) = sTicketStrings(i).Replace("rptREGBOLDFONTA", "T Ari08pt.cpf 0") ' 0 " & iY)
            'sTicketStrings(i) = sTicketStrings(i).Replace("rptREGFONTB", "T Ari08Bpt.cpf 0") ' 0 " & iY)
            'sTicketStrings(i) = sTicketStrings(i).Replace("rptREGBOLDFONTB", "T Ari08pt.cpf 0") ' 0 " & iY)
            'sTicketStrings(i) = sTicketStrings(i).Replace("rptBIGFONTA", "T Ari16pt.cpf 0") ' 0 " & iY)
            'sTicketStrings(i) = sTicketStrings(i).Replace("rptBIGFONTB", "T Ari10pt.cpf 0") ' 0 " & iY)
            'sTicketStrings(i) = sTicketStrings(i).Replace("rptBIGBOLDFONTA", "T Ari16pt.cpf 0") ' 0 " & iY)

            Select Case sAlignType
                Case "rptRIGHT"
                    sLineStyle = sLineStyle.Replace("text-align:;", "text-align:right;")
                Case "rptLEFT"
                    sLineStyle = sLineStyle.Replace("text-align:;", "text-align:left;")
                Case Else
                    sLineStyle = sLineStyle.Replace("text-align:;", "text-align:center;")
            End Select

            If Trim(sText) = "" Then
                sText = "<br>"
            End If

            sReturn = sReturn & sLineStyle
            sReturn = sReturn & sText
            sReturn = sReturn & "</td> </tr>"


        Catch ex As Exception
            WriteToLog(sLogFileName, "CreateRowForEmail", Err.Number, Err.Description & " - " & ex.Message, "ERROR")
            Return ""
        End Try

        Return sReturn
    End Function

    Public Function ProcessTicketEmail(ByVal dsTicket As DataSet, ByVal dsTables As DataSet) As String
        Dim i As Integer
        Dim dt As DataTable
        Dim sTicket As String
        Dim dc As DataColumn
        Try

            'keep the width at width:275px so that email looks good on android phone
            sTicket = "<html> <body> <div> <table align=""""left"" cellpadding=""0"" cellspacing=""0"" border=""0"" style=""width:275px; font-size:7.875pt;""> <tbody> "
            '"<tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;Low&nbsp;prices.&nbsp;Every&nbsp;item.&nbsp;<wbr>Every&nbsp;day.&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr>"
            '"<tr> <td style=""text-align:center;margin:1px;font-family:monospace;font:10pt;font-weight:normal;color:black"">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Route10&nbsp;East,&nbsp;Roxbury&nbsp;<wbr>Mall&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> </tr>"

            Dim sFontType As String = ""
            Dim sAlignType As String = ""
            Dim sText As String = ""

            For i = 0 To dsTicket.Tables(0).Rows.Count - 1
                If dsTicket.Tables(0).Rows(i).Item("Text").ToString = "@end@" Then Exit For
                'sTable(i) = Mid(dsTicket.Tables(0).Rows(i).Item("Text"), 0, 0)


                sFontType = dsTicket.Tables(0).Rows(i).Item("FontType").ToString
                sAlignType = dsTicket.Tables(0).Rows(i).Item("AlignType").ToString
                sText = dsTicket.Tables(0).Rows(i).Item("Text").ToString.Trim

                sTicket = sTicket & CreateRowForEmail(sText, sFontType, sAlignType)

                '& RawPrinterHelper.rptLF

            Next

            'sTicket = sTicket & " </tbody></table> </div> </body> </html> "

            For Each dt In dsTables.Tables
                If dt Is Nothing Then Exit For
                For Each dc In dt.Columns
                    If dt.Rows.Count > 0 Then
                        If Not TypeOf (dt.Rows(0).Item(dc.ColumnName)) Is Byte() Then
                            If Not IsDBNull(dt.Rows(0).Item(dc.ColumnName)) AndAlso dc.DataType.FullName <> "System.Guid" Then
                                sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", dt.Rows(0).Item(dc.ColumnName).ToString)
                                sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", Format(dt.Rows(0).Item(dc.ColumnName), "0.00"))
                                'sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", RawPrinterHelper.rptBARCODEPRINT & "TCK" & dt.Rows(0).Item(dc.ColumnName).ToString & Chr(0))
                                'sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", RawPrinterHelper.rptBARCODEPRINT & "" & dt.Rows(0).Item(dc.ColumnName).ToString & Chr(0))
                                'We don't need barcodes for email
                                sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                                sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")
                            Else
                                sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
                                sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
                                sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                                sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")

                            End If
                        End If
                    Else
                        sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
                        sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
                        sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                        sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")

                    End If
                Next

            Next

            'Ignore logos for now, mabye we can create a base 64 data string field in Accounts table 
            'http://stackoverflow.com/questions/6706891/embedding-image-in-html-email
            ' base64-data stringcreate one easily at: 
            'http://www.motobit.com/util/base64-decoder-encoder.asp from a image file
            sTicket = sTicket.Replace("@Logo@", "")
            sTicket = sTicket.Replace("@Logo1@", "")
            sTicket = sTicket.Replace("@Logo2@", "")
            sTicket = sTicket.Replace("@Logo3@", "")
            sTicket = sTicket.Replace("@Logo4@", "")
            sTicket = sTicket.Replace("@Logo5@", "")

            Return sTicket

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function ProcessTicket(ByVal dsTicket As DataSet, ByVal dsTables As DataSet, ByVal iBTPort As Integer) As String
        Dim i As Integer
        Dim dt As DataTable
        Dim sTicket As String
        Dim dc As DataColumn

        Try

            If iBTPort > 0 Then

                WriteToLog(sLogFileName, "ProcessBT", 0, "Start " & Now.ToString("hh:mm:ss fff"), "EVENT")
                sTicket = ProcessBT(dsTicket, dsTables)
                WriteToLog(sLogFileName, "ProcessBT", 0, "Stop " & Now.ToString("hh:mm:ss fff"), "EVENT")

                Return sTicket
            End If


            sTicket = rptINIT



            For i = 0 To dsTicket.Tables(0).Rows.Count - 1
                If dsTicket.Tables(0).Rows(i).Item("Text").ToString = "@end@" Or i = 39 Then Exit For
                'sTable(i) = Mid(dsTicket.Tables(0).Rows(i).Item("Text"), 0, 0)
                sTicket = sTicket & dsTicket.Tables(0).Rows(i).Item("FontType").ToString & dsTicket.Tables(0).Rows(i).Item("AlignType").ToString & dsTicket.Tables(0).Rows(i).Item("Text").ToString.Trim & RawPrinterHelper.rptLF

            Next



            For Each dt In dsTables.Tables
                If dt Is Nothing Then Exit For
                For Each dc In dt.Columns
                    If dt.Rows.Count > 0 Then
                        If Not TypeOf (dt.Rows(0).Item(dc.ColumnName)) Is Byte() Then
                            If Not IsDBNull(dt.Rows(0).Item(dc.ColumnName)) AndAlso dc.DataType.FullName <> "System.Guid" Then
                                sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", dt.Rows(0).Item(dc.ColumnName).ToString)
                                sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", Format(dt.Rows(0).Item(dc.ColumnName), "0.00"))
                                sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", RawPrinterHelper.rptBARCODEPRINT & "TCK" & dt.Rows(0).Item(dc.ColumnName).ToString & Chr(0))
                                sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", RawPrinterHelper.rptBARCODEPRINT & "" & dt.Rows(0).Item(dc.ColumnName).ToString & Chr(0))

                            Else
                                sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
                                sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
                                sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                                sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")

                            End If
                        End If
                    Else
                        sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
                        sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
                        sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                        sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")

                    End If
                Next

            Next

            sTicket = sTicket.Replace("rptREGFONTA", RawPrinterHelper.rptREGFONTA)
            sTicket = sTicket.Replace("rptREGBOLDFONTA", RawPrinterHelper.rptREGBOLDFONTA)
            sTicket = sTicket.Replace("rptREGFONTB", RawPrinterHelper.rptREGFONTB)
            sTicket = sTicket.Replace("rptREGBOLDFONTB", RawPrinterHelper.rptREGBOLDFONTB)
            sTicket = sTicket.Replace("rptBIGFONTA", RawPrinterHelper.rptBIGFONTA)
            sTicket = sTicket.Replace("rptBIGFONTB", RawPrinterHelper.rptBIGFONTB)
            sTicket = sTicket.Replace("rptBIGBOLDFONTA", RawPrinterHelper.rptBIGBOLDFONTA)
            sTicket = sTicket.Replace("rptBIGGERBOLDFONTA", RawPrinterHelper.rptBIGGERBOLDFONTA)
            sTicket = sTicket.Replace("rptBIGGESTBOLDFONTA", RawPrinterHelper.rptBIGGESTBOLDFONTA)
            sTicket = sTicket.Replace("rptCENTER", RawPrinterHelper.rptCENTER)
            sTicket = sTicket.Replace("rptLEFT", RawPrinterHelper.rptLEFT)
            sTicket = sTicket.Replace("rptRIGHT", RawPrinterHelper.rptRIGHT)
            sTicket = sTicket.Replace("@Logo@", RawPrinterHelper.rptCENTER & RawPrinterHelper.rptPRINTLOGO)
            sTicket = sTicket.Replace("@Logo1@", RawPrinterHelper.rptCENTER & RawPrinterHelper.rptPRINTLOGO1)
            sTicket = sTicket.Replace("@Logo2@", RawPrinterHelper.rptCENTER & RawPrinterHelper.rptPRINTLOGO2)
            sTicket = sTicket.Replace("@Logo3@", RawPrinterHelper.rptCENTER & RawPrinterHelper.rptPRINTLOGO3)
            sTicket = sTicket.Replace("@Logo4@", RawPrinterHelper.rptCENTER & RawPrinterHelper.rptPRINTLOGO4)
            sTicket = sTicket.Replace("@Logo5@", RawPrinterHelper.rptCENTER & RawPrinterHelper.rptPRINTLOGO5)

            Return sTicket

        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function ProcessBT(ByVal dsTicket As DataSet, ByVal dsTables As DataSet) As String
        Dim i As Integer
        Dim dt As DataTable
        Dim sTicket As String
        Dim dc As DataColumn
        Try
            sTicket = ""



            For i = 0 To dsTicket.Tables(0).Rows.Count - 1
                If dsTicket.Tables(0).Rows(i).Item("Text").ToString = "@end@" Or i = 39 Then Exit For
                'sTable(i) = Mid(dsTicket.Tables(0).Rows(i).Item("Text"), 0, 0)
                sTicket = sTicket & dsTicket.Tables(0).Rows(i).Item("FontType").ToString & dsTicket.Tables(0).Rows(i).Item("AlignType").ToString & dsTicket.Tables(0).Rows(i).Item("Text").ToString.Trim & RawPrinterHelper.rptLF

            Next



            For Each dt In dsTables.Tables
                If dt Is Nothing Then Exit For
                For Each dc In dt.Columns
                    If dt.Rows.Count > 0 Then
                        If Not TypeOf (dt.Rows(0).Item(dc.ColumnName)) Is Byte() Then
                            If Not IsDBNull(dt.Rows(0).Item(dc.ColumnName)) AndAlso dc.DataType.FullName <> "System.Guid" Then
                                sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", dt.Rows(0).Item(dc.ColumnName).ToString)
                                sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", Format(dt.Rows(0).Item(dc.ColumnName), "0.00"))
                                sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "#BARCODE#" & "TCK" & dt.Rows(0).Item(dc.ColumnName).ToString) '& Chr(0))
                                sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "#BARCODE#" & "" & dt.Rows(0).Item(dc.ColumnName).ToString) '& Chr(0))


                            Else
                                sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
                                sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
                                sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                                sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")

                            End If
                        End If
                    Else
                        sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
                        sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
                        sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
                        sTicket = sTicket.Replace("[" & dt.TableName & "." & dc.ColumnName & "]", "")

                    End If
                Next

            Next

            'If iPrinterLanguage = 0 Then
            '    sTicket = sTicket.Replace("@Logo@", "#LOGO#")
            'Else
            sTicket = sTicket.Replace("@Logo@", "")
            'End If

            sTicket = sTicket.Replace("@Logo1@", "")
            sTicket = sTicket.Replace("@Logo2@", "")
            sTicket = sTicket.Replace("@Logo3@", "")
            sTicket = sTicket.Replace("@Logo4@", "")
            sTicket = sTicket.Replace("@Logo5@", "")

            Return sTicket

        Catch ex As Exception
            Return ""
        End Try
    End Function

    'Public Function ProcessSticker(ByVal dsTicket As DataSet, ByVal dsTables As DataSet, ByVal sType As String) As String
    '    Dim iCounter As Integer = 0
    '    Dim dt As DataTable
    '    Dim sTicket As String = ""
    '    Dim dc As DataColumn
    '    Dim sText As String = ""
    '    Dim sX As String = "0"
    '    Dim sY As String = "0"
    '    Dim sW As String = "1"
    '    Dim sH As String = "2"
    '    Dim sFont As String = "3"
    '    Dim sReverse As String = "N"
    '    Dim sRotation As String = "0"
    '    Dim sNarrow As String = "2"
    '    Dim sWide As String = "4"
    '    'Dim iChars As Integer = 16
    '    Dim iTextStart As Integer = 0
    '    Dim iTextEnd As Integer = 0
    '    'Dim sTicket As String = ""
    '    Dim sTemp As String = ""
    '    Dim iSpaces As Integer = 0

    '    Dim sOffset As String = "OFFSET " & Math.Round(iStickerOffset / 203, 2)
    '    'Dim sShift As String = "SHIFT 36"
    '    Dim sFormat As String = ""
    '    'Dim iFontTest As Integer = 1
    '    'Dim bIgnoreStickerDimensions As Boolean = True
    '    Try

    '        ' sTicket = "OEPL1" & vbLf

    '        If sType = "MAX" Then

    '            sTicket = sTicket & Chr(2) & "m" & vbLf

    '            sTicket = sTicket & Chr(2) & "KW" & Format(iStickerWidth + 50, "0000") & vbLf

    '            sTicket = sTicket & Chr(2) & "L" & vbLf



    '            sTicket = sTicket & "H07" & vbLf
    '            sTicket = sTicket & "D11" & vbLf


    '        ElseIf sType = "TSC" Then

    '            sTicket = sTicket & "SIZE " & Math.Round(iStickerWidth / 203, 2) & "," & Math.Round(iStickerLength / 203, 2) & vbLf
    '            sTicket = sTicket & "GAP " & Math.Round(iStickerGap / 203, 2) & ",0" & vbLf
    '            If sStickerOrientation = "ZT" Then
    '                sTicket = sTicket & "DIRECTION 1,0" & vbLf
    '            Else
    '                sTicket = sTicket & "DIRECTION 0,0" & vbLf
    '            End If

    '            ' sTicket = sTicket & sShift & vbLf
    '            sTicket = sTicket & sOffset & vbLf
    '            sTicket = sTicket & "SPEED 1.5" & vbLf
    '            sTicket = sTicket & "DENSITY 7" & vbLf



    '            sTicket = sTicket & "CLS" & vbLf
    '        Else
    '            sTicket = sTicket & "O" & vbLf

    '            'If bIgnorestickerdimensions = True Then
    '            sTicket = sTicket & "q" & iStickerWidth & vbLf
    '            sTicket = sTicket & "Q" & iStickerLength & "," & iStickerGap & vbLf
    '            'End If
    '            sTicket = sTicket & "N" & vbLf

    '            sTicket = sTicket & sStickerOrientation & vbLf
    '        End If

    '        For iCounter = 0 To dsTicket.Tables(0).Rows.Count - 2
    '            ' sformat = ""
    '            If sType = "MAX" Then
    '                If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("text")) Then sText = dsTicket.Tables(0).Rows(iCounter).Item("text").ToString
    '            Else
    '                If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("text")) Then sText = """" & dsTicket.Tables(0).Rows(iCounter).Item("text") & """"
    '            End If
    '            If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("x")) Then sX = dsTicket.Tables(0).Rows(iCounter).Item("x")
    '            If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("y")) Then sY = dsTicket.Tables(0).Rows(iCounter).Item("y")
    '            If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("width")) Then sW = dsTicket.Tables(0).Rows(iCounter).Item("width")
    '            If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("height")) Then sH = dsTicket.Tables(0).Rows(iCounter).Item("height")
    '            If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("fontsize")) Then sFont = dsTicket.Tables(0).Rows(iCounter).Item("fontsize")
    '            'If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("format")) Then sformat = dsTicket.Tables(0).Rows(iCounter).Item("format")

    '            'Select Case sFont
    '            '    Case "2"
    '            '        iChars = 25
    '            '    Case "3"
    '            '        iChars = 18
    '            '    Case "4"
    '            '        iChars = 16
    '            'End Select


    '            If sType = "MAX" Then

    '                If InStr(sText, "|") > 0 Then
    '                    sTicket = sTicket & "1" & "a" & "3" & "1" & "100" & Format(iStickerLength - Val(sY), "0000") & Format(Val(sX), "0000") & sText & vbLf
    '                Else
    '                    sTicket = sTicket & "1" & "9" & "1" & "1" & Format(Val(sFont), "000") & Format(iStickerLength - Val(sY), "0000") & Format(Val(sX), "0000") & sText & vbLf
    '                    'iFontTest = iFontTest + 1
    '                End If

    '            ElseIf sType = "TSC" Then

    '                If InStr(sText, "|") > 0 Then
    '                    sTicket = sTicket & "BARCODE " & sX & "," & sY & ",""39"",40,0," & sRotation & "," & sNarrow & "," & sWide & "," & sText & vbLf
    '                Else
    '                    sTicket = sTicket & "TEXT " & sX & "," & sY & ",""" & sFont & """," & sRotation & "," & sW & "," & sH & "," & sText & vbLf
    '                End If

    '            Else

    '                If InStr(sText, "|") > 0 Then
    '                    sTicket = sTicket & "B" & sX & "," & sY & "," & sRotation & ",3," & sNarrow & "," & sWide & "," & sH & ",B," & sText & vbLf
    '                Else
    '                    sTicket = sTicket & "A" & sX & "," & sY & "," & sRotation & "," & sFont & "," & sW & "," & sH & "," & sReverse & "," & sText & vbLf
    '                End If

    '            End If



    '        Next


    '        For Each dt In dsTables.Tables
    '            If dt Is Nothing Then Exit For
    '            For Each dc In dt.Columns
    '                If dt.Rows.Count > 0 Then
    '                    If Not TypeOf (dt.Rows(0).Item(dc.ColumnName)) Is Byte() Then
    '                        If Not IsDBNull(dt.Rows(0).Item(dc.ColumnName)) And InStr(dc.ColumnName.ToUpper, "GUID") = 0 AndAlso dc.DataType.FullName <> "System.Guid" Then

    '                            'If Not IsDBNull(dsTicket.Tables(0).Rows(iCounter).Item("format")) Then sformat = dsTicket.Tables(0).Rows(iCounter).Item("format")


    '                            'If sFormat <> "" Then
    '                            '    sTemp = Format(dt.Rows(0).Item(dc.ColumnName), sFormat)
    '                            'Else
    '                            sTemp = dt.Rows(0).Item(dc.ColumnName).ToString
    '                            'End If


    '                            If sTemp.Length > 18 Then
    '                                sTemp = Mid(sTemp, 1, 18)
    '                            Else
    '                                iSpaces = CInt(Val(Format(((18 - sTemp.Length) / 2), "0")))
    '                                sTemp = Space(iSpaces) & sTemp
    '                            End If

    '                            sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", sTemp)
    '                            sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", Format(dt.Rows(0).Item(dc.ColumnName), "0.00"))
    '                            sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", dt.Rows(0).Item(dc.ColumnName).ToString)
    '                            sTicket = sTicket.Replace("#" & dt.TableName & "." & dc.ColumnName & "#", Format(dt.Rows(0).Item(dc.ColumnName), "0,000"))
    '                            sTicket = sTicket.Replace("^" & dt.TableName & "." & dc.ColumnName & "^", Format(dt.Rows(0).Item(dc.ColumnName), "MM/dd/yyyy"))

    '                        Else
    '                            sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
    '                            sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
    '                            sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")

    '                        End If
    '                    End If
    '                Else
    '                    sTicket = sTicket.Replace("%" & dt.TableName & "." & dc.ColumnName & "%", "")
    '                    sTicket = sTicket.Replace("$" & dt.TableName & "." & dc.ColumnName & "$", "0.00")
    '                    sTicket = sTicket.Replace("|" & dt.TableName & "." & dc.ColumnName & "|", "")
    '                End If
    '            Next

    '        Next


    '        If sType = "MAX" Then
    '            sTicket = sTicket & "E" & vbLf
    '        ElseIf sType = "TSC" Then
    '            sTicket = sTicket & "PRINT 1,1" & vbLf
    '        Else
    '            sTicket = sTicket & "P1" & vbLf
    '        End If

    '        '   sTicket = sTicket & Chr(27) & "EPL2" & vbLf

    '        Return sTicket

    '    Catch ex As Exception
    '        Return ""
    '    End Try
    'End Function

    Public Sub PrintTestPage()
        Dim sTemp As String = ""

        sTemp = sTemp & rptREGFONTA & "Regular Font A" & rptLF
        sTemp = sTemp & rptREGBOLDFONTA & "Regular Bold Font A" & rptLF
        sTemp = sTemp & rptBIGFONTA & "Big Font A" & rptLF
        sTemp = sTemp & rptBIGBOLDFONTA & "Big Bold Font A" & rptLF
        sTemp = sTemp & rptREGFONTB & "Regular Font B" & rptLF
        sTemp = sTemp & rptREGBOLDFONTB & "Regular Bold Font B" & rptLF
        sTemp = sTemp & rptBIGFONTB & "Big Font B" & rptLF
        sTemp = sTemp & rptBIGBOLDFONTB & "Big Bold Font B" & rptLF
        sTemp = sTemp & rptLF '& rptPARTIALCUT

        'If bCutReceipts = True Then
        '    sTemp = sTemp & rptPARTIALCUT
        'Else
        sTemp = sTemp & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF & RawPrinterHelper.rptLF
        'End If

       

        PrintString("Ticket Printer", sTemp)
    End Sub
End Class
