﻿'Option Strict On
Imports Micrologic
Public Class clsCardProcessing

    Public sReturn As String = ""

    Public sCCNumber As String = ""
    Public sCCExp As String = ""
    Public sCCType As String = ""
    Public sCCTrack As String = ""
    Public sCCAmount As String = ""
    Public sCCTicket As String = ""
    Public sFullCCTicket As String = ""
    Public sCCHolder As String = ""
    Public sTransType As String = ""
    Public sCCTicketId As Long = 0
    Public sCCCustomerId As String = ""
    Public sCCCustomerName As String = ""
    Public sCCZipCode As String = ""

    Public sCCResult As String = ""
    Public sCCAuth As String = ""
    Public sCCRef As String = ""
    Public sCCTroutD As String = ""

    Private bUnlimited As Boolean = False

    'Public WithEvents UPED As New SIM.Charge


    Private iCreditCardCtr As Integer = -1
    Private OfflineCCInfo() As CreditCardProperties
    Private dsTransData As New DataSet
    Dim sMessage As String

    Dim dbAuth As String = ""
    Dim dbRef As String = ""
    Private rCounter As Integer = 0

    Private bRelayed As Boolean = False
    Public sCCReturn As String = ""
    Public sCCRelayed As String = ""

    Private dtTransactionSent As Date = CDate("1/1/1900")

    Public iAlertsInterval As Integer = 60

    Private CreditCardLogInfo As New CreditCardLogProperties

    'Private sCCUser_PW As String = ""

    Public bRecurring As Boolean = False

    Public sLabel1 As String = ""
    Public sLabel2 As String = ""
    Public sTitle As String = ""
    Public sErrors As String = ""
    Public sProcessing As String = ""

    Public Sub New(ByVal bolRelayed As Boolean, ByVal strMessage As String, ByVal intAlertsInterval As Integer, ByVal bolUnlimited As Boolean) ', Optional ByVal sPW As String = "")

        ' This call is required by the Windows Form Designer.
        'InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        dsTransData.Tables.Add("XML_REQUEST")
        dsTransData.Tables(0).Columns.Add("USER_ID")
        dsTransData.Tables(0).Columns.Add("USER_PW")
        dsTransData.Tables(0).Columns.Add("COMMAND")
        dsTransData.Tables(0).Columns.Add("PROCESSOR_ID")
        dsTransData.Tables(0).Columns.Add("MERCH_NUM")
        dsTransData.Tables(0).Columns.Add("ACCT_NUM")
        dsTransData.Tables(0).Columns.Add("EXP_DATE")
        dsTransData.Tables(0).Columns.Add("MANUAL_FLAG")
        dsTransData.Tables(0).Columns.Add("PRESENT_FLAG")
        dsTransData.Tables(0).Columns.Add("TRANS_AMOUNT")
        dsTransData.Tables(0).Columns.Add("REFERENCE")
        dsTransData.Tables(0).Columns.Add("TRACK_DATA")
        dsTransData.Tables(0).Columns.Add("CUSTOMER_CODE")
        dsTransData.Tables(0).Columns.Add("TAX_AMOUNT")
        dsTransData.Tables(0).Columns.Add("ZIP_CODE")
        dsTransData.Tables(0).Columns.Add("STREET")
        dsTransData.Tables(0).Columns.Add("TICKET_NUM")
        dsTransData.Tables(0).Columns.Add("CARDHOLDER")
        dsTransData.Tables(0).Columns.Add("TXN_TIMEOUT")

        bRelayed = bolRelayed
        sCCRelayed = strMessage
        iAlertsInterval = intAlertsInterval

        bUnlimited = bolUnlimited

    End Sub

    Public Sub Load()
        'If sCCProgramPath <> "" Then
        '    If IsProcessRunning("", "PCCW.EXE") = False Then 'Check is PC-Charge pro is running
        '        If Dir(sCCProgramPath & "\PCCW.EXE") <> "" Then
        '            Call Shell(sCCProgramPath & "\PCCW.EXE /M", AppWinStyle.MinimizedNoFocus)
        '        End If
        '    End If
        'End If

        Dim sExpireMonth As String = ""
        Dim sExpireYear As String = ""

        If bRelayed = True Then

            'SendCCMessage()


        ElseIf bUnlimited = True Then
            If sTransType = "" Then sTransType = "SALE"

            If iCreditMode >= 4 Then
                If sCCExp.Length > 3 Then
                    sExpireMonth = Mid(sCCExp, 1, 2)
                    sExpireYear = Mid(sCCExp, sCCExp.Length - 1, 2)
                End If
                ProcessPayGateway(sTransType, sCCAmount, sCCNumber, sExpireMonth, sExpireYear, 0, AccountInfo.AccountToken, False, sCCTicket, sCCCustomerId, sCCTrack, sCCExp)
            ElseIf iCreditMode = 3 Then
                'SendPayWareConnect(sCCNumber, sCCExp, "", sTransType, sCCAmount, sCCTicket, sCCHolder)
            Else
                'SendCreditCard(sCCNumber, sCCExp, "", sTransType, sCCAmount, sCCTicket, sCCHolder)
            End If

            'ElseIf GetOfflineCreditCards() <> 0 Then
            '    'Do Until iCreditCardCtr = UBound(OfflineCCInfo)
            '    If ProcessCreditCard() = False Then
            '        If bAutoCloseBatch = True Then
            '            CloseBatch()
            '        Else

            '            dtTransactionSent = CDate("1/1/1900")
            '            Me.Close()
            '            Exit Sub
            '        End If

            '    End If
        Else
            If bAutoCloseBatch = True Then
                CloseBatch()
            Else

                dtTransactionSent = CDate("1/1/1900")
                Close()
                Exit Sub
            End If
        End If
    End Sub

    Private Function ProcessPayGateway(ByVal sChargeType As String, ByVal sChargeTotal As String, ByVal sCardNumber As String, ByVal sExpireMonth As String, ByVal sExpireYear As String, ByVal lTicketId As Long, ByVal sAccountToken As String, ByVal bVoid As Boolean, ByVal sTicketNumber As String, ByVal sCustomerId As String, ByVal sTrack2Data As String, ByVal sCCExpDate As String) As Boolean


        Dim sAuthCode As String = ""
        Dim sResult As String = ""
        Dim sReferenceNumber As String = ""
        Dim lLogId As Long = 0
        Dim sTicket As String = ""
        Dim sResponseText As String = ""
        Dim sAuthAmount As String = ""
        Dim sTransAmount As String = ""
        Dim sTroutd As String = ""

        'Dim sAuthCode As String = ""
        'Dim sResult As String = ""
        'Dim sReferenceNumber As String = ""
        'Dim lLogId As Long = 0
        'Dim sTicket As String = ""
        'Dim sResponseText As String = ""
        'Dim sAuthAmount As String = ""
        'Dim sTransAmount As String = ""
        'Dim sTroutd As String = ""
        ''Dim sRec As String = ""


        'Dim TRClient As New TransactionClient()

        'Dim CCResponse As CreditCardResponse
        'Dim rResponse As RecurringResponse

        'Dim I As Integer
        Dim sResultsText As String = ""

        'Dim tsTime As TimeSpan

        'Dim sOrderID As String = ""
        'Dim sCCDisplayNumber As String = ""

        'Dim sLogResult As String = ""
        Dim sBatchAmount As String = ""

        'Dim dsResponse As DataSet

        ''If bRecurringRequest = True Then
        'Dim rRequest As New RecurringRequest()
        ''Else
        'Dim CCRequest As New CreditCardRequest()
        ''End If

        'Dim iMpdResponseCode As Integer = 0
        'Dim sMpdResponseCodeText As String = ""
        'Dim sPayerIdentifier As String = ""
        'Dim dtManageUntil As Date = CDate("1/1/1900")
        'Dim sSpan As String = ""
        'Dim sSpanMonth As String = ""
        'Dim sSpanYear As String = ""
        'Dim lMPDId As Long = 0
        'Dim cMPDInfo As New CustomerManagedPayerData
        Dim bCreateCustomerTransaction As Boolean = False
        Try



            SendPayGateway(sTransType, sCCAmount, sCCNumber, sExpireMonth, sExpireYear, lTicketId, AccountInfo.AccountToken, False, sCCTicket, sResult, sAuthCode, sTroutd, sResponseText, sReferenceNumber, lLogId, sCCCustomerId, "", "", sTrack2Data, sCCZipCode, "", "", sCCType, 0, "", sResultsText, sCCHolder, sCCExpDate, bCreateCustomerTransaction, False, False, "", sFullCCTicket, bRecurring)


            'If sCardNumber.Length > 4 Then
            '    sCCDisplayNumber = Mid(sCardNumber, 1, 4) & "...." & Mid(sCardNumber, sCardNumber.Length - 3, 4)
            'Else
            '    sCCDisplayNumber = sCardNumber
            'End If



            'CreditCardLogInfo.Clear()
            'CreditCardLogInfo.lTicketId = lTicketId
            'CreditCardLogInfo.sTicketNumber = sTicketNumber
            'CreditCardLogInfo.sCCNumber = sCCDisplayNumber
            'CreditCardLogInfo.Amount = Val(sChargeTotal)
            'CreditCardLogInfo.sTransactionType = sTransType
            'CreditCardLogInfo.lCreditCardLogId = CreditCard.CreateLog(CreditCardLogInfo.lTicketId, CreditCardLogInfo.sTicketNumber, CreditCardLogInfo.sCCNumber, CreditCardLogInfo.Amount, UserInfo.UserName, CreditCardLogInfo.sTransactionType)
            'sOrderID = CreditCardLogInfo.lCreditCardLogId.ToString

            'sAccountToken = "FDC4E33FCE6C54083389EDF18D50AF64C3C0259544B6C3FFEEA64B268DDD5FEB9E52BE59D7BDC8EC72"

            'If sChargeType = "REFUND" Then sChargeType = "CREDIT"

            'Dim bRecurring As Boolean = False
            'If bRecurring = True Then

            '    rRequest.setCommand(RecurringRequest.COMMAND_ADD_CUSTOMER_ACCOUNT_AND_RECURRENCE)
            '    rRequest.setAccountType(RecurringRequest.ACCOUNT_TYPE_CREDIT_CARD)
            '    rRequest.setCustomerId("12345")
            '    rRequest.setCustomerName("Bill Smith")
            '    rRequest.setEmailAddress("customer@example.com")
            '    rRequest.setCreditCardNumber("4242424242424242")
            '    rRequest.setExpireMonth("04")
            '    rRequest.setExpireYear("2019")
            '    rRequest.setBillingAddress("123 Fake St.")
            '    rRequest.setZipOrPostalCode("96730")
            '    rRequest.setCountryCode("US")
            '    rRequest.setRecurrenceId("54321")
            '    rRequest.setDescription("A monthly subscription")
            '    rRequest.setChargeTotal("1.00")
            '    rRequest.setNotifyCustomer(True)
            '    rRequest.setStartDay(1)
            '    rRequest.setStartMonth(10)
            '    rRequest.setStartYear(2004)
            '    rRequest.setEndDay(1)
            '    rRequest.setEndMonth(10)
            '    rRequest.setEndYear(2005)
            '    rRequest.setNumberOfRetries(3)
            '    rRequest.setPeriod(RecurringRequest.PERIOD_MONTHLY)


            '    'rRequest.setCommand(RecurringRequest.COMMAND_ADD_CUSTOMER_ACCOUNT_ONLY)
            '    'rRequest.setAccountType(RecurringRequest.ACCOUNT_TYPE_CREDIT_CARD)
            '    'rRequest.setCustomerId(sCustomerId)
            '    'rRequest.setCustomerName(sCCCustomerName)
            '    'rRequest.setEmailAddress("support@micrologic.net")
            '    'rRequest.setCreditCardNumber(sCardNumber)
            '    'rRequest.setExpireMonth(sExpireMonth)
            '    'rRequest.setExpireYear(sExpireYear)
            '    'rRequest.setBillingAddress("111 Canfield Ave.")
            '    'rRequest.setZipOrPostalCode("07869")
            '    'rRequest.setCountryCode("US")
            '    'rRequest.setRecurrenceId(sOrderID)
            '    'rRequest.setDescription("A monthly subscription")
            '    'rRequest.setChargeTotal(sChargeTotal)
            '    ''rRequest.setNotifyCustomer(False)
            '    ''rRequest.setStartDay(1)
            '    ''rRequest.setStartMonth(10)
            '    ''rRequest.setStartYear(2004)
            '    ''rRequest.setEndDay(1)
            '    ''rRequest.setEndMonth(10)
            '    ''rRequest.setEndYear(2005)
            '    ''rRequest.setNumberOfRetries(3)
            '    ''rRequest.setPeriod(RecurringRequest.PERIOD_MONTHLY)

            '    dtSent = Now
            '    rResponse = DirectCast(TRClient.doTransaction(rRequest, sAccountToken, iCCTimeOut * 1000), RecurringResponse)
            '    dtReceived = Now

            '    sResultsText = sResultsText + "Code:  " + Convert.ToString(rResponse.getResponseCode())
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Code Text:  " + rResponse.getResponseCodeText()
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Secondary Code:  " + Convert.ToString(rResponse.getSecondaryResponseCode())
            '    sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "Approval Code:  " + rResponse.getBankApprovalCode()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "Trans ID:  " + rResponse.getBankTransactionId()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "Batch ID:  " + rResponse.getBatchId()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "Reference ID:  " + rResponse.getReferenceId()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "ISO Code:  " + CCResponse.getIsoCode()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "AVS Code:  " + CCResponse.getAvsCode()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "CVV Response:  " + CCResponse.getCreditCardVerificationResponse()
            '    'sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Date / Time:  " + rResponse.getTimeStamp().ToString

            '    sResultsText = sResultsText + vbNewLine



            '    Select Case rResponse.getResponseCode
            '        Case 1
            '            sResult = "CAPTURED"
            '        Case 100
            '            sResult = "DECLINED"
            '        Case Else
            '            sResult = "ERROR"
            '    End Select

            '    Select Case rResponse.getSecondaryResponseCode
            '        'Case 1
            '        '    sResult = "CAPTURED"
            '        'Case 100
            '        '    sResult = "DECLINED"
            '        'Case Else
            '        '    sResult = "ERROR"
            '    End Select


            '    'sResult = CCResponse.getResponseCodeText
            '    'sAuthCode = sResult
            '    'sAuthCode = rResponse.getBankApprovalCode
            '    'sReferenceNumber = rResponse.getReferenceId
            '    'sTroutd = rResponse.getBankTransactionId
            '    'sTransAmount = rResponse.getRequestedAmount
            '    'sAuthAmount = rResponse.getCapturedAmount
            '    sTicket = "" 'CCResponse.getOrderId
            '    sResponseText = rResponse.getResponseCodeText

            '    'sCustomerToken= rresponse.

            'Else

            '    If Now.Year > CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2) & sExpireYear) And CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2)) > 84 And CInt(sExpireYear) < 16 Then
            '        sExpireYear = (CInt(Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2)) + 1).ToString & sExpireYear
            '    Else
            '        sExpireYear = Mid(Now.Year.ToString, 1, Now.Year.ToString.Length - 2) & sExpireYear
            '    End If

            '    If sCCCustomerId <> "" Then

            '        CCRequest.setManagePayerData(True)
            '        cMPDInfo = Customer.GetManagedPayerData(CLng(sCCCustomerId))


            '        If cMPDInfo.lMPDId > 0 And cMPDInfo.sPayerIdentifier <> "" And cMPDInfo.sExpireMonth <> "" And cMPDInfo.sExpireYear <> "" And cMPDInfo.sSPAN <> "" Then

            '            CCRequest.setPayerIdentifier(cMPDInfo.sPayerIdentifier)
            '            CCRequest.setSecurePrimaryAccountNumber(cMPDInfo.sSPAN)
            '            CCRequest.setExpireMonth(sExpireMonth)
            '            CCRequest.setExpireYear(sExpireYear)
            '        Else
            '            CCRequest.setCreditCardNumber(sCardNumber)
            '            CCRequest.setExpireMonth(sExpireMonth)
            '            CCRequest.setExpireYear(sExpireYear)
            '        End If

            '    Else

            '        CCRequest.setCreditCardNumber(sCardNumber)
            '        CCRequest.setExpireMonth(sExpireMonth)
            '        CCRequest.setExpireYear(sExpireYear)

            '    End If

            '    CCRequest.setChargeType(sChargeType)
            '    CCRequest.setChargeTotal(sChargeTotal)

            '    CCRequest.setOrderId(sOrderID)

            '    dtSent = Now
            '    CCResponse = DirectCast(TRClient.doTransaction(CCRequest, sAccountToken, iCCTimeOut * 1000), CreditCardResponse)
            '    dtReceived = Now

            '    sResultsText = sResultsText + "Code:  " + Convert.ToString(CCResponse.getResponseCode())
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Code Text:  " + CCResponse.getResponseCodeText()
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Secondary Code:  " + Convert.ToString(CCResponse.getSecondaryResponseCode())
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Approval Code:  " + CCResponse.getBankApprovalCode()
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Trans ID:  " + CCResponse.getBankTransactionId()
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Batch ID:  " + CCResponse.getBatchId()
            '    sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Reference ID:  " + CCResponse.getReferenceId()
            '    sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "ISO Code:  " + CCResponse.getIsoCode()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "AVS Code:  " + CCResponse.getAvsCode()
            '    'sResultsText = sResultsText + vbNewLine
            '    'sResultsText = sResultsText + "CVV Response:  " + CCResponse.getCreditCardVerificationResponse()
            '    'sResultsText = sResultsText + vbNewLine
            '    sResultsText = sResultsText + "Date / Time:  " + CCResponse.getTimeStamp().ToString

            '    sResultsText = sResultsText + vbNewLine


            '    If sCCCustomerId <> "" Then
            '        iMpdResponseCode = CCResponse.getMpdResponseCode
            '        sMpdResponseCodeText = CCResponse.getMpdResponseCodeText
            '        sPayerIdentifier = CCResponse.getPayerIdentifier()
            '        dtManageUntil = CCResponse.getManageUntil()
            '        sSpan = CCResponse.getSpan()
            '        sSpanMonth = CCResponse.getExpireMonth
            '        sSpanYear = CCResponse.getExpireYear


            '        If iMpdResponseCode = 1 And CLng(sCCCustomerId) > 0 Then
            '            lMPDId = Customer.SaveManagedPayerData(CLng(sCCCustomerId), sPayerIdentifier, dtManageUntil, sSpan, sSpanMonth, sSpanYear)

            '            If lMPDId <= 0 Then
            '                lMPDId = Customer.SaveManagedPayerData(CLng(sCCCustomerId), sPayerIdentifier, dtManageUntil, sSpan, sSpanMonth, sSpanYear)
            '            End If
            '        End If

            '    End If

            '    Select Case CCResponse.getResponseCode
            '        Case 1
            '            sResult = "CAPTURED"
            '        Case 100
            '            sResult = "DECLINED"
            '        Case Else
            '            sResult = "ERROR"
            '    End Select

            '    Select Case CCResponse.getSecondaryResponseCode
            '        'Case 1
            '        '    sResult = "CAPTURED"
            '        'Case 100
            '        '    sResult = "DECLINED"
            '        'Case Else
            '        '    sResult = "ERROR"
            '    End Select


            '    'sResult = CCResponse.getResponseCodeText
            '    'sAuthCode = sResult
            '    sAuthCode = CCResponse.getBankApprovalCode
            '    sReferenceNumber = CCResponse.getReferenceId
            '    sTroutd = CCResponse.getBankTransactionId
            '    sTransAmount = CCResponse.getRequestedAmount
            '    sAuthAmount = CCResponse.getCapturedAmount
            '    sTicket = "" 'CCResponse.getOrderId
            '    sResponseText = CCResponse.getResponseCodeText

            'End If


            'tsTime = (dtReceived - dtSent)



            'If Val(sTransAmount) > 0 And Val(sAuthAmount) > 0 Then
            '    If Val(sTransAmount) > Val(sAuthAmount) Then
            '        sResult = "NOT CAPTURED"
            '        sAuthCode = "INSUFFICIENT FUNDS " & sAuthAmount
            '        sReferenceNumber = ""
            '        'sTroutd = ""
            '    End If

            'End If

            'If CreditCardLogInfo.sTicketNumber <> "" And (sTicket <> "" And InStr(sTicket, ".") <= 0) And bCCSendToQueue = True Then
            '    If CreditCardLogInfo.sTicketNumber <> sTicket Then

            '        sLogResult = CreditCard.GetLogResult(CreditCardLogInfo.lCreditCardLogId, CreditCardLogInfo.sTicketNumber)

            '        If sLogResult <> "" Then

            '            dsResponse = GetDatasetfromString(sLogResult)

            '            If ColumnExists(dsResponse.Tables(0), "RESULT") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("RESULT")) Then sResult = dsResponse.Tables(0).Rows(0).Item("RESULT").ToString
            '            End If

            '            If ColumnExists(dsResponse.Tables(0), "AUTH_CODE") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("AUTH_CODE")) Then sAuthCode = dsResponse.Tables(0).Rows(0).Item("AUTH_CODE").ToString
            '            Else
            '                sAuthCode = sResult
            '            End If

            '            If ColumnExists(dsResponse.Tables(0), "REFERENCE") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("REFERENCE")) Then sReferenceNumber = dsResponse.Tables(0).Rows(0).Item("REFERENCE").ToString
            '            ElseIf ColumnExists(dsResponse.Tables(0), "Ref") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("Ref")) Then sReferenceNumber = dsResponse.Tables(0).Rows(0).Item("Ref").ToString
            '            End If

            '            If ColumnExists(dsResponse.Tables(0), "TROUTD") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("TROUTD")) Then sTroutd = dsResponse.Tables(0).Rows(0).Item("TROUTD").ToString
            '            End If

            '            If ColumnExists(dsResponse.Tables(0), "INTRN_SEQ_NUM") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("INTRN_SEQ_NUM")) Then sBatchAmount = dsResponse.Tables(0).Rows(0).Item("INTRN_SEQ_NUM").ToString
            '            End If

            '            If ColumnExists(dsResponse.Tables(0), "TICKET") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("TICKET")) Then sTicket = dsResponse.Tables(0).Rows(0).Item("TICKET").ToString
            '            End If

            '            If ColumnExists(dsResponse.Tables(0), "RESPONSE_TEXT") Then
            '                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("RESPONSE_TEXT")) Then sResponseText = dsResponse.Tables(0).Rows(0).Item("RESPONSE_TEXT").ToString
            '            End If

            '        Else
            '            sResult = "NOT CAPTURED"
            '            sAuthCode = "TIMEOUT"
            '            sReferenceNumber = ""
            '            sTroutd = ""
            '        End If


            '    End If
            'End If

            'CreditCardLogInfo.sResult = sResult
            'CreditCardLogInfo.sResultXML = sResultsText
            'CreditCardLogInfo.iResponseTime = CInt(tsTime.TotalSeconds)
            'CreditCardLogInfo.sTroutD = sTroutd
            'CreditCardLogInfo.sAuthCode = sAuthCode

            'lLogId = CreditCard.UpdateLogTransCompleted(CreditCardLogInfo, UserInfo.UserName)


            'If bOffline = False Then

            'lLogid = CreditCard.SaveLog(0, sCCTicket, sResult, sRec, tsTime.Seconds, sTroutd)
            If bUnlimited = True Or bRelayed = True Then

                'If bSaveOfflineRelayCC = True Then

                '    If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "NOT_CAPTURED" Then
                '        If sAuthCode.ToUpper = "TIMEOUT" Or sAuthCode.ToUpper = "IP INIT" Or sAuthCode.ToUpper = "IP CONNECTION ERROR" Then 'Timeout 
                '            sRec = CreateTimeOutCapturedString(sTroutD, sRec)
                '        End If
                '    End If
                'End If
                If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT_CAPTURED" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "DECLINED" Or sResult = "" Then
                    If InStr(sResponseText.ToUpper, "COMMUNICATION ERROR") > 0 Or InStr(sAuthCode.ToUpper, "SEND") > 0 Or InStr(sAuthCode.ToUpper, "RESPONSE") > 0 Or InStr(sAuthCode.ToUpper, "CONNECT") > 0 Or InStr(sAuthCode.ToUpper, "TIMEOUT") > 0 Or sAuthCode.ToUpper = "TIMEOUT" Or sAuthCode.ToUpper = "IP INIT" Or sAuthCode.ToUpper = "IP CONNECTION ERROR" Then 'Timeout 
                        sReturn = "TIMEOUT"
                        sCCResult = sResult & " - " & sAuthCode
                    ElseIf InStr(sAuthCode.ToUpper, "DECLINE") > 0 Or InStr(sResult.ToUpper, "DECLINE") > 0 Or InStr(sAuthCode.ToUpper, "INVALID") > 0 Or InStr(sAuthCode.ToUpper, "CALL") > 0 Or InStr(sAuthCode.ToUpper, "CANCELED") > 0 Or InStr(sAuthCode.ToUpper, "HOLD") > 0 Or InStr(sAuthCode.ToUpper, "LOST") > 0 Or InStr(sAuthCode.ToUpper, "STOLEN") > 0 Or InStr(sAuthCode.ToUpper, "DECLINE") > 0 Or InStr(sAuthCode.ToUpper, "ERROR") > 0 Or InStr(sAuthCode.ToUpper, "NO ACTION TAKEN") > 0 Then
                        sReturn = "DECLINE"
                        'sReturn = "ERROR"
                        sCCResult = sResult & " - " & sAuthCode
                        'ElseIf InStr(sResponseText.ToUpper, "DUPLICATE") > 0 Then
                        '    ccCharge_Finish(sResult, sAuthCode, "", False, "", True)
                    Else
                        sReturn = "ERROR"
                        sCCResult = sResult & " - " & sAuthCode
                    End If
                Else
                    sReturn = sResult
                    sCCReturn = sResultsText
                End If



                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Function

            Else
                Select Case sTransType
                    Case "GCBALANCE"
                        'dsResponse.WriteXml("c:\GC" & rCounter & ".xml")
                        rCounter = rCounter + 1
                        If rCounter = 10 Then rCounter = 0


                        If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT_CAPTURED" Then
                            'Me.lblTitle.Text = sAuthCode
                            'ccCharge_Error(sResult, sAuthCode)
                        Else
                            'Me.lblTitle.Text = sAuthCode
                            'ccCharge_Finish(sResult, sAuthCode, sReferenceNumber)
                            ''Call PrintCreditCardReceipt(CLng(lblReferenceID.Tag), CDbl(lblChargeAmount.Text), (lblReferenceID.Text), lblAuthNumber.Text)

                            'Me.Hide()
                        End If
                    Case Else
                        'dsResponse.WriteXml("c:\MONTHCC" & rCounter & ".xml")
                        rCounter = rCounter + 1
                        If rCounter = 10 Then rCounter = 0


                        If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT_CAPTURED" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "DECLINED" Or sResult = "" Then
                            sTitle = sAuthCode
                            ccCharge_Error(sResult, sAuthCode) ', sResponseText)
                        Else
                            sTitle = sAuthCode
                            ccCharge_Finish(sResult, sAuthCode, sReferenceNumber) ', False, sTroutd, False)
                            'Call PrintCreditCardReceipt(CLng(lblReferenceID.Tag), CDbl(lblChargeAmount.Text), (lblReferenceID.Text), lblAuthNumber.Text)

                            'Me.Hide()
                        End If
                End Select
            End If

            'Else

            ''If iCreditCardCtr + 1 >= OfflineCCInfo.Length Or iCreditCardCtr = -1 Then
            ''    lLogid = CreditCard.SaveLog(0, "", sResult, sRec, tsTime.Seconds, sTroutd)
            ''Else
            ''    If Not OfflineCCInfo(iCreditCardCtr) Is Nothing Then
            ''        lLogid = CreditCard.SaveLog(OfflineCCInfo(iCreditCardCtr).ReferenceId, OfflineCCInfo(iCreditCardCtr).ReferenceNumber, sResult, sRec, tsTime.Seconds, sTroutd)
            ''    End If
            ''End If

            ''dsResponse.WriteXml("c:\OFFCC" & rCounter & ".xml")
            'rCounter = rCounter + 1
            'If rCounter = 10 Then rCounter = 0

            'If (sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT_CAPTURED" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "DECLINED") And bBatch = False Then 'Or sResult.ToUpper = "NOT CLOSED" Then
            '    Me.lblTitle.Text = sAuthCode
            '    ccOffline_Error(sResult, sAuthCode, sResponseText)
            'Else
            '    Me.lblTitle.Text = sAuthCode
            '    ccOffline_Finish(sResult, sAuthCode, sReferenceNumber, sTroutd, sBatchAmount)
            '    'Call PrintCreditCardReceipt(CLng(lblReferenceID.Tag), CDbl(lblChargeAmount.Text), (lblReferenceID.Text), lblAuthNumber.Text)

            '    'Me.Hide()
            'End If

            'End If 



        Catch ex As Exception
            WriteToLog(sLogFileName, "ProcessPayGateWayOffline: ", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")

            'If bSilent = False Then frmMsgBox(ex.Message, 0, 0, False, True, 30)

            'iTimer = 60
            'tmrCancel.Enabled = False
            'Me.Close()
        End Try


    End Function

    Private Sub ccCharge_Error(ByVal sResult As String, ByVal sAuthCode As String)
        Dim bolReturn As Boolean = False
        Try


            sErrors = "ERROR " & sAuthCode
            'lblErrors.Refresh()
            sProcessing = "ERROR PROCESSING"
            'lblProcessing.Refresh()

            If bClosingBatch = True Then

                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Sub
            End If

            'MsgBox("frmOfflineCreditCards.ccCharge_Error " & sAuthCode)
            'Me.Close()
            bolReturn = Me.ProcessCreditCard()

            If bolReturn = False Then

                If bAutoCloseBatch = False Or bClosingBatch = True Then

                    dtTransactionSent = CDate("1/1/1900")
                    Me.Close()
                    Exit Sub
                End If

                sLabel1 = "Closing Batch"

                dsTransData.Tables(0).Rows.Clear()
                dsTransData.Tables(0).Rows.Add()
                dsTransData.Tables(0).Rows(0).Item("USER_ID") = "User1"
                If sCCUser_PW <> "" Then
                    dsTransData.Tables(0).Rows(0).Item("USER_PW") = sCCUser_PW
                End If
                dsTransData.Tables(0).Rows(0).Item("COMMAND") = 31
                'ccBatch.Path = gsCCProgramPath

                dsTransData.Tables(0).Rows(0).Item("MERCH_NUM") = Trim(sMerchantNumber)
                dsTransData.Tables(0).Rows(0).Item("PROCESSOR_ID") = Trim(sProcessorCode)
                dsTransData.Tables(0).Rows(0).Item("TXN_TIMEOUT") = "60"
                'ccBatch.Send()

                bClosingBatch = True
                SendCCMessage()
                bClosingBatch = False

            End If

            Exit Sub
        Catch ex As Exception
            WriteToLog(sLogFileName, "ccCharge_Error: ", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")

            'frmMsgBox(Err.Number & " ccCharge_Error: " & Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10))
            dtTransactionSent = CDate("1/1/1900")
            Me.Close()
        End Try

    End Sub

    Private Sub ccCharge_Finish(ByVal sResult As String, ByVal sAuthCode As String, ByVal sRefNumber As String)
        'Dim sSQL As String
        Dim bolReturn As Boolean = False
        Dim sCCNum As String = ""
        Dim sTemp As String = ""
        Try



            'check to see if its captured
            'If bSettle = True Then
            If sResult.ToUpper.Trim = "ACCEPTED" Then

                'frmMsgBox("Credit Card Batch Closed.", 1)
                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Sub
            ElseIf sResult.ToUpper.Trim = "PROCESSED" And sAuthCode.ToUpper = "BATCH RELEASED" Then

                'frmMsgBox("Credit Card Batch Released.", 1)
                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Sub

            ElseIf sResult.ToUpper.Trim = "NOT CAPTURED" And sAuthCode.ToUpper = "SETTLEMENT IN PROGRESS" Then

                'frmMsgBox("Settlement In Progress.", 1)
                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Sub


            ElseIf sResult.ToUpper.Trim = "SETTLE ERROR" Then

                'frmMsgBox("Credit Card Batch Error.", 1)
                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Sub
            ElseIf sResult.ToUpper.Trim = "NO TRANSACTIONS" Then

                'frmMsgBox("No Transactions.", 1)
                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Sub

            End If
            ' Else
            If sResult = "CAPTURED" And (OfflineCCInfo(iCreditCardCtr).TransactionType = "SALE") Or (OfflineCCInfo(iCreditCardCtr).TransactionType = "REFUND" And UCase(sResult) = "PROCESSED") Then

                If OfflineCCInfo(iCreditCardCtr).CCNumber.Length > 20 Then
                    sTemp = LogicDecrypt(OfflineCCInfo(iCreditCardCtr).CCNumber)
                Else
                    sTemp = OfflineCCInfo(iCreditCardCtr).CCNumber
                End If

                sCCNum = Mid(sTemp, 1, 4) & "........." & Mid(sTemp, sTemp.Length - 3, 4)



                CreditCard.UpdateTransaction(sAuthCode, sRefNumber, Now, OfflineCCInfo(iCreditCardCtr).CCId, sCCNum, "", 0)
                Call SavePayment("CREDIT CARD", 1, "TECHNICAL", OfflineCCInfo(iCreditCardCtr).ReferenceId, OfflineCCInfo(iCreditCardCtr).InvNumber, CDbl(OfflineCCInfo(iCreditCardCtr).TotalAmount))

                bolReturn = ProcessCreditCard()
            Else
                'get result
                sErrors = sResult & " -- " & sAuthCode
                'Get reason
                bolReturn = ProcessCreditCard()
            End If
            'End If
            If bolReturn = False Then

                If bAutoCloseBatch = False Or bClosingBatch = True Then

                    dtTransactionSent = CDate("1/1/1900")
                    Me.Close()
                    Exit Sub
                End If

                sLabel1 = "Closing Batch"

                dsTransData.Tables(0).Rows.Clear()
                dsTransData.Tables(0).Rows.Add()
                dsTransData.Tables(0).Rows(0).Item("USER_ID") = "User1"
                If sCCUser_PW <> "" Then
                    dsTransData.Tables(0).Rows(0).Item("USER_PW") = sCCUser_PW
                End If
                dsTransData.Tables(0).Rows(0).Item("COMMAND") = 31
                'ccBatch.Path = gsCCProgramPath

                dsTransData.Tables(0).Rows(0).Item("MERCH_NUM") = Trim(sMerchantNumber)
                dsTransData.Tables(0).Rows(0).Item("PROCESSOR_ID") = Trim(sProcessorCode)
                dsTransData.Tables(0).Rows(0).Item("TXN_TIMEOUT") = "60"
                'ccBatch.Send()

                bClosingBatch = True
                SendCCMessage()
                bClosingBatch = False

                'Me.Close()
            End If

            Exit Sub

        Catch ex As Exception
            WriteToLog(sLogFileName, "ccCharge_Finish: ", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")
            Exit Sub
        End Try


    End Sub

    Private Function ProcessCreditCard() As Boolean


        'Dim sConn As String = ""
        'Dim dcConn As New OleDb.OleDbConnection()
        'Dim DA As New OleDb.OleDbDataAdapter
        'Dim sSql As String = ""
        'Dim dcCom As New OleDb.OleDbCommand
        'Dim drReader As OleDb.OleDbDataReader

        Dim sTemp As String = ""

        Try

            dsTransData.Tables(0).Rows.Clear()
            dsTransData.Tables(0).Rows.Add()

            iCreditCardCtr = iCreditCardCtr + 1

            If UBound(OfflineCCInfo) > iCreditCardCtr Then



                'If sCCProgramPath <> "" Then
                '    dcConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=" & sCCProgramPath & "\pccw.mdb"

                '    dcConn.Open()

                '    If OfflineCCInfo(iCreditCardCtr).TransactionType = "SALE" Then
                '        sSql = "Select * from Trans where Ticket = '" & OfflineCCInfo(iCreditCardCtr).ReferenceId & "' and Result = 'CAPTURED' and Date = #" & Now.Date & "# and Action = 1"
                '    ElseIf OfflineCCInfo(iCreditCardCtr).TransactionType = "REFUND" Then
                '        sSql = "Select * from Trans where Ticket = '" & OfflineCCInfo(iCreditCardCtr).ReferenceId & "' and Result = 'CAPTURED' and Date = #" & Now.Date & "# and Action = 2"
                '    Else
                '        sSql = "Select * from Trans where Ticket = '" & OfflineCCInfo(iCreditCardCtr).ReferenceId & "' and Result = 'CAPTURED' and Date = #" & Now.Date & "#"
                '    End If

                '    dcCom.CommandText = sSql
                '    dcCom.Connection = dcConn
                '    drReader = dcCom.ExecuteReader()

                If IsProcessedInDB(OfflineCCInfo(iCreditCardCtr).TransactionType, OfflineCCInfo(iCreditCardCtr).ReferenceNumber, OfflineCCInfo(iCreditCardCtr).DateCreated) = True Then
                    sTitle = "Ticket has already been processed."

                    '        drReader.Close()
                    '        dcConn.Close()

                    'If CreditCard.IsTicketComplete(OfflineCCInfo(iCreditCardCtr).ReferenceNumber) = False Then
                    ccCharge_Finish("CAPTURED", dbAuth, dbRef)

                    ' Me.Hide()
                    'End If

                    Exit Function
                    '    End If

                    '    drReader.Close()
                    '    dcConn.Close()

                End If

                sProcessing = "PROCESSING " & iCreditCardCtr + 1 & " OF " & UBound(OfflineCCInfo)
                'lblProcessing.Refresh()
                dsTransData.Tables(0).Rows(0).Item("USER_ID") = "User1"

                If sCCUser_PW <> "" Then
                    dsTransData.Tables(0).Rows(0).Item("USER_PW") = sCCUser_PW
                End If

                'Set the Action based on menu selection
                If OfflineCCInfo(iCreditCardCtr).TransactionType = "SALE" Then
                    dsTransData.Tables(0).Rows(0).Item("COMMAND") = 1
                    'ccCharge.Action = 1 'Sale
                ElseIf OfflineCCInfo(iCreditCardCtr).TransactionType = "REFUND" Then
                    dsTransData.Tables(0).Rows(0).Item("COMMAND") = 2
                    dsTransData.Tables(0).Rows(0).Item("REFERENCE") = OfflineCCInfo(iCreditCardCtr).ReferenceNumber
                    'ccCharge.Action = 2 'Return/Refund
                    'ccCharge.Reference = gOfflineCreditCards(iCreditCardCtr).TicketNumber
                End If

                'If giCCDemoMode = 1 Then
                '    'ccCharge.Demo = True
                'Else
                '    'ccCharge.Demo = False
                'End If
                'ccCharge.Path = Trim(sCCProgramPath)
                dsTransData.Tables(0).Rows(0).Item("PROCESSOR_ID") = Trim(sProcessorCode)
                dsTransData.Tables(0).Rows(0).Item("MERCH_NUM") = Trim(sMerchantNumber)
                dsTransData.Tables(0).Rows(0).Item("TXN_TIMEOUT") = "60"

                If OfflineCCInfo(iCreditCardCtr).CCNumber.Length > 20 Then
                    sTemp = LogicDecrypt(OfflineCCInfo(iCreditCardCtr).CCNumber)
                Else
                    sTemp = OfflineCCInfo(iCreditCardCtr).CCNumber
                End If

                dsTransData.Tables(0).Rows(0).Item("ACCT_NUM") = sTemp

                If OfflineCCInfo(iCreditCardCtr).Track2Data <> "" Then
                    sTemp = LogicDecrypt(OfflineCCInfo(iCreditCardCtr).Track2Data)
                    If sTemp <> "" Then
                        dsTransData.Tables(0).Rows(0).Item("TRACK_DATA") = sTemp
                        dsTransData.Tables(0).Rows(0).Item("MANUAL_FLAG") = 1
                        dsTransData.Tables(0).Rows(0).Item("PRESENT_FLAG") = CStr(1)
                    Else

                    End If
                End If


                'If Not ccCharge.VerifyCreditCard(OfflineCCInfo(iCreditCardCtr - 1).CCNumber) Then
                '    Beep()
                '    lblTitle.Text = "INVALID CREDIT CARD. PLEASE TRY AGAIN!"
                '    Me.Enabled = True
                '    Exit Sub
                'End If

                dsTransData.Tables(0).Rows(0).Item("EXP_DATE") = OfflineCCInfo(iCreditCardCtr).CCExpDate
                'If Not ccCharge.VerifyExpDate Then
                '    Beep()
                '    lblTitle.Text = "INVALID EXP DATE. PLEASE TRY AGAIN!"
                '    Me.Enabled = True
                '    Exit Sub
                'End If

                'it is important to format the amount with a period.
                dsTransData.Tables(0).Rows(0).Item("TRANS_AMOUNT") = Format(OfflineCCInfo(iCreditCardCtr).TotalAmount, "0.00")
                'check amount
                'If Not ccCharge.VerifyAmount Then
                '    Beep()
                '    lblTitle.Text = "INVALID AMOUNT. PLEASE TRY AGAIN!"
                '    Me.Enabled = True
                '    Exit Sub
                'End If

                'Set Ticket Number
                dsTransData.Tables(0).Rows(0).Item("TICKET_NUM") = OfflineCCInfo(iCreditCardCtr).ReferenceNumber

                'ccCharge.Send()
                SendCCMessage()
            Else
                ' iCreditCardCtr = 0
                Return False
                'Me.Close()
            End If
            Return True

        Catch ex As Exception
            WriteToLog(sLogFileName, "Offline_ProcessCC:", Err.Number, Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10), "ERROR")

            'frmMsgBox(Err.Number & " m_ProcessCreditCard: " & Err.Description & Mid(ex.StackTrace, ex.StackTrace.Length - 9, 10))
            Return False
        End Try

    End Function

    Private Function IsProcessedInDB(ByVal sTransType As String, ByVal sTicketNumber As String, ByVal CreatedDate As Date) As Boolean

        Dim bReturn As Boolean = False

        Dim sConn As String = ""
        Dim dcConn As New OleDb.OleDbConnection()
        Dim DA As New OleDb.OleDbDataAdapter
        Dim sSql As String = ""
        Dim dcCom As New OleDb.OleDbCommand
        Dim drReader As OleDb.OleDbDataReader
        Dim dbPath As String = ""

        Try

            If sCCProgramPath = "" And sCCDBPath = "" Then
                Return False
            ElseIf sCCProgramPath <> "" Then
                dbPath = sCCProgramPath
            Else
                dbPath = sCCDBPath
            End If
            dcConn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=Admin;Data Source=" & dbPath & "\pccw.mdb"

            dcConn.Open()

            If sTransType = "SALE" Then
                sSql = "Select * from Trans where Ticket = '" & sTicketNumber & Space(10 - sTicketNumber.Length) & "' and Result = 'CAPTURED' and Action = 1 and Date between #" & CreatedDate.Date & "# and #" & Now.Date & "#"
            ElseIf sTransType = "REFUND" Then
                sSql = "Select * from Trans where Ticket = '" & sTicketNumber & Space(10 - sTicketNumber.Length) & "' and Result = 'CAPTURED' and Action = 2 and Date between #" & CreatedDate.Date & "# and #" & Now.Date & "#"
            Else
                sSql = "Select * from Trans where Ticket = '" & sTicketNumber & Space(10 - sTicketNumber.Length) & "' and Result = 'CAPTURED' and Date between #" & CreatedDate.Date & "# and #" & Now.Date & "#"
            End If

            dcCom.CommandText = sSql
            dcCom.Connection = dcConn
            drReader = dcCom.ExecuteReader()

            If drReader.Read = True Then
                dbAuth = drReader.Item("Auth").ToString
                dbRef = drReader.Item("Result_Ref").ToString
                bReturn = True
            Else
                dbAuth = ""
                dbRef = ""
                bReturn = False
            End If

            drReader.Close()
            dcConn.Close()

            Return bReturn

        Catch ex As Exception
            Return False
        End Try
    End Function

    Private bClosingBatch As Boolean = False
    Private Sub CloseBatch()

        'If bAutoCloseBatch = False Then
        '    Me.Close()
        'End If
        Try


            sLabel1 = "Closing Batch"



            dsTransData.Tables(0).Rows.Clear()
            dsTransData.Tables(0).Rows.Add()
            dsTransData.Tables(0).Rows(0).Item("USER_ID") = "User1"

            If sCCUser_PW <> "" Then
                dsTransData.Tables(0).Rows(0).Item("USER_PW") = sCCUser_PW
            End If


            dsTransData.Tables(0).Rows(0).Item("COMMAND") = 31
            'ccBatch.Path = gsCCProgramPath

            dsTransData.Tables(0).Rows(0).Item("MERCH_NUM") = Trim(sMerchantNumber)
            dsTransData.Tables(0).Rows(0).Item("PROCESSOR_ID") = Trim(sProcessorCode)
            dsTransData.Tables(0).Rows(0).Item("TXN_TIMEOUT") = "60"
            'ccBatch.Send()

            bClosingBatch = True
            SendCCMessage()
            bClosingBatch = False

            dtTransactionSent = CDate("1/1/1900")
            Close()
        Catch ex As Exception
            bClosingBatch = False

        End Try
    End Sub

    Private Function SendCCMessage() As Boolean
        Dim xmlStream As New System.IO.MemoryStream
        Dim bBytes() As Byte
        Dim enc As New System.Text.ASCIIEncoding()

        Dim sTicketNumber As String = ""
        Dim sCCNumber As String = ""
        Dim sAmount As String = ""

        Dim tcpHelper As New TcpHelper

        Dim sRemoteIPAddress As String = ""

        Dim sReturn As String = ""
        Dim dsResponse As New DataSet
        Dim byteReturn(0) As Byte

        Dim sTransType As String = ""
        Dim sCreditCardNumber As String = ""
        Dim sExpirationDate As String = ""
        Dim sHolderName As String = ""
        Dim sChargeAmount As String = ""
        Dim sTrackData As String = ""
        Dim sTimeOut As String = ""


        Dim sAuthCode As String = ""
        Dim sResult As String = ""
        Dim sReferenceNumber As String = ""
        Dim lLogId As Long = 0
        'Dim sTicket As String = ""
        Dim sResponseText As String = ""
        'Dim sAuthAmount As String = ""
        'Dim sTransAmount As String = ""
        Dim sTroutd As String = ""
        Dim sResultsText As String = ""

        Try



            If sCCRelayed <> "" And bRelayed = True Then

                'WriteToLog(sLogFileName, "SendCCMessage", 0, sCCRelayed, "EVENT")

                sMessage = sCCRelayed

                If bDisableForceFlag = False Then
                    sMessage = sMessage.Replace("</XML_REQUEST>", "<FORCE_FLAG>" & "FALSE" & "</FORCE_FLAG>" & "</XML_REQUEST>")
                End If



                If InStr(sMessage.ToUpper, "<USER_PW />") > 0 And sCCUser_PW <> "" Then
                    sMessage = sMessage.Replace("<USER_PW />", "<USER_PW>" & sCCUser_PW & "</USER_PW>")
                ElseIf InStr(sMessage.ToUpper, "USER_PW") <= 0 And sCCUser_PW <> "" Then
                    sMessage = sMessage.Replace("</XML_REQUEST>", "<USER_PW>" & sCCUser_PW & "</USER_PW>" & "</XML_REQUEST>")
                End If



            Else
                dsTransData.Tables(0).WriteXml(xmlStream)

                bBytes = xmlStream.ToArray
                sMessage = enc.GetString(bBytes)

                sMessage = sMessage.Replace("<NewDataSet>", "")
                sMessage = sMessage.Replace("</NewDataSet>", "")

            End If

            dtTransactionSent = Now



            'Call TCPConnect()

            'UPGRADE_NOTE: State was upgraded to CtlState. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
            'While Me.Winsock1.CtlState = MSWinsockLib.StateConstants.sckConnecting
            '    System.Windows.Forms.Application.DoEvents()
            'End While


            'UPGRADE_NOTE: State was upgraded to CtlState. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="A9E4979A-37FA-4718-9994-97DD76ED70A7"'
            'If Me.Winsock1.CtlState = MSWinsockLib.StateConstants.sckConnected Then

            CreditCardLogInfo.Clear()
            'CreditCardLogInfo.lTicketId = lTicketId

            sTicketNumber = GetTicketFromMessage(sMessage)
            sCCNumber = getCCDisplayFromMessage(sMessage)
            sAmount = GetAmountFromMessage(sMessage)




            CreditCardLogInfo.sTicketNumber = sTicketNumber
            CreditCardLogInfo.sCCNumber = sCCNumber
            CreditCardLogInfo.Amount = Val(sAmount)
            If CreditCardLogInfo.sCCNumber <> "" Then CreditCardLogInfo.lCreditCardLogId = CreditCard.UpdateLogTransSent(CreditCardLogInfo.sTicketNumber, "LWREPORTS", CreditCardLogInfo.sCCNumber, CreditCardLogInfo.Amount)


            sTransType = GetTagFromMessage(sMessage, "<COMMAND>", "</COMMAND>")
            If iCreditMode = 1 And (sTransType = "1" Or sTransType = "2") Then

                sCreditCardNumber = GetTagFromMessage(sMessage, "<ACCT_NUM>", "</ACCT_NUM>")
                If sCreditCardNumber.Length > 0 Then
                    If IsNumeric(Mid(sCreditCardNumber, 1, 1)) = False Then
                        sCreditCardNumber = Mid(sCreditCardNumber, 2)
                    End If
                End If
                sExpirationDate = GetTagFromMessage(sMessage, "<EXP_DATE>", "</EXP_DATE>")
                sHolderName = GetTagFromMessage(sMessage, "<CARDHOLDER>", "</CARDHOLDER>")
                sChargeAmount = GetTagFromMessage(sMessage, "<TRANS_AMOUNT>", "</TRANS_AMOUNT>")
                sTrackData = GetTagFromMessage(sMessage, "<TRACK_DATA>", "</TRACK_DATA>")
                sTimeOut = GetTagFromMessage(sMessage, "<TXN_TIMEOUT>", "</TXN_TIMEOUT>")


                'Me.Enabled = False
                RunTransaction(sTransType, sCreditCardNumber, sExpirationDate, sHolderName, sChargeAmount, sTicketNumber, sTrackData, sTimeOut)
                'Me.Enabled = True
                Exit Function
            ElseIf iCreditMode >= 4 And (sTransType = "1" Or sTransType = "2") Then

                If sTransType = "1" Then sTransType = "SALE"
                If sTransType = "2" Then sTransType = "CREDIT"


                sCreditCardNumber = GetTagFromMessage(sMessage, "<ACCT_NUM>", "</ACCT_NUM>")
                If sCreditCardNumber.Length > 0 Then
                    If IsNumeric(Mid(sCreditCardNumber, 1, 1)) = False Then
                        sCreditCardNumber = Mid(sCreditCardNumber, 2)
                    End If
                End If
                sExpirationDate = GetTagFromMessage(sMessage, "<EXP_DATE>", "</EXP_DATE>")
                sHolderName = GetTagFromMessage(sMessage, "<CARDHOLDER>", "</CARDHOLDER>")
                sChargeAmount = GetTagFromMessage(sMessage, "<TRANS_AMOUNT>", "</TRANS_AMOUNT>")
                sTrackData = GetTagFromMessage(sMessage, "<TRACK_DATA>", "</TRACK_DATA>")
                sTimeOut = GetTagFromMessage(sMessage, "<TXN_TIMEOUT>", "</TXN_TIMEOUT>")


                ' Me.Enabled = False
                SendPayGateway(sTransType, sChargeAmount, sCreditCardNumber, Mid(sExpirationDate, 1, 2), Mid(sExpirationDate, sExpirationDate.Length - 1, 2), 0, AccountInfo.AccountToken, False, sTicketNumber, sResult, sAuthCode, sTroutd, sResponseText, sReferenceNumber, lLogId, "", "", "", "", "", "", "", "", 0, "", sResultsText, sHolderName, sExpirationDate, False, False, False, sTrackData, "", False)


                If bRelayed = True Then

                    'If bSaveOfflineRelayCC = True Then

                    '    If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "NOT_CAPTURED" Then
                    '        If sAuthCode.ToUpper = "TIMEOUT" Or sAuthCode.ToUpper = "IP INIT" Or sAuthCode.ToUpper = "IP CONNECTION ERROR" Then 'Timeout 
                    '            sRec = CreateTimeOutCapturedString(sTroutD, sRec)
                    '        End If
                    '    End If
                    'End If

                    sCCReturn = sResultsText

                    dtTransactionSent = CDate("1/1/1900")
                    Me.Close()
                    Exit Function
                ElseIf sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "SALE NOT FOUND" Then
                    sTitle = sResult
                    Me.ccCharge_Error(sResult, sAuthCode) ', sTroutD, lLogId, sResponseText)
                Else
                    'If sTransType = "REFUND" Or sTransType = "VOID" Then
                    '    sAuthCode = sTransType
                    '    ccCharge_Finish(sResult, sTransType, sReferenceNumber, False, sTroutD, lLogId, False)
                    '    '
                    'Else
                    ccCharge_Finish(sResult, sAuthCode, "") ', False, sTroutD, lLogId, False) ', dsreturn.Tables(0).Rows(0).Item("AUTH_CODE").ToString)
                    'End If

                End If

                'SendPayGateway(sTransType, sCreditCardNumber, sExpirationDate, sHolderName, sChargeAmount, sTicketNumber, sTrackData, sTimeOut)
                ' Me.Enabled = True
                Exit Function

            End If



            If sCCProgramAddress <> "" Then
                'UPGRADE_WARNING: Couldn't resolve default property of object RemoteIPAddress. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                sRemoteIPAddress = sCCProgramAddress
            Else
                sRemoteIPAddress = "127.0.0.1"
            End If
            'UPGRADE_WARNING: Couldn't resolve default property of object RemotePort. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'

            'WriteToLog(sLogFileName, "SendCCMessage", 0, sMessage, "EVENT")

            tcpHelper.SendMessage(sRemoteIPAddress, 31419, sMessage, True, sReturn, byteReturn, 60, "")

            'MsgBox(sReturn)

            dsResponse = GetDatasetFromByteArray(byteReturn)
            'Dim XMLreader As System.IO.StringReader = New System.IO.StringReader(sReturn.Trim(Chr(0)))
            'Dim myStream As New System.IO.MemoryStream(byteReturn)
            'dsResponse.ReadXml(myStream)

            If IsNothing(dsResponse) = True Then
                dsResponse = New DataSet
            End If

            If dsResponse.Tables.Count > 0 Then
                ProcessResponse(dsResponse, sReturn)
            Else
                If bRelayed = True Then

                    WriteToLog(sLogFileName, "Offline_SendCCMessage: ", Err.Number, sReturn, "ERROR")

                    If sReturn = "" Then
                        WriteToLog(sLogFileName, "SendCCMessage", 0, "No Return - Timeout", "EVENT")
                        sReturn = GetTimeoutString(sTicketNumber)
                    End If

                    sCCReturn = sReturn

                    dtTransactionSent = CDate("1/1/1900")
                    Me.Close()
                    Exit Function
                Else
                    WriteToLog(sLogFileName, "Offline_SendCCMessage: ", Err.Number, Err.Description, "ERROR")

                    Return False
                End If

            End If


            'SendData(sMessage)
            'UPGRADE_WARNING: TextRTF was upgraded to Text and has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="9B7D5ADD-D8FE-4819-A36C-6DEDAF088CC7"'
            'rtbSent.Text = TCPXMLMessage
            'End If

        Catch ex As Exception

            If bRelayed = True Then

                WriteToLog(sLogFileName, "Offline_SendCCMessage: ", Err.Number, sReturn, "ERROR")

                If sReturn = "" Then
                    sReturn = GetTimeoutString(sTicketNumber)
                End If

                sCCReturn = sReturn

                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
            Else
                WriteToLog(sLogFileName, "Offline_SendCCMessage: ", Err.Number, Err.Description, "ERROR")

                Return False
            End If
        End Try
    End Function

    Private Function GetTimeoutString(ByVal sTicket As String) As String
        Dim sTimeOut As String = "<XML_REQUEST>  <USER_ID>User1</USER_ID>   <RESULT>NOT CAPTURED</RESULT>  <AUTH_CODE>Timeout</AUTH_CODE> </XML_REQUEST>"
        Dim sReturn As String = ""
        Dim pccwTrans As New PCCWProperties
        Try

            GetPCCWDB()

            pccwTrans = PCCW.GetTicket(sTicket)

            If pccwTrans.sTroutD <> "" Then
                sReturn = GetPCCWString(pccwTrans)
            End If

            If sReturn = "" Then
                sReturn = sTimeOut
            End If

            Return sReturn

        Catch ex As Exception
            Return sReturn
        End Try
    End Function

    Private Function GetPCCWString(ByVal pccwTrans As PCCWProperties) As String

        Dim dsTrans As New DataSet
        Dim sReturn As String = ""

        Try
            dsTrans.Tables.Add("XML_REQUEST")
            dsTrans.Tables(0).Columns.Add("USER_ID")
            dsTrans.Tables(0).Columns.Add("TROUTD")
            dsTrans.Tables(0).Columns.Add("RESULT")
            dsTrans.Tables(0).Columns.Add("AUTH_CODE")
            dsTrans.Tables(0).Columns.Add("TICKET")
            dsTrans.Tables(0).Columns.Add("INTRN_SEQ_NUM")

            dsTrans.Tables(0).Rows.Add("User1", pccwTrans.sTroutD, pccwTrans.sResult, pccwTrans.sAuth, pccwTrans.sTicket, pccwTrans.sTroutD)

            sReturn = dsTrans.GetXml


            Return sReturn

        Catch ex As Exception
            Return sReturn
        End Try
    End Function


    Private Function ProcessResponse(ByVal dsResponse As DataSet, ByVal sRec As String) As Boolean

        Dim sResult As String = ""
        Dim sAuthCode As String = ""
        Dim sReferenceNumber As String = ""
        Dim sTroutD As String = ""
        Dim sBatchAmount As String = ""

        Dim dtReceived As Date = Now


        Try

            dsResponse.WriteXml("c:\OFFCC" & rCounter & ".xml")
            rCounter = rCounter + 1
            If rCounter = 10 Then rCounter = 0

            If ColumnExists(dsResponse.Tables(0), "RESULT") Then
                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("RESULT")) Then sResult = dsResponse.Tables(0).Rows(0).Item("RESULT").ToString
            End If

            If ColumnExists(dsResponse.Tables(0), "AUTH_CODE") Then
                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("AUTH_CODE")) Then sAuthCode = dsResponse.Tables(0).Rows(0).Item("AUTH_CODE").ToString
            End If

            If ColumnExists(dsResponse.Tables(0), "REFERENCE") Then
                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("REFERENCE")) Then sReferenceNumber = dsResponse.Tables(0).Rows(0).Item("REFERENCE").ToString
            ElseIf ColumnExists(dsResponse.Tables(0), "Ref") Then
                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("Ref")) Then sReferenceNumber = dsResponse.Tables(0).Rows(0).Item("Ref").ToString
            End If

            If ColumnExists(dsResponse.Tables(0), "TROUTD") Then
                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("TROUTD")) Then sTroutD = dsResponse.Tables(0).Rows(0).Item("TROUTD").ToString
            End If

            If ColumnExists(dsResponse.Tables(0), "INTRN_SEQ_NUM") Then
                If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("INTRN_SEQ_NUM")) Then sBatchAmount = dsResponse.Tables(0).Rows(0).Item("INTRN_SEQ_NUM").ToString
            End If

            CreditCardLogInfo.sResult = sResult
            CreditCardLogInfo.sResultXML = sRec
            CreditCardLogInfo.iResponseTime = CInt((dtReceived - dtTransactionSent).TotalSeconds)
            CreditCardLogInfo.sTroutD = sTroutD
            CreditCardLogInfo.sAuthCode = sAuthCode

            If CreditCardLogInfo.sTicketNumber <> "" Then CreditCard.UpdateLogTransReceived(CreditCardLogInfo.lCreditCardLogId, CreditCardLogInfo.sTicketNumber, "LWREPORTS", CreditCardLogInfo.sResult, CreditCardLogInfo.sAuthCode, sRec, CreditCardLogInfo.sTroutD, CInt((dtReceived - dtTransactionSent).TotalSeconds))
            ' CreditCard.SaveLog(0, "", Mid(sResult & " - " & sAuthCode, 1, 50), sRec, CInt((dtReceived - dtTransactionSent).TotalSeconds), sTroutD)


            If bRelayed = True Then

                'If bSaveOfflineRelayCC = True Then

                '    If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "NOT_CAPTURED" Then
                '        If sAuthCode.ToUpper = "TIMEOUT" Or sAuthCode.ToUpper = "IP INIT" Or sAuthCode.ToUpper = "IP CONNECTION ERROR" Then 'Timeout 
                '            sRec = CreateTimeOutCapturedString(sTroutD, sRec)
                '        End If
                '    End If
                'End If

                sCCReturn = sRec

                dtTransactionSent = CDate("1/1/1900")
                Me.Close()
                Exit Function

            ElseIf sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT_CAPTURED" Or sResult.ToUpper = "NOT CAPTURED" Then
                sTitle = sResult
                ccCharge_Error(sResult, sAuthCode)
            Else
                ccCharge_Finish(sResult, sAuthCode, sReferenceNumber)
                'Call PrintCreditCardReceipt(CLng(lblReferenceID.Tag), CDbl(lblChargeAmount.Text), (lblReferenceID.Text), lblAuthNumber.Text)

                'Me.Hide()
            End If


        Catch ex As Exception

        End Try
    End Function

    Private Sub RunTransaction(ByVal sTransType As String, ByVal sCreditCardNumber As String, ByVal sExpirationDate As String, ByVal sHoldersName As String, ByVal sChargeAmount As String, ByVal sTicketNumber As String, ByVal sTrackData As String, ByVal sCCTimeOut As String)

        Dim sResult As String = ""
        Dim sAuthCode As String = ""
        Dim sReference As String = ""
        Dim sResultCode As String = ""
        Dim sTroutD As String = ""
        Dim sResponseText As String = ""
        Dim sXMLRequest As String = ""
        Dim sXMLResponse As String = ""

        Dim lLogId As Integer = 0

        Dim dtReceived As Date = Now
        Try




            '    With UPED

            '        Select Case sTransType
            '            'Case 1
            '            '    .Action = SIM.Charge.Command.Debit_Sale
            '            Case "1"
            '                .Action = SIM.Charge.Command.Credit_Sale
            '            Case "2"
            '                .Action = SIM.Charge.Command.Credit_Return
            '        End Select

            '        '.Action = SIM.Charge.Command.Debit_Sale

            '        Dim card As String = sCreditCardNumber
            '        If card.IndexOf("*") >= 0 Then
            '            '.Card = txtCardUS.Text
            '            '.CardPresent = "2"
            '        Else
            '            .Card = card
            '        End If

            '        'If Not txtCashBackUS.Text = String.Empty Or Not txtCashBackUS.Text = "0.00" Then .CashBack = txtCashBackUS.Text


            '        .ExpDate = sExpirationDate
            '        .Member = sHoldersName
            '        .Ticket = sTicketNumber
            '        .TroutD = ""
            '        .Track = sTrackData

            '        .AuthCode = ""
            '        .DisplayString = ""

            '        'processor releated properties
            '        'If optPCCharge.Checked Then
            '        '.PaymentEngine = SIM.Charge.PaymentSoftware.PCCharge

            '        '.Processor = sProcessorCode
            '        '.MerchantNumber = sMerchantNumber

            '        .UserID = sCCUser
            '        .Amount = sChargeAmount
            '        'ElseIf optRiTA.Checked Then
            '        .ClientID = sClientId
            '        .PaymentEngine = SIM.Charge.PaymentSoftware.RiTA_PAYware
            '        '    .Amount = txtTotalUS.Text
            '        '    .UserID = txtUserID.Text
            '        .UserPW = sCCUser_PW
            '        'ElseIf optIPCharge.Checked Then
            '        '    If optMerchKey.Checked Then
            '        '        .MerchantKey = txtMerchantKey.Text
            '        '        .UserID = txtUserID.Text
            '        '        .UserPW = txtUserPW.Text
            '        '    Else
            '        '        .DeviceKey = txtDeviceKey.Text
            '        '    End If
            '        '    .ClientID = txtClientID.Text
            '        '    .PaymentEngine = SIM.Charge.PaymentSoftware.IPCharge
            '        '    .Amount = txtTotalUS.Text
            '        'End If


            '        'comm properties
            '        .Timeout = CLng(sCCTimeOut)
            '        'If optIP.Checked Then
            '        .IPAddress = sCCProgramAddress
            '        .Port = iCCProgramPort.ToString
            '        .SSL = True
            '        .SSLCertificate = ""
            '        .CommMethod = SIM.Charge.CommType.IP
            '        'ElseIf optHTTP.Checked Then
            '        '    .URL = txtURL.Text
            '        '    .CommMethod = SIM.Charge.CommType.URL
            '        '    'use dial backup
            '        '    'phone number
            '        '    'modem
            '        'ElseIf optDial.Checked Then
            '        '    'phone number
            '        '    'modem
            '        'End If

            '        .Device = SIM.Charge.DeviceType.ppdVerifone_1000

            '        .Path = "C:\Program Files\PCCW"

            '        'If opt300.Checked Then .Baud = "300"
            '        'If opt600.Checked Then .Baud = "600"
            '        'If opt1200.Checked Then .Baud = "1200"
            '        'If opt2400.Checked Then .Baud = "2400"
            '        'If opt4800.Checked Then .Baud = "4800"
            '        'If opt9600.Checked Then .Baud = "9600"
            '        'If opt19200.Checked Then .Baud = "19200"

            '        'If opt7.Checked Then .DataBits = "7"
            '        'If opt8.Checked Then .DataBits = "8"

            '        'If optEven.Checked Then .Parity = "E"
            '        'If optOdd.Checked Then .Parity = "O"
            '        'If optNone.Checked Then .Parity = "N"

            '        .Baud = "1200"
            '        .DataBits = "7"
            '        .Parity = "E"
            '        .ComPort = "COM" & 0 'iPinPadPort

            '        'If cboPort.Text <> String.Empty Then .ComPort = cboPort.Text

            '        .MxIPAddress = ""
            '        .MxPort = ""
            '        .MxSSL = False

            '        'If optMXIP.Checked Then
            '        '    .MxCommSetting = SIM.Charge.MxCommType.IP
            '        'Else
            '        .MxCommSetting = SIM.Charge.MxCommType.Serial
            '        'End If

            '        .SwipeTimeout = 120
            '        .LoggingLevel = 2
            '        .MaxLogSize = 3
            '        .PromptForSigCap = False
            '        .PromptForSurchargeConfirmation = False
            '        .PromptForTip = False

            '        .PromptForCashback = False
            '        .PromptForAmountConfirmation = False

            '        .PromptForPaymentType = False
            '        .ReverseFailedSignatures = False
            '        '.EnableCredit = chkCredit.Checked
            '        .EnableDebit = False
            '        .EnableEBT = False
            '        .EnableGift = False
            '        .CashBackLimit = "3"
            '        .EnableManualCardEntry = False
            '        .RefTroutD = ""

            '        .UseTIMPassthrough = False
            '        .Surcharge = ""

            '        'If chkUseUSB.Checked Then
            '        '    .USBDeviceIndex = cboUSBDevices.SelectedIndex
            '        '    .UseUSBCardReader = True
            '        'End If


            '        'If optEnglish.Checked Then .Language = SIM.Charge.LanguageCode.English
            '        ' If optFrench.Checked Then .Language = SIM.Charge.LanguageCode.French
            '        .Language = SIM.Charge.LanguageCode.English

            '        'If chkBinEnable.Checked Then
            '        '    .EnableBinManager = True
            '        '    .BinManagerIPAddress = txtBINIP.Text
            '        '    .BinManagerPort = txtBINPort.Text
            '        '    .BinManagerSSL = chkBinSSL.Checked
            '        '    .BinManagerSSLCertificate = txtBINSSLCert.Text
            '        'Else
            '        .EnableBinManager = False
            '        'End If


            '        If .Process Then
            '            'lnkMoreResp.Enabled = True
            '            sResult = .GetResult
            '            sAuthCode = .GetAuthCode
            '            sReference = .GetReference
            '            sResultCode = .GetResultCode
            '            sTroutD = .GetTroutD
            '            sResponseText = .GetResponseText
            '            sXMLRequest = .GetXMLRequest
            '            sXMLResponse = .GetXMLResponse

            '            .ClearProps()

            '            CreditCardLogInfo.sResult = sResult
            '            CreditCardLogInfo.sResultXML = sXMLResponse
            '            CreditCardLogInfo.iResponseTime = CInt((dtReceived - dtTransactionSent).TotalSeconds)
            '            CreditCardLogInfo.sTroutD = sTroutD
            '            CreditCardLogInfo.sAuthCode = sAuthCode

            '            If CreditCardLogInfo.sTicketNumber <> "" Then CreditCard.UpdateLogTransReceived(CreditCardLogInfo.lCreditCardLogId, CreditCardLogInfo.sTicketNumber, "LWREPORTS", CreditCardLogInfo.sResult, CreditCardLogInfo.sAuthCode, sXMLResponse, CreditCardLogInfo.sTroutD, CInt((dtReceived - dtTransactionSent).TotalSeconds))
            '            ' CreditCard.SaveLog(0, "", Mid(sResult & " - " & sAuthCode, 1, 50), sRec, CInt((dtReceived - dtTransactionSent).TotalSeconds), sTroutD)


            '            If bRelayed = True Then

            '                'If bSaveOfflineRelayCC = True Then

            '                '    If sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "NOT_CAPTURED" Then
            '                '        If sAuthCode.ToUpper = "TIMEOUT" Or sAuthCode.ToUpper = "IP INIT" Or sAuthCode.ToUpper = "IP CONNECTION ERROR" Then 'Timeout 
            '                '            sRec = CreateTimeOutCapturedString(sTroutD, sRec)
            '                '        End If
            '                '    End If
            '                'End If

            '                sCCReturn = sXMLResponse

            '                dtTransactionSent = CDate("1/1/1900")
            '                Me.Close()
            '                Exit Sub
            '            ElseIf sResult.ToUpper = "ERROR" Or sResult.ToUpper = "NOT CAPTURED" Or sResult.ToUpper = "SALE NOT FOUND" Then
            '                sTitle = sResult
            '                Me.ccCharge_Error(sResult, sAuthCode) ', sTroutD, lLogId, sResponseText)
            '            Else
            '                'If sTransType = "REFUND" Or sTransType = "VOID" Then
            '                '    sAuthCode = sTransType
            '                '    ccCharge_Finish(sResult, sTransType, sReferenceNumber, False, sTroutD, lLogId, False)
            '                '    '
            '                'Else
            '                ccCharge_Finish(sResult, sAuthCode, sReference) ', False, sTroutD, lLogId, False) ', dsreturn.Tables(0).Rows(0).Item("AUTH_CODE").ToString)
            '                'End If

            '            End If
            '            'If .Device = SIM.Charge.DeviceType.ppdVerifone_3730 Or .Device = SIM.Charge.DeviceType.ppdVerifone_Vx810_Duet Or .Device = SIM.Charge.DeviceType.ppdVerifone_Vx810_Duet_Interac Then
            '            '    If .GetResult.ToUpper = "CAPTURED" Then PrintReceipt()
            '            'End If

            '            'If .Device = SIM.Charge.DeviceType.ppdVerifone_Mx870 Or .Device = SIM.Charge.DeviceType.ppdVerifone_Mx830 Or .Device = SIM.Charge.DeviceType.ppdVerifone_Mx850 Or .Device = SIM.Charge.DeviceType.ppdVerifone_Mx860 Then
            '            '    If Not String.IsNullOrEmpty(.GetSignatureData) Then
            '            '        ShowSignatureData(.GetSignatureData)
            '            '        If optIPCharge.Checked = True Then
            '            '            If MessageBox.Show("Store the signature to PAYware Connect?", "SIM", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '            '                Dim sigData As String = .GetSignatureData
            '            '                Dim troutd As String = .GetTroutD
            '            '                .ClearProps()
            '            '                .Action = SIM.Charge.Command.Credit_Store_Signature
            '            '                .SignatureData = sigData
            '            '                .TroutD = troutd
            '            '                If .Process = True Then
            '            '                    If .GetResult.ToUpper = "CAPTURED" Then
            '            '                        MsgBox("Signature stored.  TroutD = " & troutd)
            '            '                    Else
            '            '                        MsgBox("Signature storage failed. " & .GetResponseText)
            '            '                    End If
            '            '                Else
            '            '                    MsgBox("Signature storage failed. " & .ErrorDescription)
            '            '                End If
            '            '            End If
            '            '        End If
            '            '    End If
            '            'End If

            '        Else
            '            'If Not b_cancelled Then
            '            'frmMsgBox("" & .ErrorCode & ": " & .ErrorDescription, )
            '            WriteToLog(sLogFileName, "RunTransaction:", .ErrorCode, .ErrorDescription, "ERROR")
            '            If bRelayed = True Then


            '                sCCReturn = .GetXMLResponse

            '                dtTransactionSent = CDate("1/1/1900")
            '                Me.Close()
            '                Exit Sub
            '            Else
            '                Me.ccCharge_Error("ERROR", "ERROR") ', sTroutD, lLogId, sResponseText)
            '            End If
            '            ' End If
            '        End If

            '        '
            '        'TrackData = String.Empty
            '        'cmdProcessUS.Enabled = True
            '        'cmdCancelUS.Enabled = False

            '        'System.Threading.Thread.Sleep(5000)
            '        System.Threading.Thread.Sleep(100)

            '        '.RefreshDisplay(txtChangeForm.Text)

            '        ' TransactionThread.Abort()
            '    End With

        Catch ex As Exception
            Exit Sub
        End Try

    End Sub

    Private Function GetTicketFromMessage(ByVal sMessage As String) As String

        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0

        Dim sTicket As String = ""
        Try

            iStart = InStr(sMessage, "<TICKET_NUM>")
            If iStart > 0 Then
                iStart = iStart + 12

                iEnd = InStr(sMessage, "</TICKET_NUM>")

                'If iEnd > 0 Then
                '    iEnd = iEnd
                'End If

                If iStart > 0 And iEnd > 0 Then

                    sTicket = Mid(sMessage, iStart, iEnd - iStart)

                End If
            Else

                iStart = InStr(sMessage, "<TICKET>")
                If iStart > 0 Then
                    iStart = iStart + 8

                    iEnd = InStr(sMessage, "</TICKET>")

                    'If iEnd > 0 Then
                    '    iEnd = iEnd
                    'End If

                    If iStart > 0 And iEnd > 0 Then

                        sTicket = Mid(sMessage, iStart, iEnd - iStart)

                    End If

                End If
            End If

            Return sTicket
        Catch ex As Exception
            Return sTicket
        End Try

    End Function

    Private Function getCCDisplayFromMessage(ByVal sMessage As String) As String

        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0

        Dim sTicket As String = ""
        Dim sCCDisplay As String = ""
        Try

            iStart = InStr(sMessage, "<ACCT_NUM>")
            If iStart > 0 Then
                iStart = iStart + 10

                iEnd = InStr(sMessage, "</ACCT_NUM>")

                'If iEnd > 0 Then
                '    iEnd = iEnd
                'End If

                If iStart > 0 And iEnd > 0 Then

                    sTicket = Mid(sMessage, iStart, iEnd - iStart)

                End If
                'Else

                '    iStart = InStr(sMessage, "<TICKET>")
                '    If iStart > 0 Then
                '        iStart = iStart + 8

                '        iEnd = InStr(sMessage, "</TICKET>")

                '        'If iEnd > 0 Then
                '        '    iEnd = iEnd
                '        'End If

                '        If iStart > 0 And iEnd > 0 Then

                '            sTicket = Mid(sMessage, iStart, iEnd - iStart)

                '        End If

                '    End If
            End If



            If sTicket.Length > 4 Then
                If IsNumeric(sTicket) = False Then
                    sCCDisplay = Mid(sTicket, 2, 4) & "...." & Mid(sTicket, sTicket.Length - 3)
                Else
                    sCCDisplay = Mid(sTicket, 1, 4) & "...." & Mid(sTicket, sTicket.Length - 3)
                End If
            End If

            Return sCCDisplay
        Catch ex As Exception
            Return sCCDisplay
        End Try

    End Function

    Private Function GetAmountFromMessage(ByVal sMessage As String) As String

        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0

        Dim sTicket As String = ""
        Try

            iStart = InStr(sMessage, "<TRANS_AMOUNT>")
            If iStart > 0 Then
                iStart = iStart + 14

                iEnd = InStr(sMessage, "</TRANS_AMOUNT>")

                'If iEnd > 0 Then
                '    iEnd = iEnd
                'End If

                If iStart > 0 And iEnd > 0 Then

                    sTicket = Mid(sMessage, iStart, iEnd - iStart)

                End If
                'Else

                '    iStart = InStr(sMessage, "<TICKET>")
                '    If iStart > 0 Then
                '        iStart = iStart + 8

                '        iEnd = InStr(sMessage, "</TICKET>")

                '        'If iEnd > 0 Then
                '        '    iEnd = iEnd
                '        'End If

                '        If iStart > 0 And iEnd > 0 Then

                '            sTicket = Mid(sMessage, iStart, iEnd - iStart)

                '        End If

                '    End If
            End If

            Return sTicket
        Catch ex As Exception
            Return sTicket
        End Try

    End Function

    Private Function GetTagFromMessage(ByVal sMessage As String, ByVal sOpeningTag As String, ByVal sClosingTag As String) As String

        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0

        Dim sTag As String = ""
        Try

            iStart = InStr(sMessage, sOpeningTag)
            If iStart > 0 Then
                iStart = iStart + sOpeningTag.Length

                iEnd = InStr(sMessage, sClosingTag)

                'If iEnd > 0 Then
                '    iEnd = iEnd
                'End If

                If iStart > 0 And iEnd > 0 Then

                    sTag = Mid(sMessage, iStart, iEnd - iStart)

                End If
                'Else

                '    iStart = InStr(sMessage, "<TICKET>")
                '    If iStart > 0 Then
                '        iStart = iStart + 8

                '        iEnd = InStr(sMessage, "</TICKET>")

                '        'If iEnd > 0 Then
                '        '    iEnd = iEnd
                '        'End If

                '        If iStart > 0 And iEnd > 0 Then

                '            sTicket = Mid(sMessage, iStart, iEnd - iStart)

                '        End If

                '    End If
            End If

            Return sTag
        Catch ex As Exception
            Return sTag
        End Try

    End Function

    Private Sub Close()
        Try

        Catch ex As Exception

        End Try
    End Sub


End Class
