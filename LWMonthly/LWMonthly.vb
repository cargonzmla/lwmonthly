﻿Imports micrologic

Public Class LWMonthly

    'Friend WithEvents tmrUnlimited As System.Timers.Timer
    Private tmrUnlimited As System.Timers.Timer
    Private tmrLoad As System.Timers.Timer


    'Private mylog As EventLog

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.


        Dim path As String
        path = My.Application.Info.DirectoryPath
        Dim iCounter As Integer = 0
        Dim HRIP As String = ""
        'Dim sPassword As String = ""

        'If AmIRunning("", My.Application.Info.AssemblyName & ".exe") = True Then

        '    Exit Sub
        'End If

        'CONFIGURATION VARIABLES
        dsConfig.ReadXml(path & "\reportsconfig.xml")

        sServerName = dsConfig.Tables("Database").Rows(0).Item("ServerName").ToString
        sDatabaseName = dsConfig.Tables("Database").Rows(0).Item("DatabaseName").ToString
        sServerUser = dsConfig.Tables("Database").Rows(0).Item("UserName").ToString
        sServerPassword = dsConfig.Tables("Database").Rows(0).Item("Password").ToString

        sMerchantNumber = dsConfig.Tables("CreditCard").Rows(0).Item("MerchantNumber").ToString
        sProcessorCode = dsConfig.Tables("CreditCard").Rows(0).Item("ProcessorCode").ToString
        sCCProgramAddress = dsConfig.Tables("CreditCard").Rows(0).Item("CCProgramAddress").ToString
        sCCDBPath = dsConfig.Tables("CreditCard").Rows(0).Item("DBPath").ToString
        sCCProgramPath = dsConfig.Tables("CreditCard").Rows(0).Item("CCProgramPath").ToString
        'bCCRelayMode = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("RelayMode"))
        iCCTimeOut = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("CCTimeOut"))
        bAutoCloseBatch = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("AutoCloseBatch"))
        iOfflineProcessing = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("ProcessOfflineCC"))
        iofflinehour = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("ProcessOfflineCCHour"))
        iofflineminute = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("ProcessOfflineCCMinute"))
        iPCCWTransMode = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("PCCWTransMode"))
        sCCUser_PW = dsConfig.Tables("CreditCard").Rows(0).Item("User_PW").ToString
        bDisableForceFlag = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("DisableForceFlag"))
        iCreditMode = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("CreditMode"))
        sClientId = dsConfig.Tables("CreditCard").Rows(0).Item("ClientId").ToString
        iCCProgramPort = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("CCProgramPort"))
        iUnlimitedHour = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("UnlimitedProcessHour"))
        bProcessMonthlyAccounts = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("ProcessMonthlyAccounts"))
        iMaximumTicketLength = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("MaximumTicketLength"))
        bResetUnlimitedStartDateAfterTimeOut = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("ResetUnlimitedStartDateAfterTimeOut"))
        iResetUnlimitedStartDateDaysAfterMonthlyNextPay = CInt(dsConfig.Tables("CreditCard").Rows(0).Item("ResetUnlimitedStartDateDaysAfterMonthlyNextPay"))
        bDisableUnlimitedBackCharge = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("DisableUnlimitedBackCharge"))
        bResetUnlimitedStartDateAfterDecline = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("ResetUnlimitedStartDateAfterDecline"))
        bCloseUnlimitedShift = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("CloseUnlimitedShift"))
        bCheckHistoryForDuplicateCharge = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("CheckHistoryForDuplicateCharge"))
        bSaveSecurePaymentLog = CBool(dsConfig.Tables("CreditCard").Rows(0).Item("SaveSecurePaymentLog"))
        sEdgeDirectUrl = dsConfig.Tables("CreditCard").Rows(0).Item("EdgeDirectUrl").ToString

        'bShowDash = CBool(dsConfig.Tables("options").Rows(0).Item("ShowDash"))
        'HRIP = dsConfig.Tables("options").Rows(0).Item("hrip").ToString
        'iReportStart = CInt(dsConfig.Tables("options").Rows(0).Item("ReportStart"))
        'iReportStop = CInt(dsConfig.Tables("options").Rows(0).Item("ReportStop"))
        'bShowLastHour = CBool(dsConfig.Tables("options").Rows(0).Item("ShowLastHour"))
        'iAlertsInterval = CInt(dsConfig.Tables("options").Rows(0).Item("AlertsInterval"))
        'iKioskTimeOut = CInt(dsConfig.Tables("options").Rows(0).Item("KioskTimeOut"))
        'bTimeClockMonitor = CBool(dsConfig.Tables("options").Rows(0).Item("TimeClockMonitor"))
        iFamilyAccountDiscountMode = CInt(dsConfig.Tables("Options").Rows(0).Item("FamilyAccountDiscountMode"))
        bManualFamilyAccountVerification = CBool(dsConfig.Tables("Options").Rows(0).Item("ManualFamilyAccountVerification"))
        iSendUnlimitedAutoChargeEmail = CInt(Val(dsConfig.Tables("Options").Rows(0).Item("SendUnlimitedAutoChargeEmail")))
        sEmailAPIURL = dsConfig.Tables("Options").Rows(0).Item("EmailAPIURL").ToString
        sEmailReceiptSubjectLine = dsConfig.Tables("Options").Rows(0).Item("EmailReceiptSubjectLine").ToString
        sMailFromAddress = dsConfig.Tables("Options").Rows(0).Item("SMTPFromAddress").ToString
        ' bEnableMonthlyProcessOnlyMode = CBool(dsConfig.Tables("options").Rows(0).Item("EnableMonthlyProcessOnlyMode"))

        'sReportsPath = dsConfig.Tables("Paths").Rows(0).Item("ReportsPath").ToString
        'If Mid(sReportsPath, sReportsPath.Length, 1) <> "\" Then
        '    sReportsPath = sReportsPath & "\"
        'End If


        'ReportButtons(0) = Button1
        'ReportButtons(1) = Button2
        'ReportButtons(2) = Button3
        'ReportButtons(3) = Button4
        'ReportButtons(4) = Button5
        'ReportButtons(5) = Button6
        'ReportButtons(6) = Button7
        'ReportButtons(7) = Button8
        'ReportButtons(8) = Button9
        'ReportButtons(9) = Button10

        If HRIP <> "" Then

        End If

        dsCC.Tables.Add("Queue")

        dsCC.Tables(0).Columns.Add("DateReceived")
        dsCC.Tables(0).Columns.Add("HostAddress")
        dsCC.Tables(0).Columns.Add("Status")
        dsCC.Tables(0).Columns.Add("sMessage")
        dsCC.Tables(0).Columns.Add("sTicketNumber")
        dsCC.Tables(0).Columns.Add("iIndex")


        'grdQueue.DataSource = dsCC.Tables("Queue")

        'grdQueue.Columns(0).Width = 150
        'grdQueue.Columns(1).Width = 150
        'grdQueue.Columns(2).Width = 150
        'grdQueue.Columns(3).Width = 150
        'grdQueue.Columns(4).Width = 150
        'grdQueue.Columns(5).Width = 10

        'grdQueue.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight


        dsCC.Tables.Add("Log")

        dsCC.Tables(1).Columns.Add("DateCreated")
        dsCC.Tables(1).Columns.Add("TicketNumber")
        dsCC.Tables(1).Columns.Add("ResponseTime")
        dsCC.Tables(1).Columns.Add("Result")
        dsCC.Tables(1).Columns.Add("TransactionId")

        'grdLog.DataSource = dsCC.Tables("Log")

        'grdLog.Columns(0).Width = 152
        'grdLog.Columns(1).Width = 152
        'grdLog.Columns(2).Width = 102
        'grdLog.Columns(3).Width = 202
        'grdLog.Columns(4).Width = 152
        'grdQueue.Columns(4).Width = 90
        'grdQueue.Columns(5).Width = 150

        'grdQueue.Columns(2).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight




        Me.tmrUnlimited = New System.Timers.Timer()
        Me.tmrUnlimited.Interval = 20000

        AddHandler tmrUnlimited.Elapsed, AddressOf Me.tmrUnlimited_Tick

        Me.tmrLoad = New System.Timers.Timer()
        Me.tmrLoad.Interval = 1000

        AddHandler tmrLoad.Elapsed, AddressOf Me.tmrLoad_Tick

    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        Me.tmrLoad.Enabled = True
        ' EventLog.WriteEntry("LWMonthly", "LWMonthly first starts", EventLogEntryType.Information)

    

        'TheDate = Today
        'LogData("LINE")
        'LogData(Today.ToString)

        'start SOCKET listening here
        'listenerThread = New Threading.Thread(AddressOf DoListen)
        'listenerThread.Start()

        'mylog.WriteEntry("PriceCheckerServer now listening", EventLogEntryType.Information)
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Me.tmrUnlimited.Enabled = False
    End Sub

    Private Sub tmrLoad_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles tmrLoad.Tick


        tmrLoad.Enabled = False


        'Dim i As Integer = 0

        Try

         

            ' Me.Text = sFormText

            ' Me.Text = sFormText & " - Loading database."
            EventLog.WriteEntry("LWMonthly", "Loading database.", EventLogEntryType.Information)
            If dbhelper.OpenDatabase(sServerName, sDatabaseName, sServerUser, sServerPassword, cnnLogicWash) = True Then
                ' Me.Text = sFormText
                User.DBConnection(cnnLogicWash)
                'Display.DBConnection(cnnLogicWash)
                'DisplayInfo = Display.GetCustomerDisplayMessages(DisplayInfo)
                Account.DBConnection(cnnLogicWash)
                'Service.DBConnection(cnnLogicWash)
                'POSButton.DBConnection(cnnLogicWash)
                Discount.DBConnection(cnnLogicWash)
                Surcharge.DBConnection(cnnLogicWash)
                'HouseAccount.DBConnection(cnnLogicWash)
                ''HouseAccountService.DBConnection(cnnLogicWash)
                'Inventory.DBConnection(cnnLogicWash)
                Vehicle.DBConnection(cnnLogicWash)
                Customer.DBConnection(cnnLogicWash)
                'WorkOrder.DBConnection(cnnLogicWash)
                Ticket.DBConnection(cnnLogicWash)
                TicketDetail.DBConnection(cnnLogicWash)
                'VIPAccount.DBConnection(cnnLogicWash)
                'PinCode.DBConnection(cnnLogicWash)
                Payment.DBConnection(cnnLogicWash)
                Employee.DBConnection(cnnLogicWash)
                CreditCard.DBConnection(cnnLogicWash)
                GiftCard.DBConnection(cnnLogicWash)
                'CheckChart.DBConnection(cnnLogicWash)
                'Vendor.DBConnection(cnnLogicWash)
                'History.DBConnection(cnnLogicWash)
                ' ReportDB.DBConnection(cnnLogicWash)
                'TicketRefund.DBConnection(cnnLogicWash)
                PrepaidBook.DBConnection(cnnLogicWash)
                'Parker.DBConnection(cnnLogicWash)
                'Macro.DBConnection(cnnLogicWash)
                ''POSReports.DBConnection = cnnLogicWash
                'LoadCustomerDisplayMessages()
                cPoints.DBConnection(cnnLogicWash)
                PCCW.DBConnection(cnnLogicWash)

                LoadForm()

            Else

                EventLog.WriteEntry("LWMonthly", "unable to connect to database.  Retrying in 30 seconds.", EventLogEntryType.Information)
                'Me.Text = sFormText & " - unable to connect to database.  Retrying in " & 30 - iDBCounter & " seconds."

                tmrLoad.Interval = 30000
                tmrLoad.Enabled = True
                Exit Sub

            End If

            'Else

            '    MsgBox("Unable to open database.", , "LogicWashPOS")



        Catch ex As Exception
            Exit Sub
        End Try



    End Sub

    Private Sub tmrUnlimited_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) 'Handles tmrUnlimited.Elapsed
        Dim iunlimitedminute As Integer = 0
        Dim sReturn As String = ""
        Dim bProcessNow As Boolean = False
        Try

            'iUnlimitedHour = 16
            'iunlimitedminute = 12

            tmrUnlimited.Enabled = False

            'EventLog.WriteEntry("LWMonthly", "tmrUnlimited_Tick Elapsed", EventLogEntryType.Information)


            Try
                If lComputerTerminalId > 0 Then
                    bProcessNow = CBool(Account.GetAppSetting(lComputerTerminalId, "LWMonthly", "Options", "ProcessNow", "0", False))

                    If bProcessNow = True Then
                        EventLog.WriteEntry("LWMonthly", "ProcessNow = True", EventLogEntryType.Information)
                        If Account.SetAppSetting(lComputerTerminalId, "LWMonthly", "Options", "ProcessNow", "0") = False Then
                            bProcessNow = False
                            EventLog.WriteEntry("LWMonthly", "bProcessNow = False", EventLogEntryType.Information)
                        Else
                            EventLog.WriteEntry("LWMonthly", "bProcessNow = True", EventLogEntryType.Information)
                        End If
                    End If
                End If
            Catch ex As Exception

            End Try


            If (((Hour(Now) = iUnlimitedHour Or Hour(Now.AddHours(-1)) = iUnlimitedHour) And Minute(Now) = iunlimitedminute) Or bProcessNow = True) And bProcessMonthlyAccounts = True Then

                If bProcessingMonthly = False Then
                    bProcessingMonthly = True
                    EventLog.WriteEntry("LWMonthly", "ProcessingMonthly = True", EventLogEntryType.Information)
                    sReturn = Customer.IsMonthlyDueToday(AccountInfo.LocationDesc)
                    If sReturn = "True" Then
                        If iCreditMode >= 4 Then
                            ProcessPayProsConversion() 'convert any accounts that have had their credit card
                            'info updated recently but haven't yet been converted to PayPros
                        End If

                        ' Me.Menu.MenuItems(4).Enabled = False
                        If bProcessNow = True Then
                            ProcessMonthlyAccounts(False)
                        Else
                            ProcessMonthlyAccounts(True)
                        End If

                        'Me.Menu.MenuItems(4).Enabled = True
                        Do Until Hour(Now) <> iUnlimitedHour Or Minute(Now) <> 0
                            System.Threading.Thread.Sleep(500)
                        Loop
                    Else
                        WriteToLog(sLogFileName, "IsMonthlyDueToday", 0, sReturn, "EVENT")
                    End If
                    bProcessingMonthly = False
                    EventLog.WriteEntry("LWMonthly", "ProcessingMonthly = False", EventLogEntryType.Information)
                End If
            Else
                If Hour(Now) = 1 And Minute(Now) < 3 Then
                    bProcessingMonthly = False
                    If Minute(Now) = 1 Then
                        EventLog.WriteEntry("LWMonthly", "ProcessingMonthly = False", EventLogEntryType.Information)
                    End If
                End If

            End If

            tmrUnlimited.Enabled = True

        Catch ex As Exception
            tmrUnlimited.Enabled = True
            'Me.Menu.MenuItems(4).Enabled = True
            bProcessingMonthly = False
            EventLog.WriteEntry("LWMonthly", "ProcessingMonthly = False", EventLogEntryType.Information)
        End Try
    End Sub

    Private Sub LoadForm()
        Dim iCounter As Integer = 0

        'Dim mMenu As New MainMenu
        'Dim rptMenu As New MenuItem
        'Dim filMenu As New MenuItem
        'Dim cfgMenu As New MenuItem
        'Dim pwdMenu As New MenuItem
        'Dim monMenu As New MenuItem

        Dim sFormText As String = ""
        Dim sPassword As String = ""
        Try

            sFormText = "LWReports v." & My.Application.Info.Version.Major & "." & Format(My.Application.Info.Version.Minor, "00") & " (" & My.Application.Info.Version.Revision & ")"

            ' Me.Text = sFormText


            lComputerTerminalId = Account.GetTerminalId(sComputerName)

            AccountInfo = Account.GetAccount


            If AccountInfo.XWebID <> "" Then
                sEdgeWebId = AccountInfo.XWebID
            End If

            If AccountInfo.AuthKey <> "" Then
                sEdgeAuthKey = AccountInfo.AuthKey
            End If

            If AccountInfo.TerminalID <> "" Then
                sEdgeTerminalId = AccountInfo.TerminalID
            End If

            sPassword = LogicDecrypt(AccountInfo.Password) '& AccountInfo.PasswordCounter

            If sPassword = "" And sCCUser_PW <> "" Then
                If Account.SavePassword(LogicEncrypt(sCCUser_PW)) = True Then
                    AccountInfo.Password = LogicEncrypt(sCCUser_PW)
                End If
                If AccountInfo.PasswordCounter > 0 Then sCCUser_PW = sCCUser_PW & AccountInfo.PasswordCounter
            ElseIf sPassword <> "" And sCCUser_PW <> "" Then
                sCCUser_PW = sPassword & AccountInfo.PasswordCounter
            End If

            tmrUnlimited.Enabled = True

            'sDepts = ReportDB.GetDepts



            'rptMenu.Text = "&Reports"
            'filMenu.Text = "&File"
            'cfgMenu.Text = "&Settings"
            'pwdMenu.Text = "&Payware"
            'monMenu.Text = "&Monthly"


            'filMenu.MenuItems.Add("E&xit", New System.EventHandler(AddressOf Me.ClickExit))
            'mMenu.MenuItems.Add(filMenu)

            'DisplayReports = ReportDB.GetReports(False)


            'Do Until DisplayReports(iCounter) Is Nothing Or iCounter = 10

            '    'If DisplayReports(iCounter).ShowReport = True Then
            '    'ReportButtons(iCounter).Visible = True
            '    'ReportButtons(iCounter).Text = DisplayReports(iCounter).DisplayName
            '    'ReportButtons(iCounter).Tag = iCounter
            '    rptMenu.MenuItems.Add(DisplayReports(iCounter).DisplayName, New System.EventHandler(AddressOf Me.ClickMenu))

            '    'Me.Menu.MenuItems.Add(DisplayReports(iCounter).DisplayName)

            '    ' Me.MenuStrip1.Items.Add(DisplayReports(iCounter).DisplayName)
            '    'End If

            '    iCounter = iCounter + 1
            'Loop
            'mMenu.MenuItems.Add(rptMenu)



            '' cfgMenu.MenuItems.Add("Reports", New System.EventHandler(AddressOf Me.SettingsClick))
            'cfgMenu.MenuItems.Add("SMS Setup", New System.EventHandler(AddressOf Me.ClickSettings))
            'cfgMenu.MenuItems.Add("SMS Send", New System.EventHandler(AddressOf Me.ClickSettings))
            'cfgMenu.MenuItems.Add("---", New System.EventHandler(AddressOf Me.ClickSettings))
            'cfgMenu.MenuItems.Add("Close Batch", New System.EventHandler(AddressOf Me.ClickSettings))
            'mMenu.MenuItems.Add(cfgMenu)

            'pwdMenu.MenuItems.Add("Change Password", New System.EventHandler(AddressOf Me.ClickPayware))
            'mMenu.MenuItems.Add(pwdMenu)

            'If bProcessMonthlyAccounts = True Then
            '    monMenu.MenuItems.Add("&Process Now", New System.EventHandler(AddressOf Me.MonthlyClick))
            '    monMenu.MenuItems.Add("&Zero Dollar PreAuth", New System.EventHandler(AddressOf Me.MonthlyClick))
            '    mMenu.MenuItems.Add(monMenu)
            'End If

            'Me.Menu = mMenu

            'Panel2.Width = CInt(850 * iZoom / 100)
            'Panel2.Height = CInt(1100 * iZoom / 100)


            'If bShowDash = True Then
            '    Me.pnlDash.Visible = True
            '    'Me.btnDash.Visible = True
            '    'Me.Timer1.Enabled = True
            'Else
            '    Me.pnlDash.Visible = False
            '    ' Me.btnDash.Visible = False
            '    ' Me.Timer1.Enabled = False
            'End If

            'If bTimeClockMonitor = True Then
            '    tmrTimeClock.Enabled = True
            'End If

            'Me.lblCompany.Text = ReportDB.GetAccountName
            'Me.Timer1.Enabled = True
            'bTimerEnabled = True

            'If bCCRelayMode = True Then
            '    tmrListen.Enabled = True
            '    'Winsock1.Listen()
            'End If

            'UpdateLogGrid()

        Catch ex As Exception
            EventLog.WriteEntry("LWMonthly", ex.Message, EventLogEntryType.Information)
            'MsgBox(ex.Message)
        End Try
    End Sub



End Class
