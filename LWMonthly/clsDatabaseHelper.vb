Option Strict On
Imports System.Data
Imports System.Data.SqlClient

Public Class DatabaseHelper
    Public strConnectString As String
    Public DBConnection As New SqlConnection

    Public Function ResetDatabase() As Boolean

        Try



            DBConnection.Close()
            DBConnection.Open()



            Return True
        Catch ex As Exception
            EventLog.WriteEntry("LWMonthly", "Unable to connect to database", EventLogEntryType.Information)
            'MsgBox("Unable to connect to database")
            Return False
        End Try

    End Function



    Public Sub OpenDatabase(ByVal Server As String, ByVal DatabaseName As String, ByVal UserID As String, ByVal Password As String)

        strConnectString = "Server=" & Server & _
                           ";Database=" & DatabaseName & _
                           ";User Id=" & UserID & _
                           ";Password=" & Password & _
                           ";" 'Connection Timeout = 5;"

        DBConnection.ConnectionString = strConnectString

        Try
            DBConnection.Open()

        Catch ex As Exception
            'MsgBox(ex.Message.ToString)
        End Try

    End Sub

    Public Function OpenDatabase(ByVal Server As String, ByVal DatabaseName As String, _
                                 ByVal UserID As String, ByVal Password As String, _
                                 ByVal DBConnection As System.Data.SqlClient.SqlConnection) As Boolean

        strConnectString = "Server=" & Server & _
                           ";Database=" & DatabaseName & _
                           ";User Id=" & UserID & _
                           ";Password=" & Password & ";"

        DBConnection.ConnectionString = strConnectString

        Try
            DBConnection.Open()
            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub OpenDatabase(ByVal AccessDBFileName As String)

        strConnectString = "Provider=Microsoft.Jet.OLEDB.4.0" & _
                           ";Data Source=" & AccessDBFileName & ";"

        DBConnection.ConnectionString = strConnectString

        Try
            DBConnection.Open()

        Catch ex As Exception
            'MsgBox(ex.Message.ToString)
        End Try

    End Sub
    Public Function ExecuteNonQuery(ByVal sql As String) As Integer ', ByVal params() As SqlParameter

        Dim cmdCommand As New System.Data.SqlClient.SqlCommand(sql, DBConnection)
        Dim intRetval As Integer = 0

        Try
            intRetval = cmdCommand.ExecuteNonQuery()
            Return intRetval

        Catch ex As Exception


            If ResetDatabase() Then
                intRetval = cmdCommand.ExecuteNonQuery()
            Else
                intRetval = -1
            End If
            Return intRetval
        End Try


    End Function

    Public Function ExecuteDataReader(ByVal SQL As String) As SqlClient.SqlDataReader
        Dim cmdDBCommand As New SqlCommand
        Dim rdrDBReader As SqlDataReader

        cmdDBCommand.CommandType = CommandType.Text
        cmdDBCommand.CommandText = SQL
        cmdDBCommand.Connection = DBConnection

        Try
            rdrDBReader = cmdDBCommand.ExecuteReader()
            Return rdrDBReader

        Catch ex As Exception
            If ResetDatabase() Then
                rdrDBReader = cmdDBCommand.ExecuteReader()
            Else
                'MsgBox("Unable to connect to database.")
                Return Nothing
            End If
            Return rdrDBReader
        End Try

    End Function

    Public Function ExecuteDataSet(ByVal sql As String) As DataSet
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter(sql, strConnectString)
        da.Fill(ds)
        Return ds
    End Function

    Public Function ExecuteScalar(ByVal sql As String, ByVal params() As SqlParameter) As Object
        Dim cnn As New SqlConnection(strConnectString)
        Dim cmd As New SqlCommand(sql, cnn)
        For i As Integer = 0 To params.Length - 1
            cmd.Parameters.Add(params(i))
        Next
        cnn.Open()
        Dim retval As Object = cmd.ExecuteScalar
        cnn.Close()
        Return retval
    End Function

    Public Function ExecuteDataSet(ByVal sql As String, ByVal params() As SqlParameter) As DataSet
        Dim ds As New DataSet
        Dim da As New SqlDataAdapter(sql, strConnectString)
        For i As Integer = 0 To params.Length - 1
            da.SelectCommand.Parameters.Add(params(i))
        Next
        da.Fill(ds)
        Return ds
    End Function

    Public Function CloseDatabase() As Boolean
        Try
            DBConnection.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

End Class
