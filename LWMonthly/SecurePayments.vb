Imports System.Xml.Linq
Imports System.Net
Imports System.web
Imports System.io
Imports System.Text

'Imports XCharge.XpressLink2

Public Class SecurePaymentResponse

    'Public Sub New()

    'End Sub

    Private sSpan As String
    Private sBin As String
    Private iMpdResponseCode As Integer
    Private sMpdResponseCodeText As String
    Private sPayerIdentifier As String
    Private dtManageUntil As Date
    Private sExpireMonth As String
    Private sExpireYear As String
    Private iResponseCode As Integer
    Private iSecondaryResponseCode As Integer
    Private sBankApprovalCode As String
    Private sReferenceId As String
    Private sRequestedAmount As String
    Private sCapturedAmount As String
    Private sResponseCodeText As String
    Private sCardBrand As String
    Private sTransactionId As String
    Private sSignature As String



    Public Sub New()
        sSpan = ""
        sBin = ""
        iMpdResponseCode = 0
        sMpdResponseCodeText = ""
        sPayerIdentifier = ""
        dtManageUntil = CDate("1/1/1900")
        sExpireMonth = ""
        sExpireYear = ""
        iResponseCode = 0
        iSecondaryResponseCode = 0
        sBankApprovalCode = ""
        sReferenceId = ""
        sRequestedAmount = ""
        sCapturedAmount = ""
        sResponseCodeText = ""
        sCardBrand = ""
        sTransactionId = ""

        sSignature = ""

    End Sub



    Public Function GetSignature() As String
        Return sSignature
    End Function

    Public Sub SetSignature(ByVal strSignature As String)
        sSignature = strSignature
    End Sub

    'Public Function GetExpireDate() As String
    '    Return sExpireDate
    'End Function

    'Public Sub SetExpireDate(ByVal strExpireDate As String)
    '    sExpireDate = strExpireDate
    'End Sub

    Public Function GetTransactionId() As String
        Return sTransactionId
    End Function

    Public Sub SetTransactionId(ByVal strTransactionId As String)
        sTransactionId = strTransactionId
    End Sub


    Public Function GetCardBrand() As String
        Return sCardBrand
    End Function

    Public Sub SetCardBrand(ByVal strCardBrand As String)
        sCardBrand = strCardBrand
    End Sub

    Public Function GetSpan() As String
        Return sSpan
    End Function

    Public Sub SetSpan(ByVal strSpan As String)
        sSpan = strSpan
    End Sub



    Public Function GetBin() As String
        Return sBin
    End Function

    Public Sub SetBin(ByVal strBin As String)
        sBin = strBin
    End Sub



    Public Function GetMpdResponseCode() As Integer
        Return iMpdResponseCode
    End Function

    Public Sub SetMpdResponseCode(ByVal intMpdResponseCode As Integer)
        iMpdResponseCode = intMpdResponseCode
    End Sub



    Public Function GetMpdResponseCodeText() As String
        Return sMpdResponseCodeText
    End Function

    Public Sub SetMpdResponseCodeText(ByVal strMpdResponseCodeText As String)
        sMpdResponseCodeText = strMpdResponseCodeText
    End Sub



    Public Function GetPayerIdentifier() As String
        Return sPayerIdentifier
    End Function

    Public Sub SetPayerIdentifier(ByVal strPayerIdentifier As String)
        sPayerIdentifier = strPayerIdentifier
    End Sub


    Public Function GetManageUntil() As Date
        Return dtManageUntil
    End Function

    Public Sub SetManageUntil(ByVal dateManageUntil As Date)
        dtManageUntil = dateManageUntil
    End Sub



    Public Function GetExpireMonth() As String
        Return sExpireMonth
    End Function

    Public Sub SetExpireMonth(ByVal strExpireMonth As String)
        sExpireMonth = strExpireMonth
    End Sub



    Public Function GetExpireYear() As String
        Return sExpireYear
    End Function

    Public Sub SetExpireYear(ByVal strExpireYear As String)
        sExpireYear = strExpireYear
    End Sub



    Public Function GetResponseCode() As Integer
        Return iResponseCode
    End Function

    Public Sub SetResponseCode(ByVal intResponseCode As Integer)
        iResponseCode = intResponseCode
    End Sub



    Public Function GetSecondaryResponseCode() As Integer
        Return iSecondaryResponseCode
    End Function

    Public Sub SetSecondaryResponseCode(ByVal intSecondaryResponseCode As Integer)
        iSecondaryResponseCode = intSecondaryResponseCode
    End Sub



    Public Function GetBankApprovalCode() As String
        Return sBankApprovalCode
    End Function

    Public Sub SetBankApprovalCode(ByVal strBankApprovalCode As String)
        sBankApprovalCode = strBankApprovalCode
    End Sub



    Public Function GetReferenceId() As String
        Return sReferenceId
    End Function

    Public Sub SetReferenceId(ByVal strReferenceId As String)
        sReferenceId = strReferenceId
    End Sub



    Public Function GetRequestedAmount() As String
        Return sRequestedAmount
    End Function

    Public Sub SetRequestedAmount(ByVal strRequestedAmount As String)
        sRequestedAmount = strRequestedAmount
    End Sub



    Public Function GetCapturedAmount() As String
        Return sCapturedAmount
    End Function

    Public Sub SetCapturedAmount(ByVal strCapturedAmount As String)
        sCapturedAmount = strCapturedAmount
    End Sub



    Public Function GetResponseCodeText() As String
        Return sResponseCodeText
    End Function

    Public Sub SetResponseCodeText(ByVal strResponseCodeText As String)
        sResponseCodeText = strResponseCodeText
    End Sub

End Class





Public Class SecurePaymentRequest

    Private sChargeType As String
    Private sChargeTotal As String
    Private sInvoiceNumber As String
    Private sManagePayerData As String
    Private sPurchaseOrderNumber As String
    Private sPayerIdentifier As String
    Private sTransactionConditionCode As String
    Private sIndustry As String
    Private sSecurePrimaryAccountNumber As String
    Private sCreditCardNumber As String
    Private sExpireMonth As String
    Private sExpireYear As String
    Private sTrack2 As String
    Private sTrack1 As String
    Private sTrackData As String
    Private sFullVoidFlag As String
    Private sOrderId As String
    Private sPartialApprovalFlag As String
    Private sBillPostalCode As String
    Private sTransactionType As String
    Private sExpDate As String
    Private sSignatureTitle As String
    Private sSignatureText As String
    Private sTransactionId As String

    Sub New()
        sChargeType = ""
        sChargeTotal = ""
        sInvoiceNumber = ""
        sManagePayerData = ""
        sPurchaseOrderNumber = ""
        sPayerIdentifier = ""
        sTransactionConditionCode = ""
        sIndustry = ""
        sSecurePrimaryAccountNumber = ""
        sCreditCardNumber = ""
        sExpireMonth = ""
        sExpireYear = ""
        sTrack2 = ""
        sTrack1 = ""
        sFullVoidFlag = ""
        sOrderId = ""
        sPartialApprovalFlag = ""
        sBillPostalCode = ""
        sTransactionType = ""
        sExpDate = ""
        sSignatureTitle = ""
        sSignatureText = ""
        sTransactionId = ""
    End Sub

    'Public Function GetTransactionId() As String
    '    Return sTransactionId
    'End Function

    'Public Sub SetTransactionId(ByVal strTransactionId As String)
    '    sTransactionId = strTransactionId
    'End Sub

    Public Function GetExpDate() As String
        Return sExpDate
    End Function

    Public Sub SetExpDate(ByVal strExpDate As String)
        sExpDate = strExpDate
    End Sub

    Public Function GetTransactionType() As String
        Return sTransactionType
    End Function

    Public Sub SetTransactionType(ByVal strTransactionType As String)
        sTransactionType = strTransactionType
    End Sub

    Public Function GetChargeType() As String
        Return sChargeType
    End Function

    Public Sub SetChargeType(ByVal strChargeType As String)
        sChargeType = strChargeType
    End Sub



    Public Function GetChargeTotal() As String
        Return sChargeTotal
    End Function

    Public Sub SetChargeTotal(ByVal strChargeTotal As String)
        sChargeTotal = strChargeTotal
    End Sub


    Public Function GetInvoiceNumber() As String
        Return sInvoiceNumber
    End Function

    Public Sub SetInvoiceNumber(ByVal strInvoiceNumber As String)
        sInvoiceNumber = strInvoiceNumber
    End Sub



    Public Function GetManagePayerData() As String
        Return sManagePayerData
    End Function

    Public Sub SetManagePayerData(ByVal strManagePayerData As String)
        sManagePayerData = strManagePayerData
    End Sub



    Public Function GetPurchaseOrderNumber() As String
        Return sPurchaseOrderNumber
    End Function

    Public Sub SetPurchaseOrderNumber(ByVal strPurchaseOrderNumber As String)
        sPurchaseOrderNumber = strPurchaseOrderNumber
    End Sub



    Public Function GetPayerIdentifier() As String
        Return sPayerIdentifier
    End Function

    Public Sub SetPayerIdentifier(ByVal strPayerIdentifier As String)
        sPayerIdentifier = strPayerIdentifier
    End Sub



    Public Function GetTransactionConditionCode() As String
        Return sTransactionConditionCode
    End Function

    Public Sub SetTransactionConditionCode(ByVal strTransactionConditionCode As String)
        sTransactionConditionCode = strTransactionConditionCode
    End Sub



    Public Function GetIndustry() As String
        Return sIndustry
    End Function

    Public Sub SetIndustry(ByVal strIndustry As String)
        sIndustry = strIndustry
    End Sub



    Public Function GetSecurePrimaryAccountNumber() As String
        Return sSecurePrimaryAccountNumber
    End Function

    Public Sub SetSecurePrimaryAccountNumber(ByVal strSecurePrimaryAccountNumber As String)
        sSecurePrimaryAccountNumber = strSecurePrimaryAccountNumber
    End Sub



    Public Function GetCreditCardNumber() As String
        Return sCreditCardNumber
    End Function

    Public Sub SetCreditCardNumber(ByVal strCreditCardNumber As String)
        sCreditCardNumber = strCreditCardNumber
    End Sub



    Public Function GetExpireMonth() As String
        Return sExpireMonth
    End Function

    Public Sub SetExpireMonth(ByVal strExpireMonth As String)
        sExpireMonth = strExpireMonth
    End Sub



    Public Function GetExpireYear() As String
        Return sExpireYear
    End Function

    Public Sub SetExpireYear(ByVal strExpireYear As String)
        sExpireYear = strExpireYear
    End Sub



    Public Function GetTrack2() As String
        Return sTrack2
    End Function

    Public Sub SetTrack2(ByVal strTrack2 As String)
        sTrack2 = strTrack2
    End Sub


    Public Function GetTrack1() As String
        Return sTrack1
    End Function

    Public Sub SetTrack1(ByVal strTrack1 As String)
        sTrack1 = strTrack1
    End Sub

    Public Function GetTrackData() As String
        Return sTrackData
    End Function

    Public Sub SetTrackData(ByVal strTrackData As String)
        sTrackData = strTrackData
    End Sub



    Public Function GetFullVoidFlag() As String
        Return sFullVoidFlag
    End Function

    Public Sub SetFullVoidFlag(ByVal strFullVoidFlag As String)
        sFullVoidFlag = strFullVoidFlag
    End Sub



    Public Function GetOrderId() As String
        Return sOrderId
    End Function

    Public Sub SetOrderId(ByVal strOrderId As String)
        sOrderId = strOrderId
    End Sub



    Public Function GetPartialApprovalFlag() As String
        Return sPartialApprovalFlag
    End Function

    Public Sub SetPartialApprovalFlag(ByVal strPartialApprovalFlag As String)
        sPartialApprovalFlag = strPartialApprovalFlag
    End Sub



    Public Function GetBillPostalCode() As String
        Return sBillPostalCode
    End Function

    Public Sub SetBillPostalCode(ByVal strBillPostalCode As String)
        sBillPostalCode = strBillPostalCode
    End Sub

    Public Function GetSignatureTitle() As String
        Return sSignatureTitle
    End Function

    Public Sub SetSignatureTitle(ByVal strSignatureTitle As String)
        sSignatureTitle = strSignatureTitle
    End Sub

    Public Function GetSignatureText() As String
        Return sSignatureText
    End Function

    Public Sub SetSignatureText(ByVal strSignatureText As String)
        sSignatureText = strSignatureText
    End Sub



End Class

Module SecurePayments

    Public Function doDirectToGatewayTransaction(ByVal spRequest As SecurePaymentRequest, ByVal iCCLogId As Long, ByVal boffline As Boolean, ByVal bDuplicateCheck As Boolean, ByVal bRecurring As Boolean) As SecurePaymentResponse
        Dim setupParameters As String = ""
        Dim sResponse As String = ""

        Dim sAcctNum As String = ""
        Dim sExpDate As String = ""

        Dim sChargeType As String = ""

        ' Dim CreditCardLogInfo As New Micrologic.CreditCardLogProperties
        Dim dtSent As DateTime
        Dim dtReceived As DateTime

        Dim spResponse As New SecurePaymentResponse

        Dim bTokenUpdate As Boolean = False
        Dim bTokenConversion As Boolean = False
        Dim bTokenDelete As Boolean = False

        Dim sDisplayNumber As String = ""
        Dim sTransaction As String = ""
        Dim iStart As Integer = 0
        Dim iEnd As Integer = 0

        Dim sBin As String = ""
        Dim sSPan As String = ""

        Dim sTrackData As String = ""
        'Dim sPadding As String = ""
        Try


            'If spRequest.GetTrack1 = "" Then
            '    sPadding = ";"
            'Else
            '    sPadding = "?;"
            'End If


            sTrackData = spRequest.GetTrack2
            sExpDate = spRequest.GetExpDate
            sAcctNum = spRequest.GetCreditCardNumber
            sExpDate = spRequest.GetExpDate
            If sAcctNum.Length > 14 Then
                sBin = Mid(sAcctNum, 1, 6)
                sSPan = Mid(sAcctNum, sAcctNum.Length - 3)
            End If

            sChargeType = spRequest.GetChargeType

            Select Case sChargeType
                Case "SALE", "CREDITSALE"
                    sChargeType = "CreditSaleTransaction"

                Case "RETURN", "CREDITRETURN"
                    sChargeType = "CreditReturnTransaction"

            End Select

            If Val(spRequest.GetChargeTotal) = 0 Then

                sChargeType = "CreditAuthTransaction"

            End If



            ''string entryMode = "entry_mode=KEYED";
            ''Dim entryMode As String = "entry_mode=SWIPED"
            'Dim entryMode As String = "entry_mode=EMV"
            ''Dim deviceModel As String = "pos_device_model=ingenico_isc250"
            'Dim transactionType As String = "transaction_type=CREDIT_CARD"
            'Dim chargeType As String = "charge_type=QUERY_PAYMENT"
            '' Dim protocol_version As String = "" '"protocol_version=13"
            ''Dim chargeAmount As String = "charge_total=1.00"
            ''Dim token As String = "account_token=" + "FDC4E33FCE6C54083389EDF18D50AF64C3C0259544B6C3FFEEA64B268DDD5FEB9E52BE59D7BDC8EC72" ' accountToken.Text
            ''Dim token As String = "account_token=" + "D80332EECE61540B3988EDF18754A765C8CF299440B0C6FDEBA64E2A8CDF5FEA9C55BB5B6E19B1B618" ' accountToken.Text
            'Dim token As String = "account_token=" + sAccountToken
            '' Dim order_id As String = "order_id= " + (iOrderId - 1).ToString  '+ orderID.Text

            'Dim order_id As String = "order_id= " + (iLastOrderId).ToString
            ''SetOrderId(iOrderId + 1)

            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + "&") & order_id) + "&") & protocol_version) + "&") & deviceModel) + "&") & entryMode) + "&") & chargeType) + "&") & chargeAmount
            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + "&") & order_id) + "&") & deviceModel) + "&") & entryMode) + "&") & chargeType) + "&") & chargeAmount
            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + "&") & order_id) + "&") & entryMode) + "&") & chargeType
            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + " & ") & order_id) + " & ") & entryMode) + " & ") & chargeType) + " & ") & chargeAmount

            'sResponse = webRequest_Post("https://ws.test.paygateway.com/api/v1/transactions", setupParameters)
            ' sResponse = webRequest_Post("https://ws.test.paygateway.com/HostPayService/v1/hostpay/transactions/", setupParameters)

            'Dim sPayProsDirectUrl As String = "https://test.t3secure.net/x-chargeweb.dll"

            'Dim sAcctNum As String = "4003000123456781"
            'Dim sExpDate As String = "1216"

            'Dim sPayProsWebId As String = "800000001938"
            'Dim sPayProsAuthKey As String = "Yfg74Amcfr2et0GEDS0juial4o6A2Sji"
            'Dim sPayProsTerminalId As String = "80022935"

            Dim lTrackingID As Long = 0 'CreditCard.CreateLog(0, "", Mid(sAcctNum, sAcctNum.Length - 3), 0, UserInfo.UserID, "CreateAlias", AccountInfo.GlobalLocationId, 0)

            If iCCLogId > 0 Then
                lTrackingID = iCCLogId 'CreditCardLogInfo.lCreditCardLogId
            Else
                lTrackingID = CreditCard.CreateLog(0, "", Mid(sAcctNum, sAcctNum.Length - 3), 0, userMonthly.UserID, "CreateAlias") ', AccountInfo.GlobalLocationId, 0 )
            End If

            'Dim SpecVersion As String = "SpecVersion=XWeb3.6"
            'Dim XWebID As String = "XWebID=" & sPayProsWebId
            'Dim POSType As String = "POSType=PC"
            'Dim AuthKey As String = "AuthKey=" & sPayProsAuthKey
            'Dim TerminalId As String = "TerminalId=" & sPayProsTerminalId
            'Dim PinCapabilities As String = "PinCapabilities=FALSE"
            'Dim TrackCapabilities As String = "TrackCapabilities=NONE"
            'Dim TrackingID As String = "TrackingID=" & lTrackingID
            'Dim TransactionType As String = "TransactionType=AliasCreateTransaction"
            'Dim AcctNum As String = "AcctNum=" & sAcctNum
            'Dim ExpDate As String = "ExpDate=" & sExpDate


            ' setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(SpecVersion & Convert.ToString("&")) & TransactionType) + "&") & XWebID) + "&") & POSType) + "&") & AuthKey
            'setupParameters = SpecVersion & "&" & TransactionType & "&" & XWebID & "&" & POSType & "&" & AuthKey
            'setupParameters = setupParameters & "&" & TerminalId & "&" & PinCapabilities & "&" & TrackCapabilities
            'setupParameters = setupParameters & "&" & TrackingID & "&" & TransactionType & "&" & AcctNum & "&" & ExpDate


            setupParameters = "<?xml version=""1.0""?>"
            setupParameters = setupParameters & "<GatewayRequest>"
            setupParameters = setupParameters & "<SpecVersion>XWeb3.6</SpecVersion>"
            setupParameters = setupParameters & "<XWebID>" & sEdgeWebId & "</XWebID>"
            setupParameters = setupParameters & "<AuthKey>" & sEdgeAuthKey & "</AuthKey>"
            setupParameters = setupParameters & "<TerminalID>" & sEdgeTerminalId & "</TerminalID>"
            'setupParameters = setupParameters & "<Industry>MOTO</Industry>"
            setupParameters = setupParameters & "<POSType>PC</POSType>"
            setupParameters = setupParameters & "<PinCapabilities>FALSE</PinCapabilities>"


            If sChargeType = "CreditSaleTransaction" Or sChargeType = "CreditReturnTransaction" Or sChargeType = "DebitSaleTransaction" Or sChargeType = "DebitReturnTransaction" Then
                If spRequest.GetPurchaseOrderNumber <> "" Then
                    setupParameters = setupParameters & "<UserDefined1>" & spRequest.GetPurchaseOrderNumber & "</UserDefined1>"
                End If
                If spRequest.GetInvoiceNumber <> "" Then
                    setupParameters = setupParameters & "<UserDefined2>" & spRequest.GetInvoiceNumber & "</UserDefined2>"
                End If
                'sMessage = sMessage & " <INVOICENO>" & CCRequest.GetInvoiceNumber & "</INVOICENO>"
                'sMessage = sMessage & " <PONUMBER>" & CCRequest.GetPurchaseOrderNumber & "</PONUMBER>"
                'sMessage = sMessage & " <SUPPRESSUI >" & "TRUE" & "</SUPPRESSUI >"
            End If
            'If spRequest.GetPurchaseOrderNumber <> "" Then
            '    setupParameters = setupParameters & "<UserDefined1>" & spRequest.GetPurchaseOrderNumber & "</UserDefined1>"
            'End If

            If spRequest.GetManagePayerData.ToUpper = "TRUE" And sAcctNum <> "" And sChargeType <> "ALIASUPDATE" Then
                setupParameters = setupParameters & " <CreateAlias>TRUE</CreateAlias>"
                bTokenConversion = True
            End If
            'If spRequest.GetManagePayerData.ToUpper = "TRUE" Then
            '    '    sMessage = sMessage & " <RECURRING>True</RECURRING>"
            '    If spRequest.GetPayerIdentifier <> "" Then
            '        setupParameters = setupParameters & " <ALIAS>" & spRequest.GetPayerIdentifier & "</ALIAS>"
            '    End If
            'End If
            If spRequest.GetPayerIdentifier <> "" Then
                setupParameters = setupParameters & " <Alias>" & spRequest.GetPayerIdentifier & "</Alias>"

                If bRecurring = True Then
                    setupParameters = setupParameters & "<ECI>" & "2" & "</ECI>"
                End If

                If sChargeType = "ALIASUPDATE" Then
                    setupParameters = setupParameters & "<TrackingID>" & lTrackingID & "</TrackingID>"
                    setupParameters = setupParameters & "<AcctNum>" & sAcctNum & "</AcctNum>"
                    setupParameters = setupParameters & "<ExpDate>" & sExpDate & "</ExpDate>"
                Else
                    setupParameters = setupParameters & "<CustomerPresent>" & "FALSE" & "</CustomerPresent>"
                    setupParameters = setupParameters & "<CardPresent>" & "FALSE" & "</CardPresent>"
                End If
                'ElseIf sChargeType = "ALIASUPDATE" Then
                setupParameters = setupParameters & "<TrackCapabilities>NONE</TrackCapabilities>"

            ElseIf sChargeType = "CREDITVOID" Then
                setupParameters = setupParameters & "<TrackingID>" & lTrackingID & "</TrackingID>"
                setupParameters = setupParameters & " <TransactionID>" & spRequest.GetOrderId.ToString.PadLeft(12, "0") & "</TransactionID>"
                setupParameters = setupParameters & "<TrackCapabilities>NONE</TrackCapabilities>"
                setupParameters = setupParameters & "<CustomerPresent>" & "FALSE" & "</CustomerPresent>"
                setupParameters = setupParameters & "<CardPresent>" & "FALSE" & "</CardPresent>"
            ElseIf sTrackData.Length > 10 Then
                setupParameters = setupParameters & "<TrackingID>" & lTrackingID & "</TrackingID>"
                'setupParameters = setupParameters & "<AcctNum>" & sAcctNum & "</AcctNum>"
                'setupParameters = setupParameters & "<ExpDate>" & sExpDate & "</ExpDate>"
                setupParameters = setupParameters & "<Track>" & sTrackData & "</Track>"
                setupParameters = setupParameters & "<TrackCapabilities>TRACK2</TrackCapabilities>"
                setupParameters = setupParameters & "<CustomerPresent>" & "TRUE" & "</CustomerPresent>"
                setupParameters = setupParameters & "<CardPresent>" & "TRUE" & "</CardPresent>"
            Else
                setupParameters = setupParameters & "<TrackingID>" & lTrackingID & "</TrackingID>"
                setupParameters = setupParameters & "<AcctNum>" & sAcctNum & "</AcctNum>"
                setupParameters = setupParameters & "<ExpDate>" & sExpDate & "</ExpDate>"
                'setupParameters = setupParameters & "<Track>" & sTrackData & "</Track>"
                setupParameters = setupParameters & "<TrackCapabilities>NONE</TrackCapabilities>"

                If sChargeType = "CreditSaleTransaction" Or sChargeType = "CreditReturnTransaction" Then
                    setupParameters = setupParameters & "<CustomerPresent>" & "FALSE" & "</CustomerPresent>"
                    setupParameters = setupParameters & "<CardPresent>" & "FALSE" & "</CardPresent>"
                End If
            End If
            'End If

            If sChargeType = "CREDITVOID" Then
                sChargeType = "CreditVoidTransaction"
            ElseIf sChargeType = "ALIASUPDATE" And spRequest.GetPayerIdentifier = "" Then
                sChargeType = "AliasCreateTransaction"
                bTokenConversion = True
            ElseIf sChargeType = "ALIASUPDATE" Then
                sChargeType = "AliasUpdateTransaction"
                bTokenUpdate = True
            Else

                setupParameters = setupParameters & "<Amount>" & spRequest.GetChargeTotal & "</Amount>"
                'If boffline = True Or spRequest.GetManagePayerData.ToUpper = "TRUE" Then
                '    setupParameters = setupParameters & "<CustomerPresent>" & "FALSE" & "</CustomerPresent>"
                '    setupParameters = setupParameters & "<CardPresent>" & "FALSE" & "</CardPresent>"
                'Else
                '    setupParameters = setupParameters & "<CustomerPresent>" & "TRUE" & "</CustomerPresent>"
                '    setupParameters = setupParameters & "<CardPresent>" & "TRUE" & "</CardPresent>"
                'End If

                If bDuplicateCheck = True Then
                    setupParameters = setupParameters & "<DuplicateMode>" & "CHECKING_ON" & "</DuplicateMode>"
                Else
                    setupParameters = setupParameters & "<DuplicateMode>" & "CHECKING_OFF" & "</DuplicateMode>"
                End If

            End If
            setupParameters = setupParameters & "<TransactionType>" & sChargeType & "</TransactionType>"



            setupParameters = setupParameters & "</GatewayRequest>"

            If bSaveSecurePaymentLog = True Then
                If sAcctNum.Length > 4 Then
                    sDisplayNumber = Mid(sAcctNum, 1, 4) & "...." & Mid(sAcctNum, sAcctNum.Length - 3)
                    sTransaction = setupParameters.Replace(sAcctNum, sDisplayNumber)


                    'iStart = InStr(setupParameters, "<AcctNum>")
                    'If iStart > 0 Then
                    '    iEnd = InStr(setupParameters, "</AcctNum>") + 10

                    '    If iEnd > iStart Then
                    '        sTransaction = setupParameters.Replace(Mid(setupParameters, iStart, iEnd - iStart), "<AcctNum>" & sDisplayNumber & "</AcctNum>")
                    '    End If
                    'End If
                Else
                    sTransaction = setupParameters
                End If
            
            End If

            'If wb_inline Is Nothing Then
            '    wb_inline = New WebBrowser ' 'PayPageWebBrowser(Me)
            '    wb_inline.Location = New System.Drawing.Point(50, 50)
            '    wb_inline.Size = New System.Drawing.Size(675, 650)

            '    Controls.Add(wb_inline)
            '    wb_inline.Visible = True
            'End If

            dtSent = Now
            ' sFirstDocument = ""
            Dim encoding As New ASCIIEncoding()
            Dim postData = setupParameters

            Dim AdditionalHeaders As String = "Content-Type: application/x-www-form-urlencoded" + Environment.NewLine

            'AdditionalHeaders = "Content-Type: application/json" + Environment.NewLine
            Dim data As Byte() = encoding.GetBytes(postData)

            'Dim PostData1() As Byte
            'PostData1 = StrConv(postData, VbStrConv.None)
            'Dim payProsUrl As String = "https://ws.test.paygateway.com/HostPayService/v1/hostpay/paypage/"

            'wb_inline.Navigate(payProsUrl, "", data, AdditionalHeaders)
            sResponse = WebRequest_Post(sEdgeDirectUrl, setupParameters)
            'sResponse = webRequest_Post("https://ws.test.paygateway.com/HostPayService/v1/hostpay/transactions/", setupParameters)

            dtReceived = Now
            'CreditCardLogInfo.lCreditCardLogId = lTrackingID
            'CreditCardLogInfo.sResult = ""
            'CreditCardLogInfo.sResultXML = sResponse
            'CreditCardLogInfo.iResponseTime = CInt((dtReceived - dtSent).TotalSeconds)
            'CreditCardLogInfo.sTroutD = ""
            'CreditCardLogInfo.sAuthCode = ""
            'CreditCard.UpdateLogTransCompleted(CreditCardLogInfo, UserInfo.UserName, AccountInfo.GlobalLocationId, 0)
            spResponse = ProcessResponse(sResponse, "", bTokenConversion, bTokenDelete, bTokenUpdate, "", False) ', boffline)

            If spResponse.GetPayerIdentifier <> "" Then
                If spResponse.GetBin = "" Then
                    spResponse.SetBin(sBin)
                End If
                If spResponse.GetSpan = "" Then
                    spResponse.SetSpan(sSPan)
                End If
            End If

            If bSaveSecurePaymentLog = True Then
                Account.SaveLog(sTransaction, userMonthly.UserID, sComputerName, 204, False)
                Account.SaveLog(sResponse, userMonthly.UserID, sComputerName, 205, False)
            End If

            Return spResponse
        Catch ex As Exception
            WriteToLog(sLogFileName, "doDirectToGatewayTransaction", Err.Number, ex.Message, "ERROR")
            Return spResponse
        End Try
    End Function

    Private Function ProcessResponse(ByVal sResponse As String, ByVal sStatus As String, ByVal bTokenConversion As Boolean, ByVal bTokenDelete As Boolean, ByVal bTokenUpdate As Boolean, ByVal sAlias As String, ByVal bCardSwipe As Boolean) As SecurePaymentResponse
        Dim spResponse As New SecurePaymentResponse
        Dim sRecordNo As String = ""

        Dim sSpan As String = ""

        Dim sTokenInfo(0) As String
        Dim sPayerIdentifier As String = ""

        Dim dsResponse As New DataSet

        Dim sSuccess As String = ""
        Dim sResult As String = ""
        Dim sResultMessage As String = ""

        Dim dtResponse As DataTable

        Dim sResponseCode As String = ""
        Dim sTextResponse As String = ""

        Dim sTrack As String = ""
        Try

            dsResponse.ReadXml(New System.IO.StringReader(sResponse))

            'If frmCreditCardForm.Visible = True Then



            '    frmSecurePaymentsForm.TopLevel = False
            '    frmCreditCardForm.Panel1.Controls.Add(frmSecurePaymentsForm)
            '    frmSecurePaymentsForm.bTransactionComplete = False
            '    frmSecurePaymentsForm.bEmbedded = True
            '    frmSecurePaymentsForm.Show()

            '    Do Until frmSecurePaymentsForm.bTransactionComplete = True Or (Now - dtStart).TotalMilliseconds > iCCTimeOut * 2
            '        Application.DoEvents()
            '    Loop

            'Else

            '    frmSecurePaymentsForm.ShowDialog()
            'End If





            'dsResponse = frmSecurePaymentsForm.dsResponse

            If dsResponse.Tables.Count > 0 Then

                If iCreditMode >= 8 Then

                    If iCreditMode = 8 And dsResponse.Tables.Count > 2 Then
                        If ColumnExists(dsResponse.Tables(0), "IsSuccessful") = True Then
                            If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("IsSuccessful")) Then
                                sSuccess = dsResponse.Tables(0).Rows(0).Item("IsSuccessful").ToString
                            End If
                        End If
                        If sSuccess.ToUpper = "TRUE" Then
                            dtResponse = dsResponse.Tables(2)
                        End If
                    Else 'If dsResponse.Tables.Count > 0 Then
                        sSuccess = "TRUE"
                        dtResponse = dsResponse.Tables(0)
                    End If

                    If sSuccess.ToUpper = "TRUE" Then

                        'If bOffline = True Then

                        If ColumnExists(dtResponse, "ResponseCode") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("ResponseCode")) Then
                                sResponseCode = dtResponse.Rows(0).Item("ResponseCode").ToString
                                Select Case sResponseCode
                                    Case "000"
                                        sResult = "0"
                                    Case "001"
                                        sResult = "12"
                                    Case "901", "002", "010"
                                        sResult = "901"
                                    Case "800" To "812"
                                        sResult = "901"
                                    Case "814" To "899"
                                        sResult = "901"

                                End Select
                                'If sResponseCode = "000" Then sResult = "0"
                                'If sResponseCode = "001" Then sResult = "12"
                                'If sResponseCode = "901" Then sResult = "901"
                                spResponse.SetResponseCode(sResponseCode)
                            End If
                        End If

                        If ColumnExists(dtResponse, "RESULT") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("RESULT")) Then
                                sResult = dtResponse.Rows(0).Item("RESULT").ToString
                            End If
                        End If

                        Select Case sResult
                            Case "0" 'Success
                                spResponse.SetResponseCode(1)
                            Case "12" 'Declined
                                spResponse.SetResponseCode(100)
                            Case "901" 'PROCESSING ERROR
                                spResponse.SetResponseCode(901)
                            Case Else
                                spResponse.SetResponseCode(0)
                        End Select

                        If ColumnExists(dtResponse, "RESULTMSG") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("RESULTMSG")) Then
                                sResultMessage = dtResponse.Rows(0).Item("RESULTMSG").ToString
                            End If
                        End If

                        If ColumnExists(dtResponse, "HOSTRESPONSECODE") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("HOSTRESPONSECODE")) Then
                                spResponse.SetSecondaryResponseCode(dtResponse.Rows(0).Item("HOSTRESPONSECODE").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "HOSTRESPONSEDESCRIPTION") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("HOSTRESPONSEDESCRIPTION")) Then
                                spResponse.SetResponseCodeText(dtResponse.Rows(0).Item("HOSTRESPONSEDESCRIPTION").ToString)
                            End If
                        End If


                        If ColumnExists(dtResponse, "ResponseDescription") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("ResponseDescription")) Then
                                spResponse.SetResponseCodeText(dtResponse.Rows(0).Item("ResponseDescription").ToString)
                            End If
                        End If

                        If spResponse.GetResponseCodeText = "" Then
                            spResponse.SetResponseCodeText(sResultMessage)
                        End If


                        If ColumnExists(dtResponse, "APPROVALCODE") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("APPROVALCODE")) Then
                                spResponse.SetBankApprovalCode(dtResponse.Rows(0).Item("APPROVALCODE").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "APPROVEDAMOUNT") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("APPROVEDAMOUNT")) Then
                                spResponse.SetCapturedAmount(dtResponse.Rows(0).Item("APPROVEDAMOUNT").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "AMOUNT") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("AMOUNT")) Then
                                spResponse.SetCapturedAmount(dtResponse.Rows(0).Item("AMOUNT").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "ExpDate") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("ExpDate")) Then
                                spResponse.SetExpireMonth(Mid(dtResponse.Rows(0).Item("ExpDate").ToString, 1, 2))
                            End If
                        End If

                        If ColumnExists(dtResponse, "ExpDate") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("ExpDate")) Then
                                spResponse.SetExpireYear("20" & Mid(dtResponse.Rows(0).Item("ExpDate").ToString, dtResponse.Rows(0).Item("ExpDate").ToString.Length - 1))
                            End If
                        End If

                        If ColumnExists(dtResponse, "EXPMONTH") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("EXPMONTH")) Then
                                spResponse.SetExpireMonth(dtResponse.Rows(0).Item("EXPMONTH").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "EXPYEAR") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("EXPYEAR")) Then
                                spResponse.SetExpireYear(dtResponse.Rows(0).Item("EXPYEAR").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "CARDBRAND") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("CARDBRAND")) Then
                                spResponse.SetCardBrand(dtResponse.Rows(0).Item("CARDBRAND").ToString)
                            End If
                        End If




                        If ColumnExists(dtResponse, "TRANSACTIONID") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("TRANSACTIONID")) Then
                                spResponse.SetTransactionId(dtResponse.Rows(0).Item("TRANSACTIONID").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "ALIAS") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("ALIAS")) Then
                                spResponse.SetPayerIdentifier(dtResponse.Rows(0).Item("ALIAS").ToString)
                                spResponse.SetMpdResponseCode(1)
                                spResponse.SetMpdResponseCodeText("Success")
                            End If
                        End If

                        If ColumnExists(dtResponse, "MaskedAcctNum") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("MaskedAcctNum")) Then
                                spResponse.SetSpan(Mid(dtResponse.Rows(0).Item("MaskedAcctNum").ToString, dtResponse.Rows(0).Item("MaskedAcctNum").ToString.Length - 3))
                            End If
                        End If

                        If ColumnExists(dtResponse, "ACCOUNT") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("ACCOUNT")) Then
                                spResponse.SetSpan(dtResponse.Rows(0).Item("ACCOUNT").ToString)

                                If spResponse.GetSpan.Length > 4 Then
                                    spResponse.SetSpan(Mid(spResponse.GetSpan, spResponse.GetSpan.Length - 3))
                                End If

                            End If
                        End If

                        If ColumnExists(dtResponse, "signature") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("signature")) Then
                                spResponse.SetSignature(dtResponse.Rows(0).Item("signature").ToString)
                            End If
                        End If

                        If ColumnExists(dtResponse, "signatureimage") = True Then
                            If Not IsDBNull(dtResponse.Rows(0).Item("signatureimage")) Then
                                spResponse.SetSignature(dtResponse.Rows(0).Item("signatureimage").ToString)
                            End If
                        End If


                        If bTokenConversion = True Or bTokenDelete = True Or bTokenUpdate = True Then

                            If ColumnExists(dtResponse, "ResponseCode") = True Then
                                If Not IsDBNull(dtResponse.Rows(0).Item("ResponseCode")) Then
                                    sResponseCode = dtResponse.Rows(0).Item("ResponseCode").ToString
                                    spResponse.SetResponseCode(sResponseCode)
                                End If
                            End If
                            If ColumnExists(dtResponse, "HOSTRESPONSECODE") = True Then
                                If Not IsDBNull(dtResponse.Rows(0).Item("HOSTRESPONSECODE")) Then
                                    sResponseCode = dtResponse.Rows(0).Item("HOSTRESPONSECODE").ToString
                                    spResponse.SetResponseCode(sResponseCode)
                                End If
                            End If

                            If ColumnExists(dtResponse, "ResponseDescription") = True Then
                                If Not IsDBNull(dtResponse.Rows(0).Item("ResponseDescription")) Then
                                    spResponse.SetResponseCodeText(dtResponse.Rows(0).Item("ResponseDescription").ToString)
                                End If
                            End If
                            If ColumnExists(dtResponse, "HOSTRESPONSEDESCRIPTION") = True Then
                                If Not IsDBNull(dtResponse.Rows(0).Item("HOSTRESPONSEDESCRIPTION")) Then
                                    spResponse.SetResponseCodeText(dtResponse.Rows(0).Item("HOSTRESPONSEDESCRIPTION").ToString)
                                End If
                            End If

                            If sResponseCode = "005" Then

                                If ColumnExists(dtResponse, "Alias") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("Alias")) Then
                                        spResponse.SetPayerIdentifier(dtResponse.Rows(0).Item("Alias").ToString)
                                        spResponse.SetMpdResponseCode(1)
                                        spResponse.SetMpdResponseCodeText("Success")
                                    End If
                                End If

                                If bTokenUpdate = True Then
                                    spResponse.SetPayerIdentifier(sAlias)
                                    spResponse.SetMpdResponseCode(1)
                                    spResponse.SetMpdResponseCodeText("Success")
                                End If

                                If ColumnExists(dtResponse, "ExpDate") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("ExpDate")) Then
                                        spResponse.SetExpireMonth(Mid(dtResponse.Rows(0).Item("ExpDate").ToString, 1, 2))
                                    End If
                                End If

                                If ColumnExists(dtResponse, "ExpDate") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("ExpDate")) Then
                                        spResponse.SetExpireYear("20" & Mid(dtResponse.Rows(0).Item("ExpDate").ToString, dtResponse.Rows(0).Item("ExpDate").ToString.Length - 1))
                                    End If
                                End If

                                If ColumnExists(dtResponse, "EXPMONTH") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("EXPMONTH")) Then
                                        spResponse.SetExpireMonth(dtResponse.Rows(0).Item("EXPMONTH").ToString)
                                    End If
                                End If

                                If ColumnExists(dtResponse, "EXPYEAR") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("EXPYEAR")) Then
                                        spResponse.SetExpireYear(dtResponse.Rows(0).Item("EXPYEAR").ToString)
                                    End If
                                End If

                                If ColumnExists(dtResponse, "CardBrand") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("CardBrand")) Then
                                        spResponse.SetCardBrand(dtResponse.Rows(0).Item("CardBrand").ToString)
                                    End If
                                End If

                                'If ColumnExists(dtResponse, "CARDBRANDSHORT") = True Then
                                '    If Not IsDBNull(dtResponse.Rows(0).Item("CARDBRANDSHORT")) Then
                                '        spResponse.SetCardBrand(dtResponse.Rows(0).Item("CARDBRANDSHORT").ToString)
                                '    End If
                                'End If

                                If ColumnExists(dtResponse, "MaskedAcctNum") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("MaskedAcctNum")) Then
                                        spResponse.SetSpan(Mid(dtResponse.Rows(0).Item("MaskedAcctNum").ToString, dtResponse.Rows(0).Item("MaskedAcctNum").ToString.Length - 3))
                                    End If
                                End If
                                If ColumnExists(dtResponse, "ACCOUNT") = True Then
                                    If Not IsDBNull(dtResponse.Rows(0).Item("ACCOUNT")) Then
                                        spResponse.SetSpan(Mid(dtResponse.Rows(0).Item("ACCOUNT").ToString, dtResponse.Rows(0).Item("ACCOUNT").ToString.Length - 3))
                                    End If
                                End If

                            End If

                        End If

                        If bCardSwipe = True Then

                            If ColumnExists(dtResponse, "TRACK") = True Then
                                If Not IsDBNull(dtResponse.Rows(0).Item("TRACK")) Then

                                    sTrack = dtResponse.Rows(0).Item("TRACK").ToString

                                    Do Until InStr(sTrack, "|") = 0
                                        sTrack = Mid(sTrack, InStr(sTrack, "|") + 1)
                                    Loop

                                    spResponse.SetResponseCodeText(sTrack)
                                End If
                            End If

                            If ColumnExists(dtResponse, "ACCOUNT") = True Then
                                If Not IsDBNull(dtResponse.Rows(0).Item("ACCOUNT")) Then
                                    spResponse.SetPayerIdentifier(dtResponse.Rows(0).Item("ACCOUNT").ToString)
                                End If
                            End If
                        End If


                    End If




                ElseIf dsResponse.Tables(0).Rows.Count > 0 Then


                    If ColumnExists(dsResponse.Tables(0), "SequenceNo") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("SequenceNo")) Then
                            spResponse.SetReferenceId(dsResponse.Tables(0).Rows(0).Item("SequenceNo").ToString)
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "CmdStatus") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("CmdStatus")) Then
                            sStatus = dsResponse.Tables(0).Rows(0).Item("CmdStatus").ToString
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "TextResponse") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("TextResponse")) Then
                            spResponse.SetResponseCodeText(dsResponse.Tables(0).Rows(0).Item("TextResponse").ToString)

                            sTextResponse = spResponse.GetResponseCodeText.ToUpper

                            If sTextResponse = "" And sStatus <> "" Then sTextResponse = sStatus.ToUpper

                            If InStr(sTextResponse, "DECLINED") > 0 Or InStr(sStatus.ToUpper, "DECLINED") > 0 Then
                                spResponse.SetResponseCode("100")
                            ElseIf InStr(sTextResponse, "NOT APPROVED") > 0 Or InStr(sStatus.ToUpper, "NOT APPROVED") > 0 Then
                                spResponse.SetResponseCode("100")
                            ElseIf InStr(sTextResponse, "APPROVED") > 0 Or InStr(sStatus.ToUpper, "APPROVED") > 0 Then
                                spResponse.SetResponseCode("1")
                            Else
                                spResponse.SetResponseCode("0")
                            End If

                        End If
                    End If
                    If dsResponse.Tables.Count > 1 Then
                        If ColumnExists(dsResponse.Tables(1), "AuthCode") = True Then
                            If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("AuthCode")) Then
                                spResponse.SetBankApprovalCode(dsResponse.Tables(1).Rows(0).Item("AuthCode").ToString)
                            End If
                        End If

                        If ColumnExists(dsResponse.Tables(1), "Purchase") = True Then
                            If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("Purchase")) Then
                                spResponse.SetRequestedAmount(dsResponse.Tables(1).Rows(0).Item("Purchase").ToString)
                            End If
                        End If

                        If ColumnExists(dsResponse.Tables(1), "Authorize") = True Then
                            If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("Authorize")) Then
                                spResponse.SetCapturedAmount(dsResponse.Tables(1).Rows(0).Item("Authorize").ToString)
                            End If
                        End If

                        If ColumnExists(dsResponse.Tables(1), "CardType") = True Then
                            If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("CardType")) Then
                                spResponse.SetCardBrand(dsResponse.Tables(1).Rows(0).Item("CardType").ToString)
                            End If
                        End If

                        If spResponse.GetResponseCode <> "100" And ColumnExists(dsResponse.Tables(1), "RecordNo") = True Then
                            If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("RecordNo")) Then
                                sRecordNo = dsResponse.Tables(1).Rows(0).Item("RecordNo").ToString

                                If sRecordNo <> "" Then

                                    If InStr(sRecordNo, "|") <= 0 Then
                                        spResponse.SetPayerIdentifier(sRecordNo)
                                        spResponse.SetMpdResponseCode(1)
                                        spResponse.SetMpdResponseCodeText("Success")

                                        If ColumnExists(dsResponse.Tables(1), "AcctNo") = True Then
                                            If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("AcctNo")) Then
                                                spResponse.SetSpan(Mid(dsResponse.Tables(1).Rows(0).Item("AcctNo").ToString, dsResponse.Tables(1).Rows(0).Item("AcctNo").ToString.Length - 3))
                                            End If
                                        Else
                                            sSpan = Mid(sRecordNo, sRecordNo.Length - 3)
                                            spResponse.SetSpan(sSpan)
                                        End If



                                    ElseIf Mid(sRecordNo, 1, 1) <> "|" Then
                                        sTokenInfo = sRecordNo.Split("|")
                                        If sTokenInfo.Length > 3 Then
                                            spResponse.SetPayerIdentifier(sRecordNo)
                                            spResponse.SetMpdResponseCode(1)
                                            spResponse.SetMpdResponseCodeText("Success")

                                            If sTokenInfo(1).Length > 2 Then
                                                spResponse.SetExpireMonth(Mid(sTokenInfo(1), 1, 2))
                                                spResponse.SetExpireYear(Mid(sTokenInfo(1), 3))
                                            Else
                                                spResponse.SetExpireMonth(sTokenInfo(1))
                                                spResponse.SetExpireYear(sTokenInfo(2))
                                            End If

                                            If ColumnExists(dsResponse.Tables(1), "AcctNo") = True Then
                                                If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("AcctNo")) Then
                                                    spResponse.SetSpan(Mid(dsResponse.Tables(1).Rows(0).Item("AcctNo").ToString, dsResponse.Tables(1).Rows(0).Item("AcctNo").ToString.Length - 3))
                                                End If
                                            Else
                                                If sTokenInfo(0).Length > 4 Then
                                                    spResponse.SetSpan(Mid(sTokenInfo(0), sTokenInfo(0).Length - 3))
                                                End If
                                            End If
                                        Else
                                            spResponse.SetPayerIdentifier(sRecordNo)
                                            spResponse.SetMpdResponseCode(1)
                                            spResponse.SetMpdResponseCodeText("Success")

                                            If ColumnExists(dsResponse.Tables(1), "AcctNo") = True Then
                                                If Not IsDBNull(dsResponse.Tables(1).Rows(0).Item("AcctNo")) Then
                                                    spResponse.SetSpan(Mid(dsResponse.Tables(1).Rows(0).Item("AcctNo").ToString, dsResponse.Tables(1).Rows(0).Item("AcctNo").ToString.Length - 3))
                                                End If
                                            Else
                                                sSpan = Mid(sRecordNo, sRecordNo.Length - 3)
                                                spResponse.SetSpan(sSpan)
                                            End If

                                        End If
                                    End If
                                End If
                            End If
                        End If


                    End If


                    If ColumnExists(dsResponse.Tables(0), "original_secondary_response_code") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("original_secondary_response_code")) Then
                            spResponse.SetSecondaryResponseCode(dsResponse.Tables(0).Rows(0).Item("original_secondary_response_code").ToString)
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "original_response_code") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("original_response_code")) Then
                            spResponse.SetResponseCode(dsResponse.Tables(0).Rows(0).Item("original_response_code").ToString)
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "bin") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("bin")) Then
                            spResponse.SetBin(dsResponse.Tables(0).Rows(0).Item("bin").ToString)
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "expire_month") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("expire_month")) Then
                            spResponse.SetExpireMonth(dsResponse.Tables(0).Rows(0).Item("expire_month").ToString)
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "expire_year") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("expire_year")) Then
                            spResponse.SetExpireYear(dsResponse.Tables(0).Rows(0).Item("expire_year").ToString)
                        End If
                    End If


                    If ColumnExists(dsResponse.Tables(0), "span") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("span")) Then
                            spResponse.SetSpan(dsResponse.Tables(0).Rows(0).Item("span").ToString)
                        End If
                    End If






                    If ColumnExists(dsResponse.Tables(0), "payer_identifier") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("payer_identifier")) Then
                            sPayerIdentifier = dsResponse.Tables(0).Rows(0).Item("payer_identifier").ToString
                            If Mid(sPayerIdentifier, 1, 1) <> "|" Then
                                spResponse.SetPayerIdentifier(sPayerIdentifier)
                                spResponse.SetMpdResponseCode(1)
                                spResponse.SetMpdResponseCodeText("Success")
                            End If
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "signature") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("signature")) Then
                            spResponse.SetSignature(dsResponse.Tables(0).Rows(0).Item("signature").ToString)
                        End If
                    End If

                    If ColumnExists(dsResponse.Tables(0), "signatureimage") = True Then
                        If Not IsDBNull(dsResponse.Tables(0).Rows(0).Item("signatureimage")) Then
                            spResponse.SetSignature(dsResponse.Tables(0).Rows(0).Item("signatureimage").ToString)
                        End If
                    End If


                End If
            End If

            If spResponse.GetBankApprovalCode = "" And sStatus <> "" Then
                spResponse.SetBankApprovalCode(sStatus)
            End If

            Dim iTempError As Integer = 0
            iTempError = InStr(spResponse.GetResponseCodeText.ToUpper, "ERROR:")

            If iTempError > 0 And spResponse.GetResponseCodeText.Length > iTempError + 6 Then
                spResponse.SetResponseCodeText(Mid(spResponse.GetResponseCodeText, iTempError + 6))
            End If


            Return spResponse

        Catch ex As Exception
            Return spResponse
        End Try
    End Function

    Private Function WebRequest_Post(ByVal url As String, ByVal xmlDoc As String, Optional ByVal requestMethod As String = "POST") As String
        Dim responseFromServer As String = ""
        Try


            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = CType(3072, SecurityProtocolType)
            ServicePointManager.DefaultConnectionLimit = 9999


            Dim request As WebRequest = Net.WebRequest.Create(url)
            ' Set the Method property of the request to POST.
            request.Method = requestMethod
            ' Create POST data and convert it to a byte array.
            Dim postData As String = xmlDoc
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            ' Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded"
            ' Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length
            ' Get the request stream.
            Dim dataStream As Stream = request.GetRequestStream()

            ' Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length)
            ' Close the Stream object.
            dataStream.Close()
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            ' Get the stream containing content returned by the server.
            dataStream = response.GetResponseStream()
            ' Open the stream using a StreamReader for easy access.
            Dim reader As New StreamReader(dataStream)
            ' Read the content.
            responseFromServer = reader.ReadToEnd()
            ' Clean up the streams.
            reader.Close()
            dataStream.Close()
            response.Close()
            Return responseFromServer

        Catch ex As Exception
            'MsgBox(ex.Message)
            If responseFromServer <> "" Then
                Return responseFromServer
            Else
                Return ex.Message
            End If

        End Try
    End Function

    Public Function doCreateAliasTransaction(ByVal spRequest As SecurePaymentRequest, ByVal iCCLogId As Long) As SecurePaymentResponse
        Dim setupParameters As String = ""
        Dim sResponse As String = ""

        Dim sAcctNum As String = ""
        Dim sExpDate As String = ""

        ' Dim CreditCardLogInfo As New Micrologic.CreditCardLogProperties
        Dim dtSent As DateTime
        Dim dtReceived As DateTime

        Dim spResponse As New SecurePaymentResponse
        Try
            sAcctNum = spRequest.GetCreditCardNumber
            sExpDate = spRequest.GetExpDate

            ''string entryMode = "entry_mode=KEYED";
            ''Dim entryMode As String = "entry_mode=SWIPED"
            'Dim entryMode As String = "entry_mode=EMV"
            ''Dim deviceModel As String = "pos_device_model=ingenico_isc250"
            'Dim transactionType As String = "transaction_type=CREDIT_CARD"
            'Dim chargeType As String = "charge_type=QUERY_PAYMENT"
            '' Dim protocol_version As String = "" '"protocol_version=13"
            ''Dim chargeAmount As String = "charge_total=1.00"
            ''Dim token As String = "account_token=" + "FDC4E33FCE6C54083389EDF18D50AF64C3C0259544B6C3FFEEA64B268DDD5FEB9E52BE59D7BDC8EC72" ' accountToken.Text
            ''Dim token As String = "account_token=" + "D80332EECE61540B3988EDF18754A765C8CF299440B0C6FDEBA64E2A8CDF5FEA9C55BB5B6E19B1B618" ' accountToken.Text
            'Dim token As String = "account_token=" + sAccountToken
            '' Dim order_id As String = "order_id= " + (iOrderId - 1).ToString  '+ orderID.Text

            'Dim order_id As String = "order_id= " + (iLastOrderId).ToString
            ''SetOrderId(iOrderId + 1)

            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + "&") & order_id) + "&") & protocol_version) + "&") & deviceModel) + "&") & entryMode) + "&") & chargeType) + "&") & chargeAmount
            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + "&") & order_id) + "&") & deviceModel) + "&") & entryMode) + "&") & chargeType) + "&") & chargeAmount
            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + "&") & order_id) + "&") & entryMode) + "&") & chargeType
            'setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(token & Convert.ToString("&")) & transactionType) + " & ") & order_id) + " & ") & entryMode) + " & ") & chargeType) + " & ") & chargeAmount

            'sResponse = webRequest_Post("https://ws.test.paygateway.com/api/v1/transactions", setupParameters)
            ' sResponse = webRequest_Post("https://ws.test.paygateway.com/HostPayService/v1/hostpay/transactions/", setupParameters)

            'Dim sPayProsDirectUrl As String = "https://test.t3secure.net/x-chargeweb.dll"

            'Dim sAcctNum As String = "4003000123456781"
            'Dim sExpDate As String = "1216"

            'Dim sPayProsWebId As String = "800000001938"
            'Dim sPayProsAuthKey As String = "Yfg74Amcfr2et0GEDS0juial4o6A2Sji"
            'Dim sPayProsTerminalId As String = "80022935"

            Dim lTrackingID As Long = 0 'CreditCard.CreateLog(0, "", Mid(sAcctNum, sAcctNum.Length - 3), 0, UserInfo.UserID, "CreateAlias", AccountInfo.GlobalLocationId, 0)

            If iCCLogId > 0 Then
                lTrackingID = iCCLogId 'CreditCardLogInfo.lCreditCardLogId
            Else
                lTrackingID = CreditCard.CreateLog(0, "", Mid(sAcctNum, sAcctNum.Length - 3), 0, userMonthly.UserID, "CreateAlias") ', AccountInfo.GlobalLocationId, 0 )
            End If

            'Dim SpecVersion As String = "SpecVersion=XWeb3.6"
            'Dim XWebID As String = "XWebID=" & sPayProsWebId
            'Dim POSType As String = "POSType=PC"
            'Dim AuthKey As String = "AuthKey=" & sPayProsAuthKey
            'Dim TerminalId As String = "TerminalId=" & sPayProsTerminalId
            'Dim PinCapabilities As String = "PinCapabilities=FALSE"
            'Dim TrackCapabilities As String = "TrackCapabilities=NONE"
            'Dim TrackingID As String = "TrackingID=" & lTrackingID
            'Dim TransactionType As String = "TransactionType=AliasCreateTransaction"
            'Dim AcctNum As String = "AcctNum=" & sAcctNum
            'Dim ExpDate As String = "ExpDate=" & sExpDate


            ' setupParameters = Convert.ToString((Convert.ToString((Convert.ToString((Convert.ToString(SpecVersion & Convert.ToString("&")) & TransactionType) + "&") & XWebID) + "&") & POSType) + "&") & AuthKey
            'setupParameters = SpecVersion & "&" & TransactionType & "&" & XWebID & "&" & POSType & "&" & AuthKey
            'setupParameters = setupParameters & "&" & TerminalId & "&" & PinCapabilities & "&" & TrackCapabilities
            'setupParameters = setupParameters & "&" & TrackingID & "&" & TransactionType & "&" & AcctNum & "&" & ExpDate

            setupParameters = "<?xml version=""1.0""?>"
            setupParameters = setupParameters & "<GatewayRequest>"
            setupParameters = setupParameters & "<SpecVersion>XWeb3.6</SpecVersion>"
            setupParameters = setupParameters & "<XWebID>" & sEdgeWebId & "</XWebID>"
            setupParameters = setupParameters & "<AuthKey>" & sEdgeAuthKey & "</AuthKey>"
            setupParameters = setupParameters & "<TerminalID>" & sEdgeTerminalId & "</TerminalID>"
            'setupParameters = setupParameters & "<Industry>MOTO</Industry>"
            setupParameters = setupParameters & "<POSType>PC</POSType>"
            setupParameters = setupParameters & "<PinCapabilities>FALSE</PinCapabilities>"
            setupParameters = setupParameters & "<TrackCapabilities>NONE</TrackCapabilities>"
            setupParameters = setupParameters & "<TrackingID>" & lTrackingID & "</TrackingID>"
            setupParameters = setupParameters & "<TransactionType>AliasCreateTransaction</TransactionType>"
            setupParameters = setupParameters & "<AcctNum>" & sAcctNum & "</AcctNum>"
            setupParameters = setupParameters & "<ExpDate>" & sExpDate & "</ExpDate>"
            setupParameters = setupParameters & "</GatewayRequest>"

            'If wb_inline Is Nothing Then
            '    wb_inline = New WebBrowser ' 'PayPageWebBrowser(Me)
            '    wb_inline.Location = New System.Drawing.Point(50, 50)
            '    wb_inline.Size = New System.Drawing.Size(675, 650)

            '    Controls.Add(wb_inline)
            '    wb_inline.Visible = True
            'End If

            dtSent = Now
            ' sFirstDocument = ""
            Dim encoding As New ASCIIEncoding()
            Dim postData = setupParameters

            Dim AdditionalHeaders As String = "Content-Type: application/x-www-form-urlencoded" + Environment.NewLine

            'AdditionalHeaders = "Content-Type: application/json" + Environment.NewLine
            Dim data As Byte() = encoding.GetBytes(postData)

            'Dim PostData1() As Byte
            'PostData1 = StrConv(postData, VbStrConv.None)
            'Dim payProsUrl As String = "https://ws.test.paygateway.com/HostPayService/v1/hostpay/paypage/"

            'wb_inline.Navigate(payProsUrl, "", data, AdditionalHeaders)
            sResponse = WebRequest_Post(sEdgeDirectUrl, setupParameters)
            'sResponse = webRequest_Post("https://ws.test.paygateway.com/HostPayService/v1/hostpay/transactions/", setupParameters)

            dtReceived = Now
            'CreditCardLogInfo.lCreditCardLogId = lTrackingID
            'CreditCardLogInfo.sResult = ""
            'CreditCardLogInfo.sResultXML = sResponse
            'CreditCardLogInfo.iResponseTime = CInt((dtReceived - dtSent).TotalSeconds)
            'CreditCardLogInfo.sTroutD = ""
            'CreditCardLogInfo.sAuthCode = ""
            'CreditCard.UpdateLogTransCompleted(CreditCardLogInfo, UserInfo.UserName, AccountInfo.GlobalLocationId, 0)
            spResponse = ProcessResponse(sResponse, "", True, False, False, "", False) ', False)


            Return spResponse
        Catch ex As Exception
            Return spResponse
        End Try
    End Function

    Public Function ColumnExists(ByVal DT As DataTable, ByVal ColumnName As String) As Boolean
        Try
            If Not DT Is Nothing Then
                Dim COL As DataColumn = DT.Columns(CStr(ColumnName))
                If Not COL Is Nothing Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        Catch
            Err.Clear()
            Return False
        End Try
    End Function



End Module
